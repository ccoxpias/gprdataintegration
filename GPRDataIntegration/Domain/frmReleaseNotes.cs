using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GPRDataIntegration.Domain
{
    public partial class frmReleaseNotes : Form
    {
      #region constructors
      public frmReleaseNotes()
      {
        InitializeComponent();
      }
      #endregion

      #region methods
      private void btnOk_Click(object sender, EventArgs e)
      {
        this.Close();
      }

      private void frmReleaseNotes_Load(object sender, EventArgs e)
      {
        ReleaseNotesViewer.ReleaseNotesViewer viewer = new ReleaseNotesViewer.ReleaseNotesViewer();

        string executableName = Application.ExecutablePath;
        System.IO.FileInfo executableFileInfo = new System.IO.FileInfo(executableName);

        //the path where the Release Notes folder exists
        viewer.ReleaseNotesPath = executableFileInfo.DirectoryName + @"\ReleaseNotes";

        viewer.Dock = DockStyle.Fill;

        pnlNotes.Controls.Add(viewer);
      }
      #endregion
    }
}