using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace GPRDataIntegration.Domain
{
    public partial class frmAbout : Form
    {
      #region constructors
      public frmAbout()
      {
        InitializeComponent();
      }
      #endregion

      #region methods
      private void btnOk_Click(object sender, EventArgs e)
      {
        this.Close();
      }

      private void frmAbout_Load(object sender, EventArgs e)
      {
        lblVersion.Text = "Version: " + Assembly.GetExecutingAssembly().GetName().Version.ToString().Substring(0, 3);

        DateTime buildDateTime = new DateTime(2000, 1, 1).AddDays(Assembly.GetExecutingAssembly().GetName().Version.Build);

        lblBuildDate.Text = "Build Date: " + buildDateTime.ToShortDateString();

      }
      #endregion
    }
}