using Roadware.DataAccess;
using Roadware.DataMapping;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace GPRDataIntegration.Domain
{
  public partial class GPRMainForm : Form
  {
    #region fields
    private DataMapper dataMapper = null;

    private string connectionString = "";
    #endregion

    #region constructors
    public GPRMainForm()
    {
      InitializeComponent();

      ToolStripManager.Renderer = new ToolStripProfessionalRenderer(new CustomProfessionalColors());

    }
    #endregion

    #region methods
    private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      frmAbout about = new frmAbout();
      about.ShowDialog();
    }

    private void connect()
    {
      string appProjectPath = Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + @"\Fugro Roadware\GPR\";
      if (!System.IO.Directory.Exists(appProjectPath))
      {
        System.IO.Directory.CreateDirectory(appProjectPath);
      }

      string connStr = "";
      string connectionFile = System.IO.Path.Combine(appProjectPath, "Connection.UDL");
      if (!System.IO.File.Exists(connectionFile))
      {
        using (System.IO.StreamWriter sr = System.IO.File.CreateText(connectionFile))
        {
          sr.Close();
        }
      }

      DateTime time = System.IO.File.GetLastWriteTime(connectionFile);
      System.Diagnostics.Process process = new System.Diagnostics.Process();
      process.StartInfo.FileName = connectionFile;
      process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
      process.Start();
      process.WaitForExit();
      if (System.IO.File.GetLastWriteTime(connectionFile).CompareTo(time) != 0)
      {
        connStr = System.IO.File.ReadAllText(connectionFile);
        if (!String.IsNullOrEmpty(connStr))
        {
          connStr = GetValidConnectionString(connStr);
        }
        this.connect(connStr);
      }
    }

    private void connect(string connectionString)
    {
      try
      {
        this.ucGPRControl.Reset();
        this.dataMapper = null;
        this.dataMapper = new DataMapper(ProviderType.SqlClient, connectionString);

        SqlConnectionStringBuilder conb = new SqlConnectionStringBuilder(connectionString);
        this.sslbConncetion.Text = "[" + conb.DataSource + "] [" + conb.InitialCatalog + "]";

        this.connectionString = connectionString;
        this.ucGPRControl.DataMapper = this.dataMapper;
        this.ucGPRControl.ConnectionString = connectionString;
        this.ucGPRControl.Enabled = true;
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void exToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private string GetValidConnectionString(string connectionString)
    {
      string connStr = connectionString.Substring(connectionString.IndexOf("Provider="));
      string[] connStrA = connStr.Split(';');
      SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();
      List<string> connStrPrts = new List<string>();
      foreach (string connStrPrt in connStrA)
      {
        string[] kr = connStrPrt.Split('=');
        if (scsb.ContainsKey(kr[0].Trim()) && !string.IsNullOrEmpty(kr[1].Trim().Replace("\"",string.Empty)))
        {
          connStrPrts.Add(connStrPrt);
        }
      }

      return string.Join(";", connStrPrts.ToArray());
    }

    private void GPRMainForm_Load(object sender, EventArgs e)
    {

      this.Text = "GPR Data Integration v" + Assembly.GetExecutingAssembly().GetName().Version.ToString().Substring(0, 3);

    }

    private void GPRMainForm_Shown(object sender, EventArgs e)
    {

      this.tsConnect.Enabled = false;


      this.connect();


      this.tsConnect.Enabled = true;
    }

    private void releaseNotesToolStripMenuItem_Click(object sender, EventArgs e)
    {
      frmReleaseNotes notes = new frmReleaseNotes();
      notes.ShowDialog();
    }

    private void tsConnect_Click(object sender, EventArgs e)
    {
      this.tsConnect.Enabled = false;

      this.connect();

      this.tsConnect.Enabled = true;
    }
    #endregion
  }
}