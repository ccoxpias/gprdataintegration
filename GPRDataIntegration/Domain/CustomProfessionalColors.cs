﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace GPRDataIntegration.Domain
{
    //override the professional colors for custom toolstrips
    class CustomProfessionalColors : ProfessionalColorTable
    {
        public override Color ToolStripGradientBegin
        {
            get { return Color.LightSteelBlue; }
        }
        public override Color ToolStripGradientMiddle
        {
            get { return Color.LightSteelBlue; }
        }
        public override Color ToolStripGradientEnd
        {
            get { return Color.SteelBlue; }
        }

        public override Color MenuStripGradientBegin
        {
            get { return Color.Transparent; }
        }
        public override Color MenuStripGradientEnd
        {
            get { return Color.Transparent; }
        }
        public override Color ToolStripBorder
        {
            get { return Color.Transparent; }
        }
    }
}
