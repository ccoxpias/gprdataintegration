namespace GPRDataIntegration.Domain
{
  partial class GPRMainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GPRMainForm));
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.tsConnect = new System.Windows.Forms.ToolStripButton();
      this.ssStatus = new System.Windows.Forms.StatusStrip();
      this.sslbConncetion = new System.Windows.Forms.ToolStripStatusLabel();
      this.panel1 = new System.Windows.Forms.Panel();
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.exToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.releaseNotesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.ucGPRControl = new GPRControls.Domain.GPRControl();
      this.toolStrip1.SuspendLayout();
      this.ssStatus.SuspendLayout();
      this.panel1.SuspendLayout();
      this.menuStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // toolStrip1
      // 
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsConnect});
      this.toolStrip1.Location = new System.Drawing.Point(0, 24);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(1077, 25);
      this.toolStrip1.TabIndex = 0;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // tsConnect
      // 
      this.tsConnect.Image = ((System.Drawing.Image)(resources.GetObject("tsConnect.Image")));
      this.tsConnect.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsConnect.Name = "tsConnect";
      this.tsConnect.Size = new System.Drawing.Size(67, 22);
      this.tsConnect.Text = "Connect";
      this.tsConnect.ToolTipText = "Connect to database";
      this.tsConnect.Click += new System.EventHandler(this.tsConnect_Click);
      // 
      // ssStatus
      // 
      this.ssStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sslbConncetion});
      this.ssStatus.Location = new System.Drawing.Point(0, 683);
      this.ssStatus.Name = "ssStatus";
      this.ssStatus.Size = new System.Drawing.Size(1077, 22);
      this.ssStatus.TabIndex = 1;
      // 
      // sslbConncetion
      // 
      this.sslbConncetion.Image = ((System.Drawing.Image)(resources.GetObject("sslbConncetion.Image")));
      this.sslbConncetion.Name = "sslbConncetion";
      this.sslbConncetion.Size = new System.Drawing.Size(16, 17);
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.ucGPRControl);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(0, 49);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(1077, 634);
      this.panel1.TabIndex = 2;
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new System.Drawing.Size(1077, 24);
      this.menuStrip1.TabIndex = 3;
      this.menuStrip1.Text = "menuStrip1";
      // 
      // fileToolStripMenuItem
      // 
      this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exToolStripMenuItem});
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
      this.fileToolStripMenuItem.Text = "&File";
      // 
      // exToolStripMenuItem
      // 
      this.exToolStripMenuItem.Name = "exToolStripMenuItem";
      this.exToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
      this.exToolStripMenuItem.Text = "E&xit";
      this.exToolStripMenuItem.Click += new System.EventHandler(this.exToolStripMenuItem_Click);
      // 
      // helpToolStripMenuItem
      // 
      this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.releaseNotesToolStripMenuItem,
            this.aboutToolStripMenuItem});
      this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
      this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
      this.helpToolStripMenuItem.Text = "&Help";
      // 
      // releaseNotesToolStripMenuItem
      // 
      this.releaseNotesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("releaseNotesToolStripMenuItem.Image")));
      this.releaseNotesToolStripMenuItem.Name = "releaseNotesToolStripMenuItem";
      this.releaseNotesToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
      this.releaseNotesToolStripMenuItem.Text = "&Release Notes";
      this.releaseNotesToolStripMenuItem.Click += new System.EventHandler(this.releaseNotesToolStripMenuItem_Click);
      // 
      // aboutToolStripMenuItem
      // 
      this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
      this.aboutToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
      this.aboutToolStripMenuItem.Text = "&About";
      this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
      // 
      // ucGPRControl
      // 
      this.ucGPRControl.ConnectionString = "";
      this.ucGPRControl.DataMapper = null;
      this.ucGPRControl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ucGPRControl.Enabled = false;
      this.ucGPRControl.Location = new System.Drawing.Point(0, 0);
      this.ucGPRControl.Name = "ucGPRControl";
      this.ucGPRControl.Size = new System.Drawing.Size(1077, 634);
      this.ucGPRControl.TabIndex = 0;
      // 
      // GPRMainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1077, 705);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.ssStatus);
      this.Controls.Add(this.toolStrip1);
      this.Controls.Add(this.menuStrip1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MainMenuStrip = this.menuStrip1;
      this.Name = "GPRMainForm";
      this.Text = "GPR Data Integration v1.3";
      this.Load += new System.EventHandler(this.GPRMainForm_Load);
      this.Shown += new System.EventHandler(this.GPRMainForm_Shown);
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.ssStatus.ResumeLayout(false);
      this.ssStatus.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.StatusStrip ssStatus;
    private System.Windows.Forms.ToolStripButton tsConnect;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.ToolStripStatusLabel sslbConncetion;
    private GPRControls.Domain.GPRControl ucGPRControl;
    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem exToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem releaseNotesToolStripMenuItem;
  }
}

