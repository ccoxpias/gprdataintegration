namespace GPRControls.Domain
{
	partial class GPRControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GPRControl));
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
      this.panel1 = new System.Windows.Forms.Panel();
      this.tbControl = new System.Windows.Forms.TabControl();
      this.tpSetup = new System.Windows.Forms.TabPage();
      this.panel5 = new System.Windows.Forms.Panel();
      this.panel13 = new System.Windows.Forms.Panel();
      this.dgvSetup = new System.Windows.Forms.DataGridView();
      this.ssSetup = new System.Windows.Forms.StatusStrip();
      this.ssSetupStatus = new System.Windows.Forms.ToolStripStatusLabel();
      this.tsSetup = new System.Windows.Forms.ToolStrip();
      this.tsSetupLayerCode = new System.Windows.Forms.ToolStripButton();
      this.tsSetupVelocity = new System.Windows.Forms.ToolStripButton();
      this.tsSetupReset = new System.Windows.Forms.ToolStripButton();
      this.tpMarker = new System.Windows.Forms.TabPage();
      this.panel6 = new System.Windows.Forms.Panel();
      this.pnlMar = new System.Windows.Forms.Panel();
      this.dgvMars = new System.Windows.Forms.DataGridView();
      this.splitter4 = new System.Windows.Forms.Splitter();
      this.pnlMarCollection = new System.Windows.Forms.Panel();
      this.panel9 = new System.Windows.Forms.Panel();
      this.ucMarCollections = new GPRControls.Domain.DisplayCollections();
      this.splitter5 = new System.Windows.Forms.Splitter();
      this.ssMar = new System.Windows.Forms.StatusStrip();
      this.sstsMarProgress = new System.Windows.Forms.ToolStripProgressBar();
      this.sstsMarStatus = new System.Windows.Forms.ToolStripStatusLabel();
      this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
      this.sstsMarCollections = new System.Windows.Forms.ToolStripStatusLabel();
      this.tsMar = new System.Windows.Forms.ToolStrip();
      this.tsMarCollections = new System.Windows.Forms.ToolStripButton();
      this.tsMarCreate = new System.Windows.Forms.ToolStripButton();
      this.tsMarCreateCSV = new System.Windows.Forms.ToolStripButton();
      this.tsMarRefresh = new System.Windows.Forms.ToolStripButton();
      this.tsMarImportReportingInterval = new System.Windows.Forms.ToolStripButton();
      this.tpCoreRequest = new System.Windows.Forms.TabPage();
      this.panel7 = new System.Windows.Forms.Panel();
      this.pnlCoreRequest = new System.Windows.Forms.Panel();
      this.dgvCoreRequest = new System.Windows.Forms.DataGridView();
      this.splitter7 = new System.Windows.Forms.Splitter();
      this.pnlCoreReqCollections = new System.Windows.Forms.Panel();
      this.ucCoreReqCollections = new GPRControls.Domain.DisplayCollections();
      this.splitter6 = new System.Windows.Forms.Splitter();
      this.panel8 = new System.Windows.Forms.Panel();
      this.ssCoreRequest = new System.Windows.Forms.StatusStrip();
      this.sstsCoreRequestProgressBar = new System.Windows.Forms.ToolStripProgressBar();
      this.sstsCoreReqStatus = new System.Windows.Forms.ToolStripStatusLabel();
      this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
      this.sstsCoreReqCollections = new System.Windows.Forms.ToolStripStatusLabel();
      this.tsCoreRequest = new System.Windows.Forms.ToolStrip();
      this.tsCoreReqImport = new System.Windows.Forms.ToolStripButton();
      this.tsCoreReqGenerate = new System.Windows.Forms.ToolStripButton();
      this.tsCoreReqExport = new System.Windows.Forms.ToolStripButton();
      this.tsCoreReqDelete = new System.Windows.Forms.ToolStripButton();
      this.tsCoreReqRefresh = new System.Windows.Forms.ToolStripButton();
      this.tpCoreResult = new System.Windows.Forms.TabPage();
      this.panel2 = new System.Windows.Forms.Panel();
      this.pnlCoreResults = new System.Windows.Forms.Panel();
      this.pnlCoreRes = new System.Windows.Forms.Panel();
      this.dgvCoreResults = new System.Windows.Forms.DataGridView();
      this.splitter3 = new System.Windows.Forms.Splitter();
      this.pnlLayers = new System.Windows.Forms.Panel();
      this.lvLayers = new System.Windows.Forms.ListView();
      this.colLayer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.colThickness = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.colCharacteristic = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.splitter2 = new System.Windows.Forms.Splitter();
      this.pnlCoreResultCollection = new System.Windows.Forms.Panel();
      this.ucCoreResCollections = new GPRControls.Domain.DisplayCollections();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.panel3 = new System.Windows.Forms.Panel();
      this.ssCoreResult = new System.Windows.Forms.StatusStrip();
      this.sstsCoreResultProgressBar = new System.Windows.Forms.ToolStripProgressBar();
      this.sstsCoreResultStatus = new System.Windows.Forms.ToolStripStatusLabel();
      this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
      this.sstsCoreResultCollections = new System.Windows.Forms.ToolStripStatusLabel();
      this.tsCoreResult = new System.Windows.Forms.ToolStrip();
      this.tsCoreResImport = new System.Windows.Forms.ToolStripButton();
      this.tsCoreResSelect = new System.Windows.Forms.ToolStripButton();
      this.tsCoreResExport = new System.Windows.Forms.ToolStripButton();
      this.tsCoreResUpdateImagePath = new System.Windows.Forms.ToolStripButton();
      this.tsCoreResDelete = new System.Windows.Forms.ToolStripButton();
      this.tsCoreResReset = new System.Windows.Forms.ToolStripButton();
      this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
      this.tpGRPResults = new System.Windows.Forms.TabPage();
      this.panel4 = new System.Windows.Forms.Panel();
      this.panel11 = new System.Windows.Forms.Panel();
      this.panel12 = new System.Windows.Forms.Panel();
      this.dgvGPRResults = new System.Windows.Forms.DataGridView();
      this.splitter10 = new System.Windows.Forms.Splitter();
      this.panel10 = new System.Windows.Forms.Panel();
      this.ucGPRCollections = new GPRControls.Domain.DisplayCollections();
      this.ssGPRResults = new System.Windows.Forms.StatusStrip();
      this.sstsGPRProgressBar = new System.Windows.Forms.ToolStripProgressBar();
      this.sstsGPRStatus = new System.Windows.Forms.ToolStripStatusLabel();
      this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
      this.sstsGPRCollections = new System.Windows.Forms.ToolStripStatusLabel();
      this.tsGPRResults = new System.Windows.Forms.ToolStrip();
      this.tsGPRImport = new System.Windows.Forms.ToolStripButton();
      this.tsGPRProcess = new System.Windows.Forms.ToolStripButton();
      this.tsGPRReport = new System.Windows.Forms.ToolStripButton();
      this.tsGPRGenerateReport = new System.Windows.Forms.ToolStripDropDownButton();
      this.tsmuGPRAnalysis = new System.Windows.Forms.ToolStripMenuItem();
      this.tsmuThicknessReport = new System.Windows.Forms.ToolStripMenuItem();
      this.tsGPRSave = new System.Windows.Forms.ToolStripButton();
      this.tsGPRWarning = new System.Windows.Forms.ToolStripButton();
      this.tsGPRPick = new System.Windows.Forms.ToolStripButton();
      this.tsGPRReset = new System.Windows.Forms.ToolStripButton();
      this.tsResearch = new System.Windows.Forms.ToolStripDropDownButton();
      this.mnImportStation = new System.Windows.Forms.ToolStripMenuItem();
      this.mnReport = new System.Windows.Forms.ToolStripMenuItem();
      this.muPick = new System.Windows.Forms.ToolStripMenuItem();
      this.tsELMOD = new System.Windows.Forms.ToolStripButton();
      this.tpQCReports = new System.Windows.Forms.TabPage();
      this.dgvQCReports = new System.Windows.Forms.DataGridView();
      this.ssQCReports = new System.Windows.Forms.StatusStrip();
      this.sstsQCReportsProgressBar = new System.Windows.Forms.ToolStripProgressBar();
      this.tsslQCSelectedGroup = new System.Windows.Forms.ToolStripStatusLabel();
      this.tsQCReports = new System.Windows.Forms.ToolStrip();
      this.tsbQCGroupSelect = new System.Windows.Forms.ToolStripButton();
      this.tsbQCLaneAnalysis = new System.Windows.Forms.ToolStripButton();
      this.tsbClearTempTables = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
      this.imageList1 = new System.Windows.Forms.ImageList(this.components);
      this.fbSelectFolder = new System.Windows.Forms.FolderBrowserDialog();
      this.sfSaveFile = new System.Windows.Forms.SaveFileDialog();
      this.fdOpenFile = new System.Windows.Forms.OpenFileDialog();
      this.bgWorker = new System.ComponentModel.BackgroundWorker();
      this.panel1.SuspendLayout();
      this.tbControl.SuspendLayout();
      this.tpSetup.SuspendLayout();
      this.panel5.SuspendLayout();
      this.panel13.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvSetup)).BeginInit();
      this.ssSetup.SuspendLayout();
      this.tsSetup.SuspendLayout();
      this.tpMarker.SuspendLayout();
      this.panel6.SuspendLayout();
      this.pnlMar.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvMars)).BeginInit();
      this.pnlMarCollection.SuspendLayout();
      this.panel9.SuspendLayout();
      this.ssMar.SuspendLayout();
      this.tsMar.SuspendLayout();
      this.tpCoreRequest.SuspendLayout();
      this.panel7.SuspendLayout();
      this.pnlCoreRequest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvCoreRequest)).BeginInit();
      this.pnlCoreReqCollections.SuspendLayout();
      this.panel8.SuspendLayout();
      this.ssCoreRequest.SuspendLayout();
      this.tsCoreRequest.SuspendLayout();
      this.tpCoreResult.SuspendLayout();
      this.panel2.SuspendLayout();
      this.pnlCoreResults.SuspendLayout();
      this.pnlCoreRes.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvCoreResults)).BeginInit();
      this.pnlLayers.SuspendLayout();
      this.pnlCoreResultCollection.SuspendLayout();
      this.panel3.SuspendLayout();
      this.ssCoreResult.SuspendLayout();
      this.tsCoreResult.SuspendLayout();
      this.tpGRPResults.SuspendLayout();
      this.panel4.SuspendLayout();
      this.panel11.SuspendLayout();
      this.panel12.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvGPRResults)).BeginInit();
      this.panel10.SuspendLayout();
      this.ssGPRResults.SuspendLayout();
      this.tsGPRResults.SuspendLayout();
      this.tpQCReports.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvQCReports)).BeginInit();
      this.ssQCReports.SuspendLayout();
      this.tsQCReports.SuspendLayout();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.tbControl);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Margin = new System.Windows.Forms.Padding(4);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(1485, 721);
      this.panel1.TabIndex = 0;
      // 
      // tbControl
      // 
      this.tbControl.Controls.Add(this.tpSetup);
      this.tbControl.Controls.Add(this.tpMarker);
      this.tbControl.Controls.Add(this.tpCoreRequest);
      this.tbControl.Controls.Add(this.tpCoreResult);
      this.tbControl.Controls.Add(this.tpGRPResults);
      this.tbControl.Controls.Add(this.tpQCReports);
      this.tbControl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbControl.ImageList = this.imageList1;
      this.tbControl.Location = new System.Drawing.Point(0, 0);
      this.tbControl.Margin = new System.Windows.Forms.Padding(4);
      this.tbControl.Name = "tbControl";
      this.tbControl.SelectedIndex = 0;
      this.tbControl.Size = new System.Drawing.Size(1485, 721);
      this.tbControl.TabIndex = 0;
      this.tbControl.SelectedIndexChanged += new System.EventHandler(this.tbControl_SelectedIndexChanged);
      // 
      // tpSetup
      // 
      this.tpSetup.Controls.Add(this.panel5);
      this.tpSetup.Location = new System.Drawing.Point(4, 25);
      this.tpSetup.Margin = new System.Windows.Forms.Padding(4);
      this.tpSetup.Name = "tpSetup";
      this.tpSetup.Size = new System.Drawing.Size(1477, 692);
      this.tpSetup.TabIndex = 4;
      this.tpSetup.Text = "Setup";
      this.tpSetup.UseVisualStyleBackColor = true;
      // 
      // panel5
      // 
      this.panel5.Controls.Add(this.panel13);
      this.panel5.Controls.Add(this.ssSetup);
      this.panel5.Controls.Add(this.tsSetup);
      this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel5.Location = new System.Drawing.Point(0, 0);
      this.panel5.Margin = new System.Windows.Forms.Padding(4);
      this.panel5.Name = "panel5";
      this.panel5.Size = new System.Drawing.Size(1477, 692);
      this.panel5.TabIndex = 0;
      // 
      // panel13
      // 
      this.panel13.Controls.Add(this.dgvSetup);
      this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel13.Location = new System.Drawing.Point(0, 27);
      this.panel13.Margin = new System.Windows.Forms.Padding(4);
      this.panel13.Name = "panel13";
      this.panel13.Size = new System.Drawing.Size(1477, 643);
      this.panel13.TabIndex = 2;
      // 
      // dgvSetup
      // 
      this.dgvSetup.AllowUserToAddRows = false;
      this.dgvSetup.AllowUserToDeleteRows = false;
      this.dgvSetup.BackgroundColor = System.Drawing.Color.White;
      this.dgvSetup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvSetup.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvSetup.Location = new System.Drawing.Point(0, 0);
      this.dgvSetup.Margin = new System.Windows.Forms.Padding(4);
      this.dgvSetup.Name = "dgvSetup";
      this.dgvSetup.ReadOnly = true;
      this.dgvSetup.Size = new System.Drawing.Size(1477, 643);
      this.dgvSetup.TabIndex = 0;
      // 
      // ssSetup
      // 
      this.ssSetup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ssSetupStatus});
      this.ssSetup.Location = new System.Drawing.Point(0, 670);
      this.ssSetup.Name = "ssSetup";
      this.ssSetup.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
      this.ssSetup.Size = new System.Drawing.Size(1477, 22);
      this.ssSetup.SizingGrip = false;
      this.ssSetup.TabIndex = 1;
      // 
      // ssSetupStatus
      // 
      this.ssSetupStatus.Name = "ssSetupStatus";
      this.ssSetupStatus.Size = new System.Drawing.Size(0, 17);
      // 
      // tsSetup
      // 
      this.tsSetup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSetupLayerCode,
            this.tsSetupVelocity,
            this.tsSetupReset});
      this.tsSetup.Location = new System.Drawing.Point(0, 0);
      this.tsSetup.Name = "tsSetup";
      this.tsSetup.Size = new System.Drawing.Size(1477, 27);
      this.tsSetup.TabIndex = 0;
      this.tsSetup.Text = "toolStrip1";
      this.tsSetup.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsSetup_ItemClicked);
      // 
      // tsSetupLayerCode
      // 
      this.tsSetupLayerCode.Image = ((System.Drawing.Image)(resources.GetObject("tsSetupLayerCode.Image")));
      this.tsSetupLayerCode.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsSetupLayerCode.Name = "tsSetupLayerCode";
      this.tsSetupLayerCode.Size = new System.Drawing.Size(109, 24);
      this.tsSetupLayerCode.Tag = "Layer Codes";
      this.tsSetupLayerCode.Text = "Layer Codes";
      this.tsSetupLayerCode.ToolTipText = "Import Layer Code";
      // 
      // tsSetupVelocity
      // 
      this.tsSetupVelocity.Image = ((System.Drawing.Image)(resources.GetObject("tsSetupVelocity.Image")));
      this.tsSetupVelocity.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsSetupVelocity.Name = "tsSetupVelocity";
      this.tsSetupVelocity.Size = new System.Drawing.Size(93, 24);
      this.tsSetupVelocity.Tag = "Velocities";
      this.tsSetupVelocity.Text = "Velocities";
      this.tsSetupVelocity.ToolTipText = "Import Layer Velocities";
      // 
      // tsSetupReset
      // 
      this.tsSetupReset.Image = ((System.Drawing.Image)(resources.GetObject("tsSetupReset.Image")));
      this.tsSetupReset.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsSetupReset.Name = "tsSetupReset";
      this.tsSetupReset.Size = new System.Drawing.Size(65, 24);
      this.tsSetupReset.Tag = "Reset";
      this.tsSetupReset.Text = "Reset";
      this.tsSetupReset.ToolTipText = "Reset";
      // 
      // tpMarker
      // 
      this.tpMarker.Controls.Add(this.panel6);
      this.tpMarker.Controls.Add(this.tsMar);
      this.tpMarker.Location = new System.Drawing.Point(4, 25);
      this.tpMarker.Margin = new System.Windows.Forms.Padding(4);
      this.tpMarker.Name = "tpMarker";
      this.tpMarker.Size = new System.Drawing.Size(1477, 692);
      this.tpMarker.TabIndex = 0;
      this.tpMarker.Tag = "MAR";
      this.tpMarker.Text = "MAR";
      this.tpMarker.UseVisualStyleBackColor = true;
      // 
      // panel6
      // 
      this.panel6.Controls.Add(this.pnlMar);
      this.panel6.Controls.Add(this.splitter4);
      this.panel6.Controls.Add(this.pnlMarCollection);
      this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel6.Location = new System.Drawing.Point(0, 27);
      this.panel6.Margin = new System.Windows.Forms.Padding(4);
      this.panel6.Name = "panel6";
      this.panel6.Size = new System.Drawing.Size(1477, 665);
      this.panel6.TabIndex = 2;
      // 
      // pnlMar
      // 
      this.pnlMar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlMar.Controls.Add(this.dgvMars);
      this.pnlMar.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlMar.Location = new System.Drawing.Point(0, 0);
      this.pnlMar.Margin = new System.Windows.Forms.Padding(4);
      this.pnlMar.Name = "pnlMar";
      this.pnlMar.Size = new System.Drawing.Size(1477, 401);
      this.pnlMar.TabIndex = 3;
      // 
      // dgvMars
      // 
      this.dgvMars.AllowUserToAddRows = false;
      this.dgvMars.AllowUserToDeleteRows = false;
      this.dgvMars.AllowUserToOrderColumns = true;
      this.dgvMars.BackgroundColor = System.Drawing.Color.White;
      this.dgvMars.BorderStyle = System.Windows.Forms.BorderStyle.None;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvMars.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgvMars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgvMars.DefaultCellStyle = dataGridViewCellStyle2;
      this.dgvMars.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvMars.Location = new System.Drawing.Point(0, 0);
      this.dgvMars.Margin = new System.Windows.Forms.Padding(4);
      this.dgvMars.Name = "dgvMars";
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvMars.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
      this.dgvMars.Size = new System.Drawing.Size(1475, 399);
      this.dgvMars.TabIndex = 0;
      // 
      // splitter4
      // 
      this.splitter4.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter4.Location = new System.Drawing.Point(0, 401);
      this.splitter4.Margin = new System.Windows.Forms.Padding(4);
      this.splitter4.Name = "splitter4";
      this.splitter4.Size = new System.Drawing.Size(1477, 4);
      this.splitter4.TabIndex = 2;
      this.splitter4.TabStop = false;
      // 
      // pnlMarCollection
      // 
      this.pnlMarCollection.Controls.Add(this.panel9);
      this.pnlMarCollection.Controls.Add(this.splitter5);
      this.pnlMarCollection.Controls.Add(this.ssMar);
      this.pnlMarCollection.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlMarCollection.Location = new System.Drawing.Point(0, 405);
      this.pnlMarCollection.Margin = new System.Windows.Forms.Padding(4);
      this.pnlMarCollection.Name = "pnlMarCollection";
      this.pnlMarCollection.Size = new System.Drawing.Size(1477, 260);
      this.pnlMarCollection.TabIndex = 1;
      // 
      // panel9
      // 
      this.panel9.Controls.Add(this.ucMarCollections);
      this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel9.Location = new System.Drawing.Point(0, 0);
      this.panel9.Margin = new System.Windows.Forms.Padding(4);
      this.panel9.Name = "panel9";
      this.panel9.Size = new System.Drawing.Size(1477, 234);
      this.panel9.TabIndex = 3;
      // 
      // ucMarCollections
      // 
      this.ucMarCollections.DataSource = null;
      this.ucMarCollections.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ucMarCollections.Location = new System.Drawing.Point(0, 0);
      this.ucMarCollections.Margin = new System.Windows.Forms.Padding(5);
      this.ucMarCollections.Name = "ucMarCollections";
      this.ucMarCollections.Size = new System.Drawing.Size(1477, 234);
      this.ucMarCollections.TabIndex = 0;
      this.ucMarCollections.Title = "Collections";
      // 
      // splitter5
      // 
      this.splitter5.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter5.Location = new System.Drawing.Point(0, 234);
      this.splitter5.Margin = new System.Windows.Forms.Padding(4);
      this.splitter5.Name = "splitter5";
      this.splitter5.Size = new System.Drawing.Size(1477, 4);
      this.splitter5.TabIndex = 2;
      this.splitter5.TabStop = false;
      // 
      // ssMar
      // 
      this.ssMar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sstsMarProgress,
            this.sstsMarStatus,
            this.toolStripStatusLabel1,
            this.sstsMarCollections});
      this.ssMar.Location = new System.Drawing.Point(0, 238);
      this.ssMar.Name = "ssMar";
      this.ssMar.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
      this.ssMar.Size = new System.Drawing.Size(1477, 22);
      this.ssMar.SizingGrip = false;
      this.ssMar.TabIndex = 1;
      // 
      // sstsMarProgress
      // 
      this.sstsMarProgress.Name = "sstsMarProgress";
      this.sstsMarProgress.Size = new System.Drawing.Size(133, 20);
      this.sstsMarProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
      this.sstsMarProgress.Visible = false;
      // 
      // sstsMarStatus
      // 
      this.sstsMarStatus.Name = "sstsMarStatus";
      this.sstsMarStatus.Size = new System.Drawing.Size(0, 17);
      // 
      // toolStripStatusLabel1
      // 
      this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
      this.toolStripStatusLabel1.Size = new System.Drawing.Size(1457, 17);
      this.toolStripStatusLabel1.Spring = true;
      // 
      // sstsMarCollections
      // 
      this.sstsMarCollections.Name = "sstsMarCollections";
      this.sstsMarCollections.Size = new System.Drawing.Size(0, 17);
      // 
      // tsMar
      // 
      this.tsMar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMarCollections,
            this.tsMarCreate,
            this.tsMarCreateCSV,
            this.tsMarRefresh,
            this.tsMarImportReportingInterval});
      this.tsMar.Location = new System.Drawing.Point(0, 0);
      this.tsMar.Name = "tsMar";
      this.tsMar.Size = new System.Drawing.Size(1477, 27);
      this.tsMar.TabIndex = 0;
      this.tsMar.Text = "toolStrip1";
      this.tsMar.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsMar_ItemClicked);
      // 
      // tsMarCollections
      // 
      this.tsMarCollections.Image = ((System.Drawing.Image)(resources.GetObject("tsMarCollections.Image")));
      this.tsMarCollections.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsMarCollections.Name = "tsMarCollections";
      this.tsMarCollections.Size = new System.Drawing.Size(69, 24);
      this.tsMarCollections.Tag = "Select";
      this.tsMarCollections.Text = "Select";
      this.tsMarCollections.ToolTipText = "Select Collections and Display MAR data";
      // 
      // tsMarCreate
      // 
      this.tsMarCreate.Image = ((System.Drawing.Image)(resources.GetObject("tsMarCreate.Image")));
      this.tsMarCreate.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsMarCreate.Name = "tsMarCreate";
      this.tsMarCreate.Size = new System.Drawing.Size(72, 24);
      this.tsMarCreate.Tag = "Create";
      this.tsMarCreate.Text = "Create";
      this.tsMarCreate.ToolTipText = "Create MAR Files";
      // 
      // tsMarCreateCSV
      // 
      this.tsMarCreateCSV.Image = ((System.Drawing.Image)(resources.GetObject("tsMarCreateCSV.Image")));
      this.tsMarCreateCSV.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsMarCreateCSV.Name = "tsMarCreateCSV";
      this.tsMarCreateCSV.Size = new System.Drawing.Size(102, 24);
      this.tsMarCreateCSV.Tag = "Create CSV";
      this.tsMarCreateCSV.Text = "Create CSV";
      this.tsMarCreateCSV.ToolTipText = "Create MAR CSV File";
      // 
      // tsMarRefresh
      // 
      this.tsMarRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsMarRefresh.Image")));
      this.tsMarRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsMarRefresh.Name = "tsMarRefresh";
      this.tsMarRefresh.Size = new System.Drawing.Size(65, 24);
      this.tsMarRefresh.Tag = "Reset";
      this.tsMarRefresh.Text = "Reset";
      this.tsMarRefresh.ToolTipText = "Reset";
      // 
      // tsMarImportReportingInterval
      // 
      this.tsMarImportReportingInterval.Image = global::GPRControl.Properties.Resources.TableHS;
      this.tsMarImportReportingInterval.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsMarImportReportingInterval.Name = "tsMarImportReportingInterval";
      this.tsMarImportReportingInterval.Size = new System.Drawing.Size(197, 24);
      this.tsMarImportReportingInterval.Tag = "Import Reporting Interval";
      this.tsMarImportReportingInterval.Text = "Import Reporting Interval";
      // 
      // tpCoreRequest
      // 
      this.tpCoreRequest.Controls.Add(this.panel7);
      this.tpCoreRequest.Controls.Add(this.tsCoreRequest);
      this.tpCoreRequest.Location = new System.Drawing.Point(4, 25);
      this.tpCoreRequest.Margin = new System.Windows.Forms.Padding(4);
      this.tpCoreRequest.Name = "tpCoreRequest";
      this.tpCoreRequest.Size = new System.Drawing.Size(1477, 692);
      this.tpCoreRequest.TabIndex = 1;
      this.tpCoreRequest.Tag = "Core Request";
      this.tpCoreRequest.Text = "Core Requests";
      this.tpCoreRequest.UseVisualStyleBackColor = true;
      // 
      // panel7
      // 
      this.panel7.Controls.Add(this.pnlCoreRequest);
      this.panel7.Controls.Add(this.splitter7);
      this.panel7.Controls.Add(this.pnlCoreReqCollections);
      this.panel7.Controls.Add(this.splitter6);
      this.panel7.Controls.Add(this.panel8);
      this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel7.Location = new System.Drawing.Point(0, 27);
      this.panel7.Margin = new System.Windows.Forms.Padding(4);
      this.panel7.Name = "panel7";
      this.panel7.Size = new System.Drawing.Size(1477, 665);
      this.panel7.TabIndex = 2;
      // 
      // pnlCoreRequest
      // 
      this.pnlCoreRequest.Controls.Add(this.dgvCoreRequest);
      this.pnlCoreRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlCoreRequest.Location = new System.Drawing.Point(0, 0);
      this.pnlCoreRequest.Margin = new System.Windows.Forms.Padding(4);
      this.pnlCoreRequest.Name = "pnlCoreRequest";
      this.pnlCoreRequest.Size = new System.Drawing.Size(1477, 404);
      this.pnlCoreRequest.TabIndex = 5;
      // 
      // dgvCoreRequest
      // 
      this.dgvCoreRequest.AllowUserToAddRows = false;
      this.dgvCoreRequest.AllowUserToDeleteRows = false;
      this.dgvCoreRequest.BackgroundColor = System.Drawing.Color.White;
      dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvCoreRequest.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
      this.dgvCoreRequest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
      dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgvCoreRequest.DefaultCellStyle = dataGridViewCellStyle5;
      this.dgvCoreRequest.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvCoreRequest.Location = new System.Drawing.Point(0, 0);
      this.dgvCoreRequest.Margin = new System.Windows.Forms.Padding(4);
      this.dgvCoreRequest.Name = "dgvCoreRequest";
      this.dgvCoreRequest.ReadOnly = true;
      dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvCoreRequest.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
      this.dgvCoreRequest.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgvCoreRequest.Size = new System.Drawing.Size(1477, 404);
      this.dgvCoreRequest.TabIndex = 0;
      // 
      // splitter7
      // 
      this.splitter7.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter7.Location = new System.Drawing.Point(0, 404);
      this.splitter7.Margin = new System.Windows.Forms.Padding(4);
      this.splitter7.Name = "splitter7";
      this.splitter7.Size = new System.Drawing.Size(1477, 4);
      this.splitter7.TabIndex = 4;
      this.splitter7.TabStop = false;
      // 
      // pnlCoreReqCollections
      // 
      this.pnlCoreReqCollections.Controls.Add(this.ucCoreReqCollections);
      this.pnlCoreReqCollections.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlCoreReqCollections.Location = new System.Drawing.Point(0, 408);
      this.pnlCoreReqCollections.Margin = new System.Windows.Forms.Padding(4);
      this.pnlCoreReqCollections.Name = "pnlCoreReqCollections";
      this.pnlCoreReqCollections.Size = new System.Drawing.Size(1477, 225);
      this.pnlCoreReqCollections.TabIndex = 3;
      // 
      // ucCoreReqCollections
      // 
      this.ucCoreReqCollections.DataSource = null;
      this.ucCoreReqCollections.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ucCoreReqCollections.Location = new System.Drawing.Point(0, 0);
      this.ucCoreReqCollections.Margin = new System.Windows.Forms.Padding(5);
      this.ucCoreReqCollections.Name = "ucCoreReqCollections";
      this.ucCoreReqCollections.Size = new System.Drawing.Size(1477, 225);
      this.ucCoreReqCollections.TabIndex = 0;
      this.ucCoreReqCollections.Title = "Collections";
      // 
      // splitter6
      // 
      this.splitter6.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter6.Location = new System.Drawing.Point(0, 633);
      this.splitter6.Margin = new System.Windows.Forms.Padding(4);
      this.splitter6.Name = "splitter6";
      this.splitter6.Size = new System.Drawing.Size(1477, 2);
      this.splitter6.TabIndex = 2;
      this.splitter6.TabStop = false;
      // 
      // panel8
      // 
      this.panel8.Controls.Add(this.ssCoreRequest);
      this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel8.Location = new System.Drawing.Point(0, 635);
      this.panel8.Margin = new System.Windows.Forms.Padding(4);
      this.panel8.Name = "panel8";
      this.panel8.Size = new System.Drawing.Size(1477, 30);
      this.panel8.TabIndex = 1;
      // 
      // ssCoreRequest
      // 
      this.ssCoreRequest.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sstsCoreRequestProgressBar,
            this.sstsCoreReqStatus,
            this.toolStripStatusLabel2,
            this.sstsCoreReqCollections});
      this.ssCoreRequest.Location = new System.Drawing.Point(0, 8);
      this.ssCoreRequest.Name = "ssCoreRequest";
      this.ssCoreRequest.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
      this.ssCoreRequest.Size = new System.Drawing.Size(1477, 22);
      this.ssCoreRequest.SizingGrip = false;
      this.ssCoreRequest.TabIndex = 0;
      // 
      // sstsCoreRequestProgressBar
      // 
      this.sstsCoreRequestProgressBar.Name = "sstsCoreRequestProgressBar";
      this.sstsCoreRequestProgressBar.Size = new System.Drawing.Size(133, 20);
      this.sstsCoreRequestProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
      this.sstsCoreRequestProgressBar.Visible = false;
      // 
      // sstsCoreReqStatus
      // 
      this.sstsCoreReqStatus.Name = "sstsCoreReqStatus";
      this.sstsCoreReqStatus.Size = new System.Drawing.Size(0, 17);
      // 
      // toolStripStatusLabel2
      // 
      this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
      this.toolStripStatusLabel2.Size = new System.Drawing.Size(1457, 17);
      this.toolStripStatusLabel2.Spring = true;
      // 
      // sstsCoreReqCollections
      // 
      this.sstsCoreReqCollections.Name = "sstsCoreReqCollections";
      this.sstsCoreReqCollections.Size = new System.Drawing.Size(0, 17);
      // 
      // tsCoreRequest
      // 
      this.tsCoreRequest.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsCoreReqImport,
            this.tsCoreReqGenerate,
            this.tsCoreReqExport,
            this.tsCoreReqDelete,
            this.tsCoreReqRefresh});
      this.tsCoreRequest.Location = new System.Drawing.Point(0, 0);
      this.tsCoreRequest.Name = "tsCoreRequest";
      this.tsCoreRequest.Size = new System.Drawing.Size(1477, 27);
      this.tsCoreRequest.TabIndex = 0;
      this.tsCoreRequest.Text = "toolStrip1";
      this.tsCoreRequest.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsCoreRequest_ItemClicked);
      // 
      // tsCoreReqImport
      // 
      this.tsCoreReqImport.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreReqImport.Image")));
      this.tsCoreReqImport.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreReqImport.Name = "tsCoreReqImport";
      this.tsCoreReqImport.Size = new System.Drawing.Size(74, 24);
      this.tsCoreReqImport.Tag = "Import";
      this.tsCoreReqImport.Text = "Import";
      this.tsCoreReqImport.ToolTipText = "Import Raw Core Requests";
      // 
      // tsCoreReqGenerate
      // 
      this.tsCoreReqGenerate.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreReqGenerate.Image")));
      this.tsCoreReqGenerate.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreReqGenerate.Name = "tsCoreReqGenerate";
      this.tsCoreReqGenerate.Size = new System.Drawing.Size(89, 24);
      this.tsCoreReqGenerate.Tag = "Generate";
      this.tsCoreReqGenerate.Text = "Generate";
      this.tsCoreReqGenerate.ToolTipText = "Generate Core Requests";
      // 
      // tsCoreReqExport
      // 
      this.tsCoreReqExport.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreReqExport.Image")));
      this.tsCoreReqExport.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreReqExport.Name = "tsCoreReqExport";
      this.tsCoreReqExport.Size = new System.Drawing.Size(72, 24);
      this.tsCoreReqExport.Tag = "Export";
      this.tsCoreReqExport.Text = "Export";
      this.tsCoreReqExport.ToolTipText = "Export Core Requests";
      // 
      // tsCoreReqDelete
      // 
      this.tsCoreReqDelete.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreReqDelete.Image")));
      this.tsCoreReqDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreReqDelete.Name = "tsCoreReqDelete";
      this.tsCoreReqDelete.Size = new System.Drawing.Size(73, 24);
      this.tsCoreReqDelete.Tag = "Delete";
      this.tsCoreReqDelete.Text = "Delete";
      this.tsCoreReqDelete.ToolTipText = "Delete Core Requests";
      // 
      // tsCoreReqRefresh
      // 
      this.tsCoreReqRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreReqRefresh.Image")));
      this.tsCoreReqRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreReqRefresh.Name = "tsCoreReqRefresh";
      this.tsCoreReqRefresh.Size = new System.Drawing.Size(65, 24);
      this.tsCoreReqRefresh.Tag = "Reset";
      this.tsCoreReqRefresh.Text = "Reset";
      this.tsCoreReqRefresh.ToolTipText = "Reset";
      // 
      // tpCoreResult
      // 
      this.tpCoreResult.Controls.Add(this.panel2);
      this.tpCoreResult.Controls.Add(this.tsCoreResult);
      this.tpCoreResult.ImageKey = "(none)";
      this.tpCoreResult.Location = new System.Drawing.Point(4, 25);
      this.tpCoreResult.Margin = new System.Windows.Forms.Padding(4);
      this.tpCoreResult.Name = "tpCoreResult";
      this.tpCoreResult.Size = new System.Drawing.Size(1477, 692);
      this.tpCoreResult.TabIndex = 2;
      this.tpCoreResult.Tag = "Core Result";
      this.tpCoreResult.Text = "Core Results";
      this.tpCoreResult.UseVisualStyleBackColor = true;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.pnlCoreResults);
      this.panel2.Controls.Add(this.splitter2);
      this.panel2.Controls.Add(this.pnlCoreResultCollection);
      this.panel2.Controls.Add(this.splitter1);
      this.panel2.Controls.Add(this.panel3);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel2.Location = new System.Drawing.Point(0, 27);
      this.panel2.Margin = new System.Windows.Forms.Padding(4);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(1477, 665);
      this.panel2.TabIndex = 1;
      // 
      // pnlCoreResults
      // 
      this.pnlCoreResults.Controls.Add(this.pnlCoreRes);
      this.pnlCoreResults.Controls.Add(this.splitter3);
      this.pnlCoreResults.Controls.Add(this.pnlLayers);
      this.pnlCoreResults.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlCoreResults.Location = new System.Drawing.Point(0, 0);
      this.pnlCoreResults.Margin = new System.Windows.Forms.Padding(4);
      this.pnlCoreResults.Name = "pnlCoreResults";
      this.pnlCoreResults.Size = new System.Drawing.Size(1477, 355);
      this.pnlCoreResults.TabIndex = 4;
      // 
      // pnlCoreRes
      // 
      this.pnlCoreRes.Controls.Add(this.dgvCoreResults);
      this.pnlCoreRes.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlCoreRes.Location = new System.Drawing.Point(0, 0);
      this.pnlCoreRes.Margin = new System.Windows.Forms.Padding(4);
      this.pnlCoreRes.Name = "pnlCoreRes";
      this.pnlCoreRes.Size = new System.Drawing.Size(1045, 355);
      this.pnlCoreRes.TabIndex = 5;
      // 
      // dgvCoreResults
      // 
      this.dgvCoreResults.AllowUserToAddRows = false;
      this.dgvCoreResults.AllowUserToDeleteRows = false;
      this.dgvCoreResults.BackgroundColor = System.Drawing.Color.White;
      dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvCoreResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
      this.dgvCoreResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
      dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgvCoreResults.DefaultCellStyle = dataGridViewCellStyle8;
      this.dgvCoreResults.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvCoreResults.Location = new System.Drawing.Point(0, 0);
      this.dgvCoreResults.Margin = new System.Windows.Forms.Padding(4);
      this.dgvCoreResults.Name = "dgvCoreResults";
      this.dgvCoreResults.ReadOnly = true;
      this.dgvCoreResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgvCoreResults.Size = new System.Drawing.Size(1045, 355);
      this.dgvCoreResults.TabIndex = 0;
      this.dgvCoreResults.SelectionChanged += new System.EventHandler(this.dgvCoreResults_SelectionChanged);
      // 
      // splitter3
      // 
      this.splitter3.Dock = System.Windows.Forms.DockStyle.Right;
      this.splitter3.Location = new System.Drawing.Point(1045, 0);
      this.splitter3.Margin = new System.Windows.Forms.Padding(4);
      this.splitter3.Name = "splitter3";
      this.splitter3.Size = new System.Drawing.Size(4, 355);
      this.splitter3.TabIndex = 4;
      this.splitter3.TabStop = false;
      // 
      // pnlLayers
      // 
      this.pnlLayers.Controls.Add(this.lvLayers);
      this.pnlLayers.Dock = System.Windows.Forms.DockStyle.Right;
      this.pnlLayers.Location = new System.Drawing.Point(1049, 0);
      this.pnlLayers.Margin = new System.Windows.Forms.Padding(4);
      this.pnlLayers.Name = "pnlLayers";
      this.pnlLayers.Size = new System.Drawing.Size(428, 355);
      this.pnlLayers.TabIndex = 3;
      this.pnlLayers.Visible = false;
      // 
      // lvLayers
      // 
      this.lvLayers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colLayer,
            this.colThickness,
            this.colCharacteristic});
      this.lvLayers.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lvLayers.FullRowSelect = true;
      this.lvLayers.GridLines = true;
      this.lvLayers.Location = new System.Drawing.Point(0, 0);
      this.lvLayers.Margin = new System.Windows.Forms.Padding(4);
      this.lvLayers.Name = "lvLayers";
      this.lvLayers.Size = new System.Drawing.Size(428, 355);
      this.lvLayers.TabIndex = 2;
      this.lvLayers.UseCompatibleStateImageBehavior = false;
      this.lvLayers.View = System.Windows.Forms.View.Details;
      // 
      // colLayer
      // 
      this.colLayer.Text = "Layer";
      this.colLayer.Width = 86;
      // 
      // colThickness
      // 
      this.colThickness.Text = "Thickness";
      this.colThickness.Width = 84;
      // 
      // colCharacteristic
      // 
      this.colCharacteristic.Text = "Characteristic";
      this.colCharacteristic.Width = 150;
      // 
      // splitter2
      // 
      this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter2.Location = new System.Drawing.Point(0, 355);
      this.splitter2.Margin = new System.Windows.Forms.Padding(4);
      this.splitter2.Name = "splitter2";
      this.splitter2.Size = new System.Drawing.Size(1477, 4);
      this.splitter2.TabIndex = 3;
      this.splitter2.TabStop = false;
      // 
      // pnlCoreResultCollection
      // 
      this.pnlCoreResultCollection.Controls.Add(this.ucCoreResCollections);
      this.pnlCoreResultCollection.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlCoreResultCollection.Location = new System.Drawing.Point(0, 359);
      this.pnlCoreResultCollection.Margin = new System.Windows.Forms.Padding(4);
      this.pnlCoreResultCollection.Name = "pnlCoreResultCollection";
      this.pnlCoreResultCollection.Size = new System.Drawing.Size(1477, 272);
      this.pnlCoreResultCollection.TabIndex = 2;
      // 
      // ucCoreResCollections
      // 
      this.ucCoreResCollections.DataSource = null;
      this.ucCoreResCollections.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ucCoreResCollections.Location = new System.Drawing.Point(0, 0);
      this.ucCoreResCollections.Margin = new System.Windows.Forms.Padding(5);
      this.ucCoreResCollections.Name = "ucCoreResCollections";
      this.ucCoreResCollections.Size = new System.Drawing.Size(1477, 272);
      this.ucCoreResCollections.TabIndex = 0;
      this.ucCoreResCollections.Title = "Collections";
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter1.Location = new System.Drawing.Point(0, 631);
      this.splitter1.Margin = new System.Windows.Forms.Padding(4);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(1477, 4);
      this.splitter1.TabIndex = 1;
      this.splitter1.TabStop = false;
      // 
      // panel3
      // 
      this.panel3.Controls.Add(this.ssCoreResult);
      this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel3.Location = new System.Drawing.Point(0, 635);
      this.panel3.Margin = new System.Windows.Forms.Padding(4);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(1477, 30);
      this.panel3.TabIndex = 0;
      // 
      // ssCoreResult
      // 
      this.ssCoreResult.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sstsCoreResultProgressBar,
            this.sstsCoreResultStatus,
            this.toolStripStatusLabel3,
            this.sstsCoreResultCollections});
      this.ssCoreResult.Location = new System.Drawing.Point(0, 8);
      this.ssCoreResult.Name = "ssCoreResult";
      this.ssCoreResult.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
      this.ssCoreResult.Size = new System.Drawing.Size(1477, 22);
      this.ssCoreResult.SizingGrip = false;
      this.ssCoreResult.TabIndex = 0;
      this.ssCoreResult.Text = "statusStrip1";
      // 
      // sstsCoreResultProgressBar
      // 
      this.sstsCoreResultProgressBar.Name = "sstsCoreResultProgressBar";
      this.sstsCoreResultProgressBar.Size = new System.Drawing.Size(133, 20);
      this.sstsCoreResultProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
      this.sstsCoreResultProgressBar.Visible = false;
      // 
      // sstsCoreResultStatus
      // 
      this.sstsCoreResultStatus.Name = "sstsCoreResultStatus";
      this.sstsCoreResultStatus.Size = new System.Drawing.Size(0, 17);
      // 
      // toolStripStatusLabel3
      // 
      this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
      this.toolStripStatusLabel3.Size = new System.Drawing.Size(1457, 17);
      this.toolStripStatusLabel3.Spring = true;
      // 
      // sstsCoreResultCollections
      // 
      this.sstsCoreResultCollections.Name = "sstsCoreResultCollections";
      this.sstsCoreResultCollections.Size = new System.Drawing.Size(0, 17);
      // 
      // tsCoreResult
      // 
      this.tsCoreResult.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsCoreResImport,
            this.tsCoreResSelect,
            this.tsCoreResExport,
            this.tsCoreResUpdateImagePath,
            this.tsCoreResDelete,
            this.tsCoreResReset,
            this.toolStripButton1});
      this.tsCoreResult.Location = new System.Drawing.Point(0, 0);
      this.tsCoreResult.Name = "tsCoreResult";
      this.tsCoreResult.Size = new System.Drawing.Size(1477, 27);
      this.tsCoreResult.TabIndex = 0;
      this.tsCoreResult.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsCoreResult_ItemClicked);
      // 
      // tsCoreResImport
      // 
      this.tsCoreResImport.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreResImport.Image")));
      this.tsCoreResImport.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreResImport.Name = "tsCoreResImport";
      this.tsCoreResImport.Size = new System.Drawing.Size(74, 24);
      this.tsCoreResImport.Tag = "Import";
      this.tsCoreResImport.Text = "Import";
      this.tsCoreResImport.ToolTipText = "Import Core Results";
      // 
      // tsCoreResSelect
      // 
      this.tsCoreResSelect.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreResSelect.Image")));
      this.tsCoreResSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreResSelect.Name = "tsCoreResSelect";
      this.tsCoreResSelect.Size = new System.Drawing.Size(69, 24);
      this.tsCoreResSelect.Tag = "Select";
      this.tsCoreResSelect.Text = "Select";
      this.tsCoreResSelect.ToolTipText = "Select Collections and Display Core Results";
      // 
      // tsCoreResExport
      // 
      this.tsCoreResExport.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreResExport.Image")));
      this.tsCoreResExport.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreResExport.Name = "tsCoreResExport";
      this.tsCoreResExport.Size = new System.Drawing.Size(72, 24);
      this.tsCoreResExport.Tag = "Create";
      this.tsCoreResExport.Text = "Create";
      this.tsCoreResExport.ToolTipText = "Create COR Files";
      // 
      // tsCoreResUpdateImagePath
      // 
      this.tsCoreResUpdateImagePath.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreResUpdateImagePath.Image")));
      this.tsCoreResUpdateImagePath.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreResUpdateImagePath.Name = "tsCoreResUpdateImagePath";
      this.tsCoreResUpdateImagePath.Size = new System.Drawing.Size(129, 24);
      this.tsCoreResUpdateImagePath.Tag = "Set Image Path";
      this.tsCoreResUpdateImagePath.Text = "Set Image Path";
      // 
      // tsCoreResDelete
      // 
      this.tsCoreResDelete.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreResDelete.Image")));
      this.tsCoreResDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreResDelete.Name = "tsCoreResDelete";
      this.tsCoreResDelete.Size = new System.Drawing.Size(73, 24);
      this.tsCoreResDelete.Tag = "Delete";
      this.tsCoreResDelete.Text = "Delete";
      this.tsCoreResDelete.ToolTipText = "Delete Core Results";
      // 
      // tsCoreResReset
      // 
      this.tsCoreResReset.Image = ((System.Drawing.Image)(resources.GetObject("tsCoreResReset.Image")));
      this.tsCoreResReset.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsCoreResReset.Name = "tsCoreResReset";
      this.tsCoreResReset.Size = new System.Drawing.Size(65, 24);
      this.tsCoreResReset.Tag = "Reset";
      this.tsCoreResReset.Text = "Reset";
      // 
      // toolStripButton1
      // 
      this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
      this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton1.Name = "toolStripButton1";
      this.toolStripButton1.Size = new System.Drawing.Size(129, 24);
      this.toolStripButton1.Tag = "Import Existing";
      this.toolStripButton1.Text = "Import Existing";
      // 
      // tpGRPResults
      // 
      this.tpGRPResults.Controls.Add(this.panel4);
      this.tpGRPResults.Controls.Add(this.tsGPRResults);
      this.tpGRPResults.Location = new System.Drawing.Point(4, 25);
      this.tpGRPResults.Margin = new System.Windows.Forms.Padding(4);
      this.tpGRPResults.Name = "tpGRPResults";
      this.tpGRPResults.Size = new System.Drawing.Size(1477, 692);
      this.tpGRPResults.TabIndex = 3;
      this.tpGRPResults.Text = "GPR Results";
      this.tpGRPResults.UseVisualStyleBackColor = true;
      // 
      // panel4
      // 
      this.panel4.Controls.Add(this.panel11);
      this.panel4.Controls.Add(this.splitter10);
      this.panel4.Controls.Add(this.panel10);
      this.panel4.Controls.Add(this.ssGPRResults);
      this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel4.Location = new System.Drawing.Point(0, 27);
      this.panel4.Margin = new System.Windows.Forms.Padding(4);
      this.panel4.Name = "panel4";
      this.panel4.Size = new System.Drawing.Size(1477, 665);
      this.panel4.TabIndex = 1;
      // 
      // panel11
      // 
      this.panel11.Controls.Add(this.panel12);
      this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel11.Location = new System.Drawing.Point(0, 0);
      this.panel11.Margin = new System.Windows.Forms.Padding(4);
      this.panel11.Name = "panel11";
      this.panel11.Size = new System.Drawing.Size(1477, 416);
      this.panel11.TabIndex = 4;
      // 
      // panel12
      // 
      this.panel12.Controls.Add(this.dgvGPRResults);
      this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
      this.panel12.Location = new System.Drawing.Point(0, 0);
      this.panel12.Margin = new System.Windows.Forms.Padding(4);
      this.panel12.Name = "panel12";
      this.panel12.Size = new System.Drawing.Size(1477, 416);
      this.panel12.TabIndex = 2;
      // 
      // dgvGPRResults
      // 
      this.dgvGPRResults.AllowUserToAddRows = false;
      this.dgvGPRResults.AllowUserToDeleteRows = false;
      this.dgvGPRResults.BackgroundColor = System.Drawing.Color.White;
      dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvGPRResults.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
      this.dgvGPRResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
      dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dgvGPRResults.DefaultCellStyle = dataGridViewCellStyle10;
      this.dgvGPRResults.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvGPRResults.Location = new System.Drawing.Point(0, 0);
      this.dgvGPRResults.Margin = new System.Windows.Forms.Padding(4);
      this.dgvGPRResults.Name = "dgvGPRResults";
      this.dgvGPRResults.ReadOnly = true;
      this.dgvGPRResults.Size = new System.Drawing.Size(1477, 416);
      this.dgvGPRResults.TabIndex = 0;
      // 
      // splitter10
      // 
      this.splitter10.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splitter10.Location = new System.Drawing.Point(0, 416);
      this.splitter10.Margin = new System.Windows.Forms.Padding(4);
      this.splitter10.Name = "splitter10";
      this.splitter10.Size = new System.Drawing.Size(1477, 2);
      this.splitter10.TabIndex = 3;
      this.splitter10.TabStop = false;
      // 
      // panel10
      // 
      this.panel10.Controls.Add(this.ucGPRCollections);
      this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel10.Location = new System.Drawing.Point(0, 418);
      this.panel10.Margin = new System.Windows.Forms.Padding(4);
      this.panel10.Name = "panel10";
      this.panel10.Size = new System.Drawing.Size(1477, 225);
      this.panel10.TabIndex = 2;
      // 
      // ucGPRCollections
      // 
      this.ucGPRCollections.DataSource = null;
      this.ucGPRCollections.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ucGPRCollections.Location = new System.Drawing.Point(0, 0);
      this.ucGPRCollections.Margin = new System.Windows.Forms.Padding(5);
      this.ucGPRCollections.Name = "ucGPRCollections";
      this.ucGPRCollections.Size = new System.Drawing.Size(1477, 225);
      this.ucGPRCollections.TabIndex = 0;
      this.ucGPRCollections.Title = "Collections";
      // 
      // ssGPRResults
      // 
      this.ssGPRResults.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sstsGPRProgressBar,
            this.sstsGPRStatus,
            this.toolStripStatusLabel4,
            this.sstsGPRCollections});
      this.ssGPRResults.Location = new System.Drawing.Point(0, 643);
      this.ssGPRResults.Name = "ssGPRResults";
      this.ssGPRResults.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
      this.ssGPRResults.Size = new System.Drawing.Size(1477, 22);
      this.ssGPRResults.SizingGrip = false;
      this.ssGPRResults.TabIndex = 0;
      this.ssGPRResults.Text = "statusStrip1";
      // 
      // sstsGPRProgressBar
      // 
      this.sstsGPRProgressBar.Name = "sstsGPRProgressBar";
      this.sstsGPRProgressBar.Size = new System.Drawing.Size(133, 20);
      this.sstsGPRProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
      this.sstsGPRProgressBar.Visible = false;
      // 
      // sstsGPRStatus
      // 
      this.sstsGPRStatus.Name = "sstsGPRStatus";
      this.sstsGPRStatus.Size = new System.Drawing.Size(0, 17);
      // 
      // toolStripStatusLabel4
      // 
      this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
      this.toolStripStatusLabel4.Size = new System.Drawing.Size(1457, 17);
      this.toolStripStatusLabel4.Spring = true;
      // 
      // sstsGPRCollections
      // 
      this.sstsGPRCollections.Name = "sstsGPRCollections";
      this.sstsGPRCollections.Size = new System.Drawing.Size(0, 17);
      // 
      // tsGPRResults
      // 
      this.tsGPRResults.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsGPRImport,
            this.tsGPRProcess,
            this.tsGPRReport,
            this.tsGPRGenerateReport,
            this.tsGPRSave,
            this.tsGPRWarning,
            this.tsGPRPick,
            this.tsGPRReset,
            this.tsResearch,
            this.tsELMOD});
      this.tsGPRResults.Location = new System.Drawing.Point(0, 0);
      this.tsGPRResults.Name = "tsGPRResults";
      this.tsGPRResults.Size = new System.Drawing.Size(1477, 27);
      this.tsGPRResults.TabIndex = 0;
      this.tsGPRResults.Tag = "Process";
      this.tsGPRResults.Text = "toolStrip1";
      this.tsGPRResults.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsGPRResults_ItemClicked);
      // 
      // tsGPRImport
      // 
      this.tsGPRImport.Image = ((System.Drawing.Image)(resources.GetObject("tsGPRImport.Image")));
      this.tsGPRImport.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsGPRImport.Name = "tsGPRImport";
      this.tsGPRImport.Size = new System.Drawing.Size(74, 24);
      this.tsGPRImport.Tag = "Import";
      this.tsGPRImport.Text = "Import";
      this.tsGPRImport.ToolTipText = "Import GPR Results";
      // 
      // tsGPRProcess
      // 
      this.tsGPRProcess.Image = ((System.Drawing.Image)(resources.GetObject("tsGPRProcess.Image")));
      this.tsGPRProcess.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsGPRProcess.Name = "tsGPRProcess";
      this.tsGPRProcess.Size = new System.Drawing.Size(78, 24);
      this.tsGPRProcess.Tag = "Process";
      this.tsGPRProcess.Text = "Process";
      this.tsGPRProcess.ToolTipText = "Process GPR Results";
      // 
      // tsGPRReport
      // 
      this.tsGPRReport.Image = ((System.Drawing.Image)(resources.GetObject("tsGPRReport.Image")));
      this.tsGPRReport.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsGPRReport.Name = "tsGPRReport";
      this.tsGPRReport.Size = new System.Drawing.Size(74, 24);
      this.tsGPRReport.Tag = "Report";
      this.tsGPRReport.Text = "Report";
      this.tsGPRReport.ToolTipText = "Generate Report";
      // 
      // tsGPRGenerateReport
      // 
      this.tsGPRGenerateReport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmuGPRAnalysis,
            this.tsmuThicknessReport});
      this.tsGPRGenerateReport.Image = ((System.Drawing.Image)(resources.GetObject("tsGPRGenerateReport.Image")));
      this.tsGPRGenerateReport.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsGPRGenerateReport.Name = "tsGPRGenerateReport";
      this.tsGPRGenerateReport.Size = new System.Drawing.Size(83, 24);
      this.tsGPRGenerateReport.Text = "Report";
      this.tsGPRGenerateReport.ToolTipText = "Generate Report";
      this.tsGPRGenerateReport.Visible = false;
      this.tsGPRGenerateReport.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsGPRGenerateReport_DropDownItemClicked);
      // 
      // tsmuGPRAnalysis
      // 
      this.tsmuGPRAnalysis.Image = global::GPRControl.Properties.Resources.TableHS;
      this.tsmuGPRAnalysis.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.tsmuGPRAnalysis.Name = "tsmuGPRAnalysis";
      this.tsmuGPRAnalysis.Size = new System.Drawing.Size(189, 24);
      this.tsmuGPRAnalysis.Tag = "Analysis";
      this.tsmuGPRAnalysis.Text = "Analysis Report";
      // 
      // tsmuThicknessReport
      // 
      this.tsmuThicknessReport.Image = global::GPRControl.Properties.Resources.TableHS;
      this.tsmuThicknessReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.tsmuThicknessReport.Name = "tsmuThicknessReport";
      this.tsmuThicknessReport.Size = new System.Drawing.Size(189, 24);
      this.tsmuThicknessReport.Tag = "Thickness";
      this.tsmuThicknessReport.Text = "Thickness Report";
      // 
      // tsGPRSave
      // 
      this.tsGPRSave.Image = ((System.Drawing.Image)(resources.GetObject("tsGPRSave.Image")));
      this.tsGPRSave.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsGPRSave.Name = "tsGPRSave";
      this.tsGPRSave.Size = new System.Drawing.Size(60, 24);
      this.tsGPRSave.Tag = "Save";
      this.tsGPRSave.Text = "Save";
      this.tsGPRSave.ToolTipText = "Save Report";
      // 
      // tsGPRWarning
      // 
      this.tsGPRWarning.Image = ((System.Drawing.Image)(resources.GetObject("tsGPRWarning.Image")));
      this.tsGPRWarning.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsGPRWarning.Name = "tsGPRWarning";
      this.tsGPRWarning.Size = new System.Drawing.Size(124, 24);
      this.tsGPRWarning.Tag = "Warning";
      this.tsGPRWarning.Text = "Missing Layers";
      this.tsGPRWarning.ToolTipText = "Show Sections with Missing Layers";
      this.tsGPRWarning.Visible = false;
      // 
      // tsGPRPick
      // 
      this.tsGPRPick.Image = ((System.Drawing.Image)(resources.GetObject("tsGPRPick.Image")));
      this.tsGPRPick.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsGPRPick.Name = "tsGPRPick";
      this.tsGPRPick.Size = new System.Drawing.Size(102, 24);
      this.tsGPRPick.Tag = "Create PCK";
      this.tsGPRPick.Text = "Create PCK";
      this.tsGPRPick.ToolTipText = "Create PCK File";
      // 
      // tsGPRReset
      // 
      this.tsGPRReset.Image = ((System.Drawing.Image)(resources.GetObject("tsGPRReset.Image")));
      this.tsGPRReset.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsGPRReset.Name = "tsGPRReset";
      this.tsGPRReset.Size = new System.Drawing.Size(65, 24);
      this.tsGPRReset.Tag = "Reset";
      this.tsGPRReset.Text = "Reset";
      // 
      // tsResearch
      // 
      this.tsResearch.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnImportStation,
            this.mnReport,
            this.muPick});
      this.tsResearch.Image = ((System.Drawing.Image)(resources.GetObject("tsResearch.Image")));
      this.tsResearch.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsResearch.Name = "tsResearch";
      this.tsResearch.Size = new System.Drawing.Size(150, 24);
      this.tsResearch.Text = "Research Section";
      this.tsResearch.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsResearch_DropDownItemClicked);
      // 
      // mnImportStation
      // 
      this.mnImportStation.Name = "mnImportStation";
      this.mnImportStation.Size = new System.Drawing.Size(174, 24);
      this.mnImportStation.Tag = "Import";
      this.mnImportStation.Text = "Import Station";
      // 
      // mnReport
      // 
      this.mnReport.Name = "mnReport";
      this.mnReport.Size = new System.Drawing.Size(174, 24);
      this.mnReport.Tag = "Report";
      this.mnReport.Text = "Report";
      // 
      // muPick
      // 
      this.muPick.Name = "muPick";
      this.muPick.Size = new System.Drawing.Size(174, 24);
      this.muPick.Tag = "Create PCK";
      this.muPick.Text = "Create PCK";
      // 
      // tsELMOD
      // 
      this.tsELMOD.Image = ((System.Drawing.Image)(resources.GetObject("tsELMOD.Image")));
      this.tsELMOD.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsELMOD.Name = "tsELMOD";
      this.tsELMOD.Size = new System.Drawing.Size(100, 24);
      this.tsELMOD.Tag = "ELMOD";
      this.tsELMOD.Text = "LA ELMOD";
      this.tsELMOD.ToolTipText = "Create ELMOD for LAGPR";
      // 
      // tpQCReports
      // 
      this.tpQCReports.Controls.Add(this.dgvQCReports);
      this.tpQCReports.Controls.Add(this.ssQCReports);
      this.tpQCReports.Controls.Add(this.tsQCReports);
      this.tpQCReports.Location = new System.Drawing.Point(4, 25);
      this.tpQCReports.Margin = new System.Windows.Forms.Padding(4);
      this.tpQCReports.Name = "tpQCReports";
      this.tpQCReports.Padding = new System.Windows.Forms.Padding(4);
      this.tpQCReports.Size = new System.Drawing.Size(1477, 692);
      this.tpQCReports.TabIndex = 5;
      this.tpQCReports.Text = "QC Reports";
      this.tpQCReports.UseVisualStyleBackColor = true;
      // 
      // dgvQCReports
      // 
      this.dgvQCReports.AllowUserToAddRows = false;
      this.dgvQCReports.AllowUserToDeleteRows = false;
      this.dgvQCReports.AllowUserToOrderColumns = true;
      this.dgvQCReports.AllowUserToResizeRows = false;
      this.dgvQCReports.BackgroundColor = System.Drawing.Color.White;
      this.dgvQCReports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvQCReports.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvQCReports.Location = new System.Drawing.Point(4, 31);
      this.dgvQCReports.Margin = new System.Windows.Forms.Padding(4);
      this.dgvQCReports.Name = "dgvQCReports";
      this.dgvQCReports.Size = new System.Drawing.Size(1469, 632);
      this.dgvQCReports.TabIndex = 2;
      // 
      // ssQCReports
      // 
      this.ssQCReports.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sstsQCReportsProgressBar,
            this.tsslQCSelectedGroup});
      this.ssQCReports.Location = new System.Drawing.Point(4, 663);
      this.ssQCReports.Name = "ssQCReports";
      this.ssQCReports.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
      this.ssQCReports.Size = new System.Drawing.Size(1469, 25);
      this.ssQCReports.TabIndex = 1;
      this.ssQCReports.Text = "statusStrip1";
      // 
      // sstsQCReportsProgressBar
      // 
      this.sstsQCReportsProgressBar.Name = "sstsQCReportsProgressBar";
      this.sstsQCReportsProgressBar.Size = new System.Drawing.Size(133, 21);
      this.sstsQCReportsProgressBar.Visible = false;
      // 
      // tsslQCSelectedGroup
      // 
      this.tsslQCSelectedGroup.Image = ((System.Drawing.Image)(resources.GetObject("tsslQCSelectedGroup.Image")));
      this.tsslQCSelectedGroup.Name = "tsslQCSelectedGroup";
      this.tsslQCSelectedGroup.Size = new System.Drawing.Size(151, 20);
      this.tsslQCSelectedGroup.Text = "No Group Selected";
      // 
      // tsQCReports
      // 
      this.tsQCReports.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbQCGroupSelect,
            this.tsbQCLaneAnalysis,
            this.tsbClearTempTables,
            this.toolStripSeparator1,
            this.toolStripButton3});
      this.tsQCReports.Location = new System.Drawing.Point(4, 4);
      this.tsQCReports.Name = "tsQCReports";
      this.tsQCReports.Size = new System.Drawing.Size(1469, 27);
      this.tsQCReports.TabIndex = 0;
      this.tsQCReports.Text = "toolStrip1";
      this.tsQCReports.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.tsQCReports_ItemClicked);
      // 
      // tsbQCGroupSelect
      // 
      this.tsbQCGroupSelect.Image = ((System.Drawing.Image)(resources.GetObject("tsbQCGroupSelect.Image")));
      this.tsbQCGroupSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbQCGroupSelect.Name = "tsbQCGroupSelect";
      this.tsbQCGroupSelect.Size = new System.Drawing.Size(69, 24);
      this.tsbQCGroupSelect.Tag = "Collections";
      this.tsbQCGroupSelect.Text = "Select";
      // 
      // tsbQCLaneAnalysis
      // 
      this.tsbQCLaneAnalysis.Image = ((System.Drawing.Image)(resources.GetObject("tsbQCLaneAnalysis.Image")));
      this.tsbQCLaneAnalysis.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbQCLaneAnalysis.Name = "tsbQCLaneAnalysis";
      this.tsbQCLaneAnalysis.Size = new System.Drawing.Size(117, 24);
      this.tsbQCLaneAnalysis.Tag = "Lane Analysis";
      this.tsbQCLaneAnalysis.Text = "Lane Analysis";
      // 
      // tsbClearTempTables
      // 
      this.tsbClearTempTables.Image = ((System.Drawing.Image)(resources.GetObject("tsbClearTempTables.Image")));
      this.tsbClearTempTables.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.tsbClearTempTables.Name = "tsbClearTempTables";
      this.tsbClearTempTables.Size = new System.Drawing.Size(152, 24);
      this.tsbClearTempTables.Tag = "Clear Temp Tables";
      this.tsbClearTempTables.Text = "Clear Temp Tables";
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
      // 
      // toolStripButton3
      // 
      this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
      this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton3.Name = "toolStripButton3";
      this.toolStripButton3.Size = new System.Drawing.Size(65, 24);
      this.toolStripButton3.Tag = "Reset";
      this.toolStripButton3.Text = "Reset";
      // 
      // imageList1
      // 
      this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
      this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
      this.imageList1.Images.SetKeyName(0, "OrganizerHS.png");
      this.imageList1.Images.SetKeyName(1, "NOTE06.ICO");
      this.imageList1.Images.SetKeyName(2, "BehindTextHS.png");
      // 
      // sfSaveFile
      // 
      this.sfSaveFile.DefaultExt = "xls";
      this.sfSaveFile.Filter = "Excel Files | *.xls";
      // 
      // fdOpenFile
      // 
      this.fdOpenFile.Multiselect = true;
      // 
      // bgWorker
      // 
      this.bgWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_DoWork);
      this.bgWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_RunWorkerCompleted);
      // 
      // GPRControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel1);
      this.Margin = new System.Windows.Forms.Padding(4);
      this.Name = "GPRControl";
      this.Size = new System.Drawing.Size(1485, 721);
      this.panel1.ResumeLayout(false);
      this.tbControl.ResumeLayout(false);
      this.tpSetup.ResumeLayout(false);
      this.panel5.ResumeLayout(false);
      this.panel5.PerformLayout();
      this.panel13.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvSetup)).EndInit();
      this.ssSetup.ResumeLayout(false);
      this.ssSetup.PerformLayout();
      this.tsSetup.ResumeLayout(false);
      this.tsSetup.PerformLayout();
      this.tpMarker.ResumeLayout(false);
      this.tpMarker.PerformLayout();
      this.panel6.ResumeLayout(false);
      this.pnlMar.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvMars)).EndInit();
      this.pnlMarCollection.ResumeLayout(false);
      this.pnlMarCollection.PerformLayout();
      this.panel9.ResumeLayout(false);
      this.ssMar.ResumeLayout(false);
      this.ssMar.PerformLayout();
      this.tsMar.ResumeLayout(false);
      this.tsMar.PerformLayout();
      this.tpCoreRequest.ResumeLayout(false);
      this.tpCoreRequest.PerformLayout();
      this.panel7.ResumeLayout(false);
      this.pnlCoreRequest.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvCoreRequest)).EndInit();
      this.pnlCoreReqCollections.ResumeLayout(false);
      this.panel8.ResumeLayout(false);
      this.panel8.PerformLayout();
      this.ssCoreRequest.ResumeLayout(false);
      this.ssCoreRequest.PerformLayout();
      this.tsCoreRequest.ResumeLayout(false);
      this.tsCoreRequest.PerformLayout();
      this.tpCoreResult.ResumeLayout(false);
      this.tpCoreResult.PerformLayout();
      this.panel2.ResumeLayout(false);
      this.pnlCoreResults.ResumeLayout(false);
      this.pnlCoreRes.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvCoreResults)).EndInit();
      this.pnlLayers.ResumeLayout(false);
      this.pnlCoreResultCollection.ResumeLayout(false);
      this.panel3.ResumeLayout(false);
      this.panel3.PerformLayout();
      this.ssCoreResult.ResumeLayout(false);
      this.ssCoreResult.PerformLayout();
      this.tsCoreResult.ResumeLayout(false);
      this.tsCoreResult.PerformLayout();
      this.tpGRPResults.ResumeLayout(false);
      this.tpGRPResults.PerformLayout();
      this.panel4.ResumeLayout(false);
      this.panel4.PerformLayout();
      this.panel11.ResumeLayout(false);
      this.panel12.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgvGPRResults)).EndInit();
      this.panel10.ResumeLayout(false);
      this.ssGPRResults.ResumeLayout(false);
      this.ssGPRResults.PerformLayout();
      this.tsGPRResults.ResumeLayout(false);
      this.tsGPRResults.PerformLayout();
      this.tpQCReports.ResumeLayout(false);
      this.tpQCReports.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvQCReports)).EndInit();
      this.ssQCReports.ResumeLayout(false);
      this.ssQCReports.PerformLayout();
      this.tsQCReports.ResumeLayout(false);
      this.tsQCReports.PerformLayout();
      this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TabControl tbControl;
		private System.Windows.Forms.TabPage tpMarker;
		private System.Windows.Forms.ToolStrip tsMar;
		private System.Windows.Forms.TabPage tpCoreRequest;
		private System.Windows.Forms.ToolStripButton tsMarCreate;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.DataGridView dgvMars;
		private System.Windows.Forms.FolderBrowserDialog fbSelectFolder;
		private System.Windows.Forms.ToolStrip tsCoreRequest;
		private System.Windows.Forms.ToolStripButton tsCoreReqImport;
		private System.Windows.Forms.ToolStripButton tsCoreReqExport;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.DataGridView dgvCoreRequest;
		private System.Windows.Forms.SaveFileDialog sfSaveFile;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.ToolStripButton tsMarCollections;
		private System.Windows.Forms.ToolStripButton tsMarRefresh;
		private System.Windows.Forms.Panel pnlMar;
		private System.Windows.Forms.Splitter splitter4;
		private System.Windows.Forms.Panel pnlMarCollection;
		private System.Windows.Forms.StatusStrip ssMar;
		private System.Windows.Forms.Splitter splitter5;
		private System.Windows.Forms.ToolStripStatusLabel sstsMarStatus;
		private System.Windows.Forms.ToolStripStatusLabel sstsMarCollections;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.ToolStripProgressBar sstsMarProgress;
		private System.Windows.Forms.Splitter splitter6;
		private System.Windows.Forms.Panel panel8;
		private System.Windows.Forms.StatusStrip ssCoreRequest;
		private System.Windows.Forms.Panel pnlCoreRequest;
		private System.Windows.Forms.Splitter splitter7;
		private System.Windows.Forms.Panel pnlCoreReqCollections;
		private DisplayCollections ucCoreReqCollections;
		private System.Windows.Forms.ToolStripStatusLabel sstsCoreReqStatus;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
		private System.Windows.Forms.ToolStripStatusLabel sstsCoreReqCollections;
		private System.Windows.Forms.ToolStripButton tsCoreReqGenerate;
		private System.Windows.Forms.ToolStripButton tsCoreReqRefresh;
		private System.Windows.Forms.Panel panel9;
		private System.Windows.Forms.OpenFileDialog fdOpenFile;
		private System.Windows.Forms.ToolStripProgressBar sstsCoreRequestProgressBar;
		private System.ComponentModel.BackgroundWorker bgWorker;
		private System.Windows.Forms.ToolStripButton tsCoreReqDelete;
		private System.Windows.Forms.TabPage tpCoreResult;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ToolStrip tsCoreResult;
		private System.Windows.Forms.Panel pnlCoreResults;
		private System.Windows.Forms.DataGridView dgvCoreResults;
		private System.Windows.Forms.Splitter splitter2;
		private System.Windows.Forms.Panel pnlCoreResultCollection;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.StatusStrip ssCoreResult;
		private System.Windows.Forms.ToolStripProgressBar sstsCoreResultProgressBar;
		private System.Windows.Forms.ToolStripStatusLabel sstsCoreResultStatus;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
		private System.Windows.Forms.ToolStripStatusLabel sstsCoreResultCollections;
		private System.Windows.Forms.ToolStripButton tsCoreResImport;
		private System.Windows.Forms.ToolStripButton tsCoreResExport;
        private System.Windows.Forms.ToolStripButton tsCoreResSelect;
		private System.Windows.Forms.ListView lvLayers;
		private System.Windows.Forms.ColumnHeader colLayer;
		private System.Windows.Forms.ColumnHeader colThickness;
		private System.Windows.Forms.ColumnHeader colCharacteristic;
		private System.Windows.Forms.ToolStripButton tsCoreResReset;
		private System.Windows.Forms.TabPage tpGRPResults;
		private System.Windows.Forms.ToolStrip tsGPRResults;
		private System.Windows.Forms.ToolStripButton tsGPRReset;
		private System.Windows.Forms.ToolStripButton tsGPRImport;
		private System.Windows.Forms.ToolStripButton tsGPRSave;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.ToolStripButton tsCoreResUpdateImagePath;
		private System.Windows.Forms.StatusStrip ssGPRResults;
		private System.Windows.Forms.ToolStripProgressBar sstsGPRProgressBar;
		private System.Windows.Forms.ToolStripStatusLabel sstsGPRStatus;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
		private System.Windows.Forms.ToolStripStatusLabel sstsGPRCollections;
		private System.Windows.Forms.ToolStripButton tsGPRProcess;
		private System.Windows.Forms.Panel panel10;
		private System.Windows.Forms.Splitter splitter10;
		private DisplayCollections ucGPRCollections;
		private System.Windows.Forms.Panel panel11;
		private System.Windows.Forms.Panel panel12;
		private System.Windows.Forms.DataGridView dgvGPRResults;
		private DisplayCollections ucCoreResCollections;
		private System.Windows.Forms.Panel pnlLayers;
		private System.Windows.Forms.Panel pnlCoreRes;
		private System.Windows.Forms.Splitter splitter3;
		private System.Windows.Forms.ToolStripButton tsCoreResDelete;
		private System.Windows.Forms.ToolStripDropDownButton tsGPRGenerateReport;
		private System.Windows.Forms.ToolStripMenuItem tsmuGPRAnalysis;
		private System.Windows.Forms.ToolStripMenuItem tsmuThicknessReport;
		private System.Windows.Forms.ToolStripButton tsGPRPick;
		private System.Windows.Forms.ToolStripButton tsGPRReport;
		private System.Windows.Forms.ToolStripButton tsGPRWarning;
		private System.Windows.Forms.ToolStripDropDownButton tsResearch;
		private System.Windows.Forms.ToolStripMenuItem mnImportStation;
		private System.Windows.Forms.ToolStripMenuItem mnReport;
		private System.Windows.Forms.ToolStripMenuItem muPick;
		private System.Windows.Forms.TabPage tpSetup;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Panel panel13;
		private System.Windows.Forms.DataGridView dgvSetup;
		private System.Windows.Forms.StatusStrip ssSetup;
		private System.Windows.Forms.ToolStrip tsSetup;
		private System.Windows.Forms.ToolStripButton tsSetupLayerCode;
		private System.Windows.Forms.ToolStripButton tsSetupVelocity;
		private System.Windows.Forms.ToolStripStatusLabel ssSetupStatus;
		private System.Windows.Forms.ToolStripButton tsSetupReset;
        private System.Windows.Forms.ToolStripButton tsELMOD;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton tsMarImportReportingInterval;
        private System.Windows.Forms.TabPage tpQCReports;
        private System.Windows.Forms.DataGridView dgvQCReports;
        private System.Windows.Forms.StatusStrip ssQCReports;
        private System.Windows.Forms.ToolStrip tsQCReports;
        private System.Windows.Forms.ToolStripButton tsbQCLaneAnalysis;
        private System.Windows.Forms.ToolStripProgressBar sstsQCReportsProgressBar;
        private System.Windows.Forms.ToolStripButton tsbQCGroupSelect;
        private System.Windows.Forms.ToolStripStatusLabel tsslQCSelectedGroup;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private DisplayCollections ucMarCollections;
        private System.Windows.Forms.ToolStripButton tsbClearTempTables;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsMarCreateCSV;
	}
}
