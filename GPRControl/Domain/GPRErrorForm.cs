using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GPRControls.Domain
{
    public partial class GPRErrorForm : Form
    {
        public GPRErrorForm()
        {
            InitializeComponent();
        }

        public GPRErrorForm(string description, DataTable dtErrors)
        {
            InitializeComponent();

            this.lblErrorDescription.Text = description;
            this.dgvErrors.DataSource = dtErrors;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}