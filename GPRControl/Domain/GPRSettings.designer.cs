namespace GPRControls.Domain
{
	partial class GPRSettings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlMain = new System.Windows.Forms.Panel();
			this.pnlContent = new System.Windows.Forms.Panel();
			this.pnlChainage = new System.Windows.Forms.Panel();
			this.cbCoreThickness = new System.Windows.Forms.ComboBox();
			this.lbCoreThickness = new System.Windows.Forms.Label();
			this.cbCoreChainage = new System.Windows.Forms.ComboBox();
			this.lbCoreChainage = new System.Windows.Forms.Label();
			this.pnlGPRProcessing = new System.Windows.Forms.Panel();
			this.rbnConstVelocity = new System.Windows.Forms.RadioButton();
			this.rbnCalcVelocity = new System.Windows.Forms.RadioButton();
			this.label7 = new System.Windows.Forms.Label();
			this.cbVelocity = new System.Windows.Forms.ComboBox();
			this.txtVelocity = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txtThreshold = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.pnlGPRReport = new System.Windows.Forms.Panel();
			this.lbIntervalUnit = new System.Windows.Forms.Label();
			this.txtInterval = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.cbGPRThickness = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.cbGPRChainage = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.pnlButtons = new System.Windows.Forms.Panel();
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.pnlMain.SuspendLayout();
			this.pnlContent.SuspendLayout();
			this.pnlChainage.SuspendLayout();
			this.pnlGPRProcessing.SuspendLayout();
			this.pnlGPRReport.SuspendLayout();
			this.pnlButtons.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlMain
			// 
			this.pnlMain.Controls.Add(this.pnlContent);
			this.pnlMain.Controls.Add(this.splitter1);
			this.pnlMain.Controls.Add(this.pnlButtons);
			this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlMain.Location = new System.Drawing.Point(0, 0);
			this.pnlMain.Name = "pnlMain";
			this.pnlMain.Size = new System.Drawing.Size(273, 404);
			this.pnlMain.TabIndex = 0;
			// 
			// pnlContent
			// 
			this.pnlContent.Controls.Add(this.pnlChainage);
			this.pnlContent.Controls.Add(this.pnlGPRProcessing);
			this.pnlContent.Controls.Add(this.pnlGPRReport);
			this.pnlContent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlContent.Location = new System.Drawing.Point(0, 0);
			this.pnlContent.Name = "pnlContent";
			this.pnlContent.Size = new System.Drawing.Size(273, 358);
			this.pnlContent.TabIndex = 2;
			// 
			// pnlChainage
			// 
			this.pnlChainage.Controls.Add(this.cbCoreThickness);
			this.pnlChainage.Controls.Add(this.lbCoreThickness);
			this.pnlChainage.Controls.Add(this.cbCoreChainage);
			this.pnlChainage.Controls.Add(this.lbCoreChainage);
			this.pnlChainage.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlChainage.Location = new System.Drawing.Point(0, 5);
			this.pnlChainage.Name = "pnlChainage";
			this.pnlChainage.Size = new System.Drawing.Size(273, 97);
			this.pnlChainage.TabIndex = 2;
			this.pnlChainage.Visible = false;
			// 
			// cbCoreThickness
			// 
			this.cbCoreThickness.FormattingEnabled = true;
			this.cbCoreThickness.Location = new System.Drawing.Point(92, 59);
			this.cbCoreThickness.Name = "cbCoreThickness";
			this.cbCoreThickness.Size = new System.Drawing.Size(165, 21);
			this.cbCoreThickness.TabIndex = 3;
			// 
			// lbCoreThickness
			// 
			this.lbCoreThickness.AutoSize = true;
			this.lbCoreThickness.Location = new System.Drawing.Point(24, 59);
			this.lbCoreThickness.Name = "lbCoreThickness";
			this.lbCoreThickness.Size = new System.Drawing.Size(56, 13);
			this.lbCoreThickness.TabIndex = 2;
			this.lbCoreThickness.Text = "Thickness";
			// 
			// cbCoreChainage
			// 
			this.cbCoreChainage.FormattingEnabled = true;
			this.cbCoreChainage.Location = new System.Drawing.Point(92, 24);
			this.cbCoreChainage.Name = "cbCoreChainage";
			this.cbCoreChainage.Size = new System.Drawing.Size(165, 21);
			this.cbCoreChainage.TabIndex = 1;
			// 
			// lbCoreChainage
			// 
			this.lbCoreChainage.AutoSize = true;
			this.lbCoreChainage.Location = new System.Drawing.Point(21, 24);
			this.lbCoreChainage.Name = "lbCoreChainage";
			this.lbCoreChainage.Size = new System.Drawing.Size(52, 13);
			this.lbCoreChainage.TabIndex = 0;
			this.lbCoreChainage.Text = "Chainage";
			// 
			// pnlGPRProcessing
			// 
			this.pnlGPRProcessing.Controls.Add(this.rbnConstVelocity);
			this.pnlGPRProcessing.Controls.Add(this.rbnCalcVelocity);
			this.pnlGPRProcessing.Controls.Add(this.label7);
			this.pnlGPRProcessing.Controls.Add(this.cbVelocity);
			this.pnlGPRProcessing.Controls.Add(this.txtVelocity);
			this.pnlGPRProcessing.Controls.Add(this.label6);
			this.pnlGPRProcessing.Controls.Add(this.txtThreshold);
			this.pnlGPRProcessing.Controls.Add(this.label4);
			this.pnlGPRProcessing.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlGPRProcessing.Location = new System.Drawing.Point(0, 102);
			this.pnlGPRProcessing.Name = "pnlGPRProcessing";
			this.pnlGPRProcessing.Size = new System.Drawing.Size(273, 122);
			this.pnlGPRProcessing.TabIndex = 1;
			this.pnlGPRProcessing.Visible = false;
			// 
			// rbnConstVelocity
			// 
			this.rbnConstVelocity.AutoSize = true;
			this.rbnConstVelocity.Location = new System.Drawing.Point(150, 16);
			this.rbnConstVelocity.Name = "rbnConstVelocity";
			this.rbnConstVelocity.Size = new System.Drawing.Size(107, 17);
			this.rbnConstVelocity.TabIndex = 1;
			this.rbnConstVelocity.Text = "Constant Velocity";
			this.rbnConstVelocity.UseVisualStyleBackColor = true;
			this.rbnConstVelocity.CheckedChanged += new System.EventHandler(this.rbnConstVelocity_CheckedChanged_1);
			// 
			// rbnCalcVelocity
			// 
			this.rbnCalcVelocity.AutoSize = true;
			this.rbnCalcVelocity.Checked = true;
			this.rbnCalcVelocity.Location = new System.Drawing.Point(24, 16);
			this.rbnCalcVelocity.Name = "rbnCalcVelocity";
			this.rbnCalcVelocity.Size = new System.Drawing.Size(115, 17);
			this.rbnCalcVelocity.TabIndex = 0;
			this.rbnCalcVelocity.TabStop = true;
			this.rbnCalcVelocity.Text = "Calculated Velocity";
			this.rbnCalcVelocity.UseVisualStyleBackColor = true;
			this.rbnCalcVelocity.CheckedChanged += new System.EventHandler(this.rbnCalcVelocity_CheckedChanged);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(185, 91);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(33, 13);
			this.label7.TabIndex = 7;
			this.label7.Text = "meter";
			// 
			// cbVelocity
			// 
			this.cbVelocity.FormattingEnabled = true;
			this.cbVelocity.Location = new System.Drawing.Point(184, 49);
			this.cbVelocity.Name = "cbVelocity";
			this.cbVelocity.Size = new System.Drawing.Size(78, 21);
			this.cbVelocity.TabIndex = 6;
			// 
			// txtVelocity
			// 
			this.txtVelocity.Location = new System.Drawing.Point(94, 49);
			this.txtVelocity.Name = "txtVelocity";
			this.txtVelocity.Size = new System.Drawing.Size(84, 20);
			this.txtVelocity.TabIndex = 4;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(24, 49);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(44, 13);
			this.label6.TabIndex = 3;
			this.label6.Text = "Velocity";
			// 
			// txtThreshold
			// 
			this.txtThreshold.Location = new System.Drawing.Point(94, 85);
			this.txtThreshold.Name = "txtThreshold";
			this.txtThreshold.Size = new System.Drawing.Size(84, 20);
			this.txtThreshold.TabIndex = 1;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(21, 85);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(54, 13);
			this.label4.TabIndex = 0;
			this.label4.Text = "Threshold";
			// 
			// pnlGPRReport
			// 
			this.pnlGPRReport.Controls.Add(this.lbIntervalUnit);
			this.pnlGPRReport.Controls.Add(this.txtInterval);
			this.pnlGPRReport.Controls.Add(this.label3);
			this.pnlGPRReport.Controls.Add(this.cbGPRThickness);
			this.pnlGPRReport.Controls.Add(this.label2);
			this.pnlGPRReport.Controls.Add(this.cbGPRChainage);
			this.pnlGPRReport.Controls.Add(this.label1);
			this.pnlGPRReport.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlGPRReport.Location = new System.Drawing.Point(0, 224);
			this.pnlGPRReport.Name = "pnlGPRReport";
			this.pnlGPRReport.Size = new System.Drawing.Size(273, 134);
			this.pnlGPRReport.TabIndex = 0;
			this.pnlGPRReport.Visible = false;
			// 
			// lbIntervalUnit
			// 
			this.lbIntervalUnit.AutoSize = true;
			this.lbIntervalUnit.Location = new System.Drawing.Point(228, 99);
			this.lbIntervalUnit.Name = "lbIntervalUnit";
			this.lbIntervalUnit.Size = new System.Drawing.Size(0, 13);
			this.lbIntervalUnit.TabIndex = 6;
			// 
			// txtInterval
			// 
			this.txtInterval.Location = new System.Drawing.Point(92, 96);
			this.txtInterval.Name = "txtInterval";
			this.txtInterval.Size = new System.Drawing.Size(130, 20);
			this.txtInterval.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(24, 96);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(42, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Interval";
			// 
			// cbGPRThickness
			// 
			this.cbGPRThickness.FormattingEnabled = true;
			this.cbGPRThickness.Location = new System.Drawing.Point(92, 56);
			this.cbGPRThickness.Name = "cbGPRThickness";
			this.cbGPRThickness.Size = new System.Drawing.Size(130, 21);
			this.cbGPRThickness.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(21, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Thickness";
			// 
			// cbGPRChainage
			// 
			this.cbGPRChainage.FormattingEnabled = true;
			this.cbGPRChainage.Location = new System.Drawing.Point(92, 16);
			this.cbGPRChainage.Name = "cbGPRChainage";
			this.cbGPRChainage.Size = new System.Drawing.Size(130, 21);
			this.cbGPRChainage.TabIndex = 1;
			this.cbGPRChainage.SelectedIndexChanged += new System.EventHandler(this.cbGPRChainage_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(21, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(52, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Chainage";
			// 
			// splitter1
			// 
			this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.splitter1.Location = new System.Drawing.Point(0, 358);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(273, 3);
			this.splitter1.TabIndex = 1;
			this.splitter1.TabStop = false;
			// 
			// pnlButtons
			// 
			this.pnlButtons.Controls.Add(this.btnCancel);
			this.pnlButtons.Controls.Add(this.btnOK);
			this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlButtons.Location = new System.Drawing.Point(0, 361);
			this.pnlButtons.Name = "pnlButtons";
			this.pnlButtons.Size = new System.Drawing.Size(273, 43);
			this.pnlButtons.TabIndex = 0;
			// 
			// btnCancel
			// 
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(182, 8);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 1;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// btnOK
			// 
			this.btnOK.Location = new System.Drawing.Point(101, 8);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 0;
			this.btnOK.Text = "OK";
			this.btnOK.UseVisualStyleBackColor = true;
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// GPRSettings
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(273, 404);
			this.Controls.Add(this.pnlMain);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "GPRSettings";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "GPR Settings";
			this.pnlMain.ResumeLayout(false);
			this.pnlContent.ResumeLayout(false);
			this.pnlChainage.ResumeLayout(false);
			this.pnlChainage.PerformLayout();
			this.pnlGPRProcessing.ResumeLayout(false);
			this.pnlGPRProcessing.PerformLayout();
			this.pnlGPRReport.ResumeLayout(false);
			this.pnlGPRReport.PerformLayout();
			this.pnlButtons.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlMain;
		private System.Windows.Forms.Panel pnlButtons;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.Splitter splitter1;
		private System.Windows.Forms.Panel pnlContent;
		private System.Windows.Forms.Panel pnlGPRReport;
		private System.Windows.Forms.ComboBox cbGPRChainage;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lbIntervalUnit;
		private System.Windows.Forms.TextBox txtInterval;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox cbGPRThickness;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel pnlGPRProcessing;
		private System.Windows.Forms.TextBox txtThreshold;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtVelocity;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Panel pnlChainage;
		private System.Windows.Forms.ComboBox cbCoreChainage;
		private System.Windows.Forms.Label lbCoreChainage;
		private System.Windows.Forms.ComboBox cbVelocity;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox cbCoreThickness;
		private System.Windows.Forms.Label lbCoreThickness;
		private System.Windows.Forms.RadioButton rbnCalcVelocity;
		private System.Windows.Forms.RadioButton rbnConstVelocity;
	}
}