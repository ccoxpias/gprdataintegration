using ADOX;
using ARAN.Library.Units;
using Roadware.DataMapping;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GPRControls.Domain
{
  public partial class GPRControl : UserControl
  {
    public enum TaskType
    {
      MAR_SELECT,
      MAR_CREATE,
      MAR_CREATE_CSV,
      COREREQ_IMPORT,
      COREREQ_GENERATE,
      COREREQ_EXPORT,
      CORERES_IMPORT,
      CORERES_SELECT,
      CORERES_EXPORT,
      CORERES_SET_PATH,
      GPR_IMPORT,
      GPR_PROCESS,
      GPR_REPORT,
      GPR_SAVE,
      GPR_PCK,
      GPR_ELMOD,
      GPR_WARNING,
      FWD_IMPORT,
      SETUP_LAYER,
      SETUP_VELOCITY,
      REPORTINTERVAL_IMPORT,
      QC_LANE_ANALYSIS,
      CLEAR_TEMP_TABLES,
      NONE
    }

    public enum ReportType
    {
      GPR_ANALYSIS,
      GPR_THICKNESS,
      GPR_RESEARCH
    }

    #region fields
    private DataMapper dataMapper;
    string connStr = "";

    // MAR
    private DataTable dtMars = new DataTable();
    private DataTable dtMarCollections = new DataTable();
    List<string> MarCollections = new List<string>();

    // Core requests
    private DataTable dtCoreRequests = new DataTable();
    private DataTable dtRawCoreRequests = new DataTable();
    private DataTable dtCoreReqCollections = new DataTable();
    List<string> CoreReqCollections = new List<string>();

    // Core results
    private DataTable dtCoreResults = new DataTable();
    private DataTable dtCoreLayers = new DataTable();
    private DataTable dtCoreImages = new DataTable();
    private DataTable dtCoreResCollections = new DataTable();
    List<string> CoreResCollections = new List<string>();
    CoreUnitSetting coreUnits = new CoreUnitSetting();

    // GPR results
    private bool gprResults_all = true;
    //private bool importAllGPRResults = true;

    private DataTable dtFWDStations = new DataTable();
    private DataTable dtGPRCollections = new DataTable();
    private DataTable dtGPRProcessedResults = new DataTable();
    private DataTable dtGPRReport = new DataTable();
    DataTable dtImportErrors = new DataTable();

    private DSGPRResults.GPRResultsDataTable dtGPRResults = new DSGPRResults.GPRResultsDataTable();
    private DSGPRResults.GPRResults_staging_mar2DataTable dtGPRResults_mar2 = new DSGPRResults.GPRResults_staging_mar2DataTable();

    GPRResultSetting GPRReportSetting = new GPRResultSetting();

    List<string> GPRCollections = new List<string>();

    ReportType reportType = ReportType.GPR_THICKNESS;

    string GPRImportErrorText = "";
    private string mar2path = "";

    // Setup
    private DataTable dtSetup = new DataTable();

    TaskType taskType = TaskType.NONE;
    #endregion

    public DataMapper DataMapper
    {
      get { return this.dataMapper; }
      set { this.dataMapper = value; }
    }

    public string ConnectionString
    {
      get { return connStr; }
      set { connStr = value; }
    }

    public GPRControl()
    {
      InitializeComponent();

      this.tsGPRResults.Visible = true;
      //this.tsGPRResults.Visible = true;
      this.tsCoreRequest.Visible = true;
      this.tsCoreResult.Visible = true;
      this.ssQCReports.Visible = true;
      this.tsQCReports.Visible = true;
    }

    public void Reset()
    {
      ResetMar();
      ResetCoreRequests();
      ResetCoreResults();
      ResetGPRResults();
      ResetSetup();

      this.GPRReportSetting = null;
      this.coreUnits = null;
      this.GPRReportSetting = new GPRResultSetting();
      this.coreUnits = new CoreUnitSetting();

      this.tbControl.SelectedTab = this.tpSetup;
    }

    private void tbControl_SelectedIndexChanged(object sender, EventArgs e)
    {
      string page = Convert.ToString(this.tbControl.SelectedTab.Tag);

      switch (page)
      {
        case "MAR":
          break;

        case "Core Request":
          break;
      }
    }

    #region Setup
    private void tsSetup_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {
      string action = Convert.ToString(e.ClickedItem.Tag);

      switch (action)
      {
        case "Layer Codes":
          ImportLayerCodes();
          break;

        case "Velocities":
          ImportLayerVelocities();
          break;

        case "Reset":
          ResetSetup();
          break;
      }
    }

    private void ImportLayerCodes()
    {
      this.fdOpenFile.Multiselect = false;
      this.fdOpenFile.DefaultExt = "xls";
      this.fdOpenFile.Filter = "MS Excel Files | *.xls";

      if (this.fdOpenFile.ShowDialog(this) == DialogResult.OK)
      {
        this.EnableButtons(false);
        this.ResetSetup();

        this.ssSetupStatus.Text = "Importing layer codes ...";
        this.ssSetup.Update();

        logGPRTool("Import Layer Codes", "", this.fdOpenFile.FileName);

        this.taskType = TaskType.SETUP_LAYER;
        this.bgWorker.RunWorkerAsync(this.fdOpenFile.FileName);
      }
    }

    private void ImportLayerCodes(string filename)
    {
      try
      {
        string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties=Excel 5.0";

        using (OleDbConnection conn = new OleDbConnection(connString))
        {
          conn.Open();

          // layer codes
          using (OleDbCommand command = new OleDbCommand())
          {
            command.Connection = conn;
            command.CommandText = "SELECT [PCK Code] as PCKCode, [Material Code] as MaterialCode, [Description] FROM [LayerCode$]";

            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
              adapter.Fill(this.dtSetup);
            }
          }

          string sqlStr = "exec gpr.usp_UpdateLayerCode @LayerNumber, @LayerType, @Description";
          Dictionary<string, object> paras = new Dictionary<string, object>();

          foreach (DataRow row in dtSetup.Rows)
          {
            paras.Clear();
            paras.Add("LayerNumber", Convert.ToInt32(row["PCKCode"]));
            paras.Add("LayerType", Convert.ToString(row["MaterialCode"]));
            paras.Add("Description", Convert.ToString(row["Description"]));

            this.dataMapper.DataProvider.ExecuteNonQuery(sqlStr, paras);
          }

          DataTable dtConfidence = new DataTable();
          using (OleDbCommand command = new OleDbCommand())
          {
            // data quality
            command.Connection = conn;
            command.CommandText = "SELECT [PCK Code] as PCKCode, [Material Code] as MaterialCode, [Description] FROM [DataQuality$]";


            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
              adapter.Fill(dtConfidence);
            }

            this.UpdateGPRConfidenceDefinitions(dtConfidence, "Data Quality");
            //this.dtSetup.Merge(dtConfidence);

            // core correlation
            dtConfidence.Clear();
            dtConfidence = null;
            dtConfidence = new DataTable();
            command.CommandText = "SELECT [PCK Code] as PCKCode, [Material Code] as MaterialCode, [Description] FROM [CoreCorrelation$]";

            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
              adapter.Fill(dtConfidence);
            }

            this.UpdateGPRConfidenceDefinitions(dtConfidence, "Core Correlation");
            //this.dtSetup.Merge(dtConfidence);
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void ImportLayerVelocities()
    {
      this.fdOpenFile.Multiselect = false;
      this.fdOpenFile.DefaultExt = "xls";
      this.fdOpenFile.Filter = "MS Excel Files | *.xls";

      if (this.fdOpenFile.ShowDialog(this) == DialogResult.OK)
      {
        this.EnableButtons(false);
        this.ResetSetup();

        this.ssSetupStatus.Text = "Importing layer velocities ...";
        this.ssSetup.Update();

        logGPRTool("Import Layer Veolcity", "", this.fdOpenFile.FileName);

        this.taskType = TaskType.SETUP_VELOCITY;
        this.bgWorker.RunWorkerAsync(this.fdOpenFile.FileName);
      }
    }

    private void ImportLayerVelocities(string filename)
    {

      if (!QC_LayerVelocityXLS(filename))
      {
        return;
      }

      try
      {
        string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties=Excel 5.0";

        using (OleDbConnection conn = new OleDbConnection(connString))
        {
          conn.Open();

          // layer codes
          using (OleDbCommand command = new OleDbCommand())
          {
            command.Connection = conn;
            command.CommandText = "SELECT [Filename], [Layer Code] as LayerCode, [Velocity (m/ns)] as Velocity, " +
                        "[Begin MChain] as BeginMChain, [End MChain] as EndMChain FROM [LayerVelocity$] " +
                        " ORDER BY [Filename], [Layer Code]";

            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
              adapter.Fill(this.dtSetup);
            }
          }

          string sqlDelete = "exec gpr.usp_DeleteGPRLayerVelocity @ARANFileName, @LayerCode";
          string sqlInsert = "exec gpr.usp_InsertGPRLayerVelocity @ARANFileName, @LayerCode, @Velocity, @BeginMChain, @EndMChain";
          Dictionary<string, object> paras = new Dictionary<string, object>();
          string aranFilename, preFilename;
          int layerCode, preLayerCode;
          double velocity, beginMChain, endMChain;

          preFilename = "";
          preLayerCode = -9999;

          foreach (DataRow row in dtSetup.Rows)
          {
            aranFilename = Convert.ToString(row["Filename"]);
            layerCode = Convert.ToInt32(row["LayerCode"]);
            velocity = Convert.ToDouble(row["Velocity"]);
            beginMChain = Convert.ToDouble(row["BeginMChain"]);
            endMChain = Convert.ToDouble(row["EndMChain"]);

            // delete existing layer velocity
            if (aranFilename != preFilename || layerCode != preLayerCode)
            {
              paras.Clear();
              paras.Add("ARANFileName", aranFilename);
              paras.Add("LayerCode", layerCode);

              this.DataMapper.DataProvider.ExecuteNonQuery(sqlDelete, paras);
            }

            // insert new layer velocity
            paras.Clear();
            paras.Add("ARANFileName", aranFilename);
            paras.Add("LayerCode", layerCode);
            paras.Add("Velocity", velocity);
            paras.Add("BeginMChain", beginMChain);
            paras.Add("EndMChain", endMChain);

            this.dataMapper.DataProvider.ExecuteNonQuery(sqlInsert, paras);

            preFilename = aranFilename;
            preLayerCode = layerCode;
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private bool QC_LayerVelocityXLS(string filename)
    {
      DataTable dtTest = new DataTable();
      string error = "";

      string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties=Excel 5.0";

      using (OleDbConnection conn = new OleDbConnection(connString))
      {
        conn.Open();

        // layer codes
        using (OleDbCommand command = new OleDbCommand())
        {
          command.Connection = conn;
          command.CommandText = "SELECT TOP 1 * FROM [LayerVelocity$] ";

          using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
          {
            adapter.Fill(dtTest);
          }

          int columnCount = 1;

          foreach (DataColumn row in dtTest.Columns)
          {
            /*
            [Filename],[Layer Code],[Velocity (m/ns)],[Begin MChain],[End MChain]
            */
            switch (columnCount)
            {
              case 1:
                if (Convert.ToString(row.ColumnName) != "Filename")
                  error += "\n Column heading 'Filename' missing";
                break;
              case 2:
                if (Convert.ToString(row.ColumnName) != "Layer Code")
                  error += "\n Column heading 'Layer Code' missing";
                break;
              case 3:
                if (Convert.ToString(row.ColumnName) != "Velocity (m/ns)")
                  error += "\n Column heading 'Velocity (m/ns)' missing";
                break;
              case 4:
                if (Convert.ToString(row.ColumnName) != "Begin MChain")
                  error += "\n Column heading 'Begin MChain' missing";
                break;
              case 5:
                if (Convert.ToString(row.ColumnName) != "End MChain")
                  error += "\n Column heading 'End MChain' missing";
                break;

            }

            columnCount++;
          }

        }

        conn.Close();
      }

      if (error != "")
      {
        MessageBox.Show(error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }

      return true;
    }

    private void UpdateGPRConfidenceDefinitions(DataTable dtConfidence, string confType)
    {
      try
      {
        string sqlStr = "exec gpr.usp_UpdateGPRConfidenceDefinition @ConfidenceType, @Code, @Value, @Description";
        Dictionary<string, object> paras = new Dictionary<string, object>();

        foreach (DataRow row in dtConfidence.Rows)
        {
          paras.Clear();
          paras.Add("ConfidenceType", confType);
          paras.Add("Code", Convert.ToInt32(row["PCKCode"]));
          paras.Add("Value", Convert.ToString(row["MaterialCode"]));
          paras.Add("Description", Convert.ToString(row["Description"]));

          this.dataMapper.DataProvider.ExecuteNonQuery(sqlStr, paras);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private void ResetSetup()
    {
      this.dtSetup.Clear();
      this.dtSetup = null;
      this.dtSetup = new DataTable();

      this.dgvSetup.DataSource = null;
      this.ssSetupStatus.Text = "";
    }

    private void PerformSetupTask(TaskType taskType, DoWorkEventArgs e)
    {
      string filename = "";

      switch (taskType)
      {
        case TaskType.SETUP_LAYER:
          filename = e.Argument as string;
          this.ImportLayerCodes(filename);
          break;

        case TaskType.SETUP_VELOCITY:
          filename = e.Argument as string;
          this.ImportLayerVelocities(filename);
          break;
      }
    }

    private void CompleteSetupTask(TaskType taskType, RunWorkerCompletedEventArgs e)
    {
      switch (taskType)
      {
        case TaskType.SETUP_LAYER:
        case TaskType.SETUP_VELOCITY:
          this.dgvSetup.DataSource = this.dtSetup;

          if (!e.Cancelled && e.Error == null)
          {
            this.ssSetupStatus.Text = taskType == TaskType.SETUP_LAYER ?
                          "Layer codes have been imported." : "Layer velocities have been imported.";
          }
          else
          {
            this.sstsGPRStatus.Text = taskType == TaskType.SETUP_LAYER ?
                          "Importing layer codes failed." : "Importing layer velocities failed.";
          }
          break;
      }
    }
    #endregion

    #region MAR
    private bool CheckSelectedCollectionReadyForMar(out string msg, out List<string> noList)
    {
      bool ready = true;

      msg = null;
      noList = new List<string>();

      try
      {
        // get transaction id
        string sqlComm = "SELECT IDTransition FROM wf.Transitions t " +
                 "INNER JOIN wf.Actions a ON t.IDAction = a.IDAction WHERE a.ActionName = 'ExportMAR'";
        int IDTransaction = Convert.ToInt32(this.dataMapper.DataProvider.ExecuteScalar(sqlComm, null));

        // check if transaction allowed
        int IDSession;
        int count;
        string uniquerun;

        foreach (DataRow row in this.dtMarCollections.Rows)
        {
          IDSession = Convert.ToInt32(row["IDSession"]);
          sqlComm = "SELECT count(*) FROM wf.fn_GetSessionTransitionsAllowed(" + IDSession.ToString() +
                ", " + IDTransaction.ToString() + ")";
          count = Convert.ToInt32(this.dataMapper.DataProvider.ExecuteScalar(sqlComm, null));

          if (count <= 0)
          {
            ready = false;
            uniquerun = Convert.ToString(row["UniqueRun"]);

            if (!string.IsNullOrEmpty(msg))
            {
              msg = msg + ", " + uniquerun;
            }
            else
            {
              msg = uniquerun;
            }

            if (!noList.Contains(uniquerun))
            {
              noList.Add(uniquerun);
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }

      return ready;
    }

    private void CompleteMARTask(TaskType taskType, RunWorkerCompletedEventArgs e)
    {
      switch (taskType)
      {
        case TaskType.MAR_SELECT:
          this.ucMarCollections.DataSource = this.dtMarCollections;
          this.dgvMars.DataSource = this.dtMars;
          this.sstsMarStatus.Text = "";

          foreach (DataGridViewColumn col in this.dgvMars.Columns)
          {
            //if (col.DataPropertyName == "DistanceStamp")
            //{
            //  col.Visible = false;
            //}
          }

          this.sstsMarCollections.Text = this.MarCollections.Count == 0 ? string.Empty :
                           this.MarCollections.Count.ToString() + " collections " +
                           this.dtMars.Rows.Count.ToString() + " rows";
          break;

        case TaskType.MAR_CREATE:
          if (!e.Cancelled && e.Error == null)
          {
            this.sstsMarStatus.Text = "Marker files have been created.";
          }
          else
          {
            this.sstsMarStatus.Text = "Creating marker files failed.";
          }

          break;

        case TaskType.MAR_CREATE_CSV:
          if (!e.Cancelled && e.Error == null)
          {
            this.sstsMarStatus.Text = "CSV Marker file has been created.";
          }
          else
          {
            this.sstsMarStatus.Text = "Creating CSV marker file failed.";
          }

          break;

        case TaskType.REPORTINTERVAL_IMPORT:
          //this.ucCoreReqCollections.DataSource = this.dtCoreReqCollections;
          //this.dgvCoreRequest.DataSource = this.dtRawCoreRequests;

          this.sstsMarStatus.Text = "";

          break;

      }

      this.sstsMarProgress.Visible = false;
    }

    private DataTable ConsolidateUniqueRun()
    {
      DataView dv = new DataView(dtMars);
      DataTable dtDstnctUniqueRun = dv.ToTable(true, "ARANFileName");

      dtDstnctUniqueRun.Columns.Add("RootUniqueRun", Type.GetType("System.String"));
      foreach (DataRow dr in dtDstnctUniqueRun.Rows)
      {
        dr["RootUniqueRun"] = dr["ARANFileName"].ToString().Substring(0, 6);
      }

      var gRslt =
        from row in dtDstnctUniqueRun.AsEnumerable()
        group row by new { rur = row["RootUniqueRun"] } into g
        select new
        {
          minUniqueRun = g.Min(x => x["ARANFileName"])
        };

      DataTable dtMinUniqueRun = new DataTable();
      dtMinUniqueRun.TableName = "MinUniqueRun";
      dtMinUniqueRun.Columns.Add("ARANFileName", Type.GetType("System.String"));

      foreach (var itm in gRslt)
      {
        DataRow dr = dtMinUniqueRun.NewRow();
        dr["ARANFileName"] = itm.minUniqueRun;
        dtMinUniqueRun.Rows.Add(dr);
      }

      var jRslt =
        from tbl1 in dtMars.AsEnumerable()
        join tbl2 in dtMinUniqueRun.AsEnumerable()
        on tbl1.Field<string>("ARANFileName") equals tbl2.Field<string>("ARANFileName")
        select tbl1.ItemArray;

      DataTable dt = dtMars.Clone();

      foreach (object[] itm in jRslt)
      {
        dt.Rows.Add(itm);
      }

      return dt;
    }

    private void CreateMarCSVFile()
    {
      bool ready = true;
      string msg = null;
      List<string> noList = new List<string>();

      if (this.MarCollections.Count > 0)
      {
        this.EnableButtons(false);

        try
        {
          this.sstsMarStatus.Text = "Checking collections...";
          this.ssMar.Update();

          //PO - override the checks, the workflow does not work properly yet

          /*
          if (!this.CheckSelectedCollectionReadyForMar(out msg, out noList))
          {
              this.sstsMarStatus.Text = "";
              this.ssMar.Update();

              string message;

              if (this.MarCollections.Count == noList.Count)
              {
                  ready = false;
                  message = "Cannot create marker files for all selected collections. " +
                            "Please check workflow to see if they have been segmented and had events merged.";
                  MessageBox.Show(message, "Warning");
              }
              else
              {
                  message = "Cannot create marker files for following collections: " + msg +
                            ".\nPlease check workflow to see if they have been segmented and had events merged. \n\n" +
                            "Click Yes button to create marker files excluding the above collections. \n" +
                            "Click No button to stop.";

                  if (MessageBox.Show(message, "Warning", MessageBoxButtons.YesNo) == DialogResult.No)
                  {
                      ready = false;
                  }
              }
          }
          */

          this.sstsMarStatus.Text = "";
          this.ssMar.Update();

          this.sfSaveFile.DefaultExt = "CSV";
          this.sfSaveFile.Filter = "MS Excel CSV File | *.csv";
          if (ready && this.sfSaveFile.ShowDialog(this) == DialogResult.OK)
          {
            this.sstsMarStatus.Text = "Creating marker CSV file...";
            this.ssMar.Update();

            this.sstsMarProgress.Visible = true;
            this.taskType = TaskType.MAR_CREATE_CSV;
            logGPRTool("Create MAR CSV file", "", "");
            this.bgWorker.RunWorkerAsync(this.sfSaveFile.FileName);
          }
          else
          {
            this.EnableButtons(true);
          }
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.StackTrace, ex.Message);
          this.EnableButtons(true);
        }
      }
      else
      {
        MessageBox.Show("Please select collections first.", "Warning");
      }
    }

    private void CreateMarCSVFile(string fileFullName)
    {
      Excel.Application excelApp = null;
      Excel.Workbook workbook = null;
      Excel.Worksheet worksheet = null;
      VBIDE.VBComponent module = null;

      try
      {
        // create workbook and worksheet
        excelApp = new Excel.Application();
        //excelApp.Visible = true;
        excelApp.Visible = false;
        excelApp.UserControl = false;

        workbook = excelApp.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet) as Excel.Workbook;
        worksheet = workbook.ActiveSheet as Excel.Worksheet;
        // CSV files do not allow the worksheet to be named anything different from the file name.
        // The saveas will automatically name the worksheet tab to the file name.

        // add header
        DataTable dt = ConsolidateUniqueRun();

        int totalCols = dt.Columns.Count;
        int colNo = 0;
        int colSkip = -1;
        for (int i = 0; i < totalCols; i++)
        {
          //if (dt.Columns[i].ColumnName.ToLower() != "distancestamp")
          //{
            colNo++;
            worksheet.Cells[1, colNo] = dt.Columns[i].ColumnName;
          //}
          //else
          //{
          //  colSkip = i;
          //}
        }

        // add contents
        int totalRows = dt.Rows.Count;
        int startRowIndex = 2;
        for (int row = 0; row < totalRows; row++)
        {
          for (int col = 0; col < totalCols; col++)
          {
            //if (col < colSkip) { colNo = col + 1; } else { colNo = col; }
            if (col < colSkip || colSkip < 0) { colNo = col + 1; } else { colNo = col; }
            if (col != colSkip)
            {
              worksheet.Cells[row + startRowIndex, colNo] = dt.Rows[row][col].ToString();
            }
          }
        }

        // save
        if (File.Exists(fileFullName)) { File.Delete(fileFullName); }
        
        workbook.SaveAs(fileFullName, Excel.XlFileFormat.xlCSV, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
              Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        workbook.Close(false, Type.Missing, Type.Missing);
      }
      catch (Exception ex)
      {
        throw ex;
      }
      finally
      {
        if (excelApp != null)
        {
          excelApp.Quit();
        }

        if (module != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(module);
          module = null;
        }

        if (worksheet != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(worksheet);
          worksheet = null;
        }

        if (workbook != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);
          workbook = null;
        }

        if (excelApp != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
          excelApp = null;
        }

        GC.Collect();
      }
    }

    private void CreateMarFiles()
    {
      bool ready = true;
      string msg = null;
      List<string> noList = new List<string>();

      if (this.MarCollections.Count > 0)
      {
        this.EnableButtons(false);

        try
        {
          this.sstsMarStatus.Text = "Checking collections...";
          this.ssMar.Update();

          //PO - override the checks, the workflow does not work properly yet

          /*
          if (!this.CheckSelectedCollectionReadyForMar(out msg, out noList))
          {
              this.sstsMarStatus.Text = "";
              this.ssMar.Update();

              string message;

              if (this.MarCollections.Count == noList.Count)
              {
                  ready = false;
                  message = "Cannot create marker files for all selected collections. " +
                            "Please check workflow to see if they have been segmented and had events merged.";
                  MessageBox.Show(message, "Warning");
              }
              else
              {
                  message = "Cannot create marker files for following collections: " + msg +
                            ".\nPlease check workflow to see if they have been segmented and had events merged. \n\n" +
                            "Click Yes button to create marker files excluding the above collections. \n" +
                            "Click No button to stop.";

                  if (MessageBox.Show(message, "Warning", MessageBoxButtons.YesNo) == DialogResult.No)
                  {
                      ready = false;
                  }
              }
          }
          */

          this.sstsMarStatus.Text = "";
          this.ssMar.Update();

          if (ready && this.fbSelectFolder.ShowDialog(this) == DialogResult.OK)
          {
            string path = this.fbSelectFolder.SelectedPath;

            this.sstsMarStatus.Text = "Creating marker files ...";
            this.ssMar.Update();

            this.sstsMarProgress.Visible = true;
            this.taskType = TaskType.MAR_CREATE;

            List<object> paras = new List<object>();
            paras.Add(path);
            paras.Add(noList);

            logGPRTool("Create MAR files", "", "");

            this.bgWorker.RunWorkerAsync(paras);
          }
          else
          {
            this.EnableButtons(true);
          }
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.StackTrace, ex.Message);
          this.EnableButtons(true);
        }
      }
      else
      {
        MessageBox.Show("Please select collections first.", "Warning");
      }
    }

    private void CreateMarFiles(string path, string filename, DataRow[] events)
    {
      try
      {
        // delete existing file
        string fileFullName;
        if (!path.EndsWith("\\"))
        {
          path = path + "\\";
        }

        fileFullName = path + filename + "001.txt";

        if (File.Exists(fileFullName))
        {
          File.Delete(fileFullName);
        }

        // write to file
        StreamWriter writer = new StreamWriter(fileFullName);
        string chainage;
        string comment;

        foreach (DataRow row in events)
        {
          chainage = Convert.ToString(row["Chainage"]);
          comment = Convert.ToString(row["Comment"]);

          writer.WriteLine(chainage + " " + comment);
        }

        writer.Flush();
        writer.Close();
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private void ImportReportingInterval()
    {
      this.fdOpenFile.DefaultExt = "CSV";
      this.fdOpenFile.Filter = "CSV | *.CSV";
      this.fdOpenFile.Multiselect = false;

      if (this.fdOpenFile.ShowDialog(this) == DialogResult.OK)
      {
        this.EnableButtons(false);
        this.sstsMarProgress.Text = "Importing Reporting Interval...";

        this.ssMar.Update();
        this.taskType = TaskType.REPORTINTERVAL_IMPORT;

        this.sstsMarProgress.Visible = true;

        //log action
        logGPRTool("Import Reporting Interval", "", this.fdOpenFile.FileName);

        this.bgWorker.RunWorkerAsync(this.fdOpenFile.FileName);
      }
    }

    private void ImportReportingInterval(string filename)
    {

      System.IO.StreamReader file = new System.IO.StreamReader(filename);

      int count = 0;

      string county, route, direction, subRoute, classification;
      int lane, interval;
      double beginChainage, endChainage;
      object result;
      string message = string.Empty;
      string line = null;

      Dictionary<string, object> paras = new Dictionary<string, object>();

      try
      {
        //foreach (DataRow row in dtReportInterval.Rows)
        while ((line = file.ReadLine()) != null)
        {
          //skip the first line, contains the headers
          if (count != 0)
          {
            paras.Clear();
            char delimeter = ',';
            string[] data = line.Split(delimeter);

            //check for blank line
            if (data.Length > 1)
            {

              county = data[0].ToString();
              route = data[1].ToString();
              lane = Convert.ToInt32(data[2]);
              direction = data[3].ToString();
              subRoute = data[4].ToString();
              beginChainage = Convert.ToDouble(data[5]);
              endChainage = Convert.ToDouble(data[6]);
              classification = data[7].ToString();
              interval = Convert.ToInt32(data[8]);

              while (route.Length < 3)
              {
                route = '0' + route;
              }

              paras.Add("County", county);
              paras.Add("Route", route);
              paras.Add("Lane", lane);
              paras.Add("Direction", direction);
              paras.Add("SubRoute", subRoute);
              paras.Add("BeginChainage", beginChainage);
              paras.Add("EndChainage", endChainage);
              paras.Add("Classification", classification);
              paras.Add("Interval", interval);

              string sqlImport = "exec gpr.usp_ImportReportingInterval @County, @Route, @Lane, @Direction, @SubRoute, @BeginChainage, @EndChainage, @Classification, @Interval";
              this.dataMapper.DataProvider.ExecuteNonQuery(sqlImport, paras);

            }
          }

          count++;
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }

      file.Close();
    }

    private void LoadMarCollections(string categories)
    {
      List<string> errorFiles = new List<string>();

      try
      {
        this.MarCollections.Clear();
        this.MarCollections = this.GetCollectionList(this.dtMarCollections);

        // load events
        if (this.MarCollections.Count > 0)
        {
          this.dtMars.Clear();
          Dictionary<string, object> paras = new Dictionary<string, object>();
          string sqlComm = string.Empty;
          DialogResult dr = MessageBox.Show("Aggregate events?", "MARs", MessageBoxButtons.YesNo);

          foreach (string cls in this.MarCollections)
          {
            paras.Clear();
            paras.Add("UniqueRun", cls);
            paras.Add("Categories", categories);
            if (dr == DialogResult.No)
            {
              paras.Add("AggregateEvents", 0);
              sqlComm = "exec gpr.usp_GetMarByCategories @UniqueRun, @Categories, @AggregateEvents";
              //sqlComm = "exec dbo.##usp_GetMarByCategories @UniqueRun, @Categories, @AggregateEvents";
            }
            else
            {
              sqlComm = "exec gpr.usp_GetMarByCategories @UniqueRun, @Categories";
              //sqlComm = "exec dbo.##usp_GetMarByCategories @UniqueRun, @Categories";
            }
            //sqlComm = "exec gpr.usp_GetMarBySession @UniqueRun";

            try
            {
              this.dataMapper.DataProvider.FillDataTable(dtMars, sqlComm, paras);
            }
            catch (Exception ex)
            {
              errorFiles.Add(cls);
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }

      if (errorFiles.Count > 0)
      {
        string errorMsg = "Error with the following " + errorFiles.Count.ToString() + " files: \n\n";
        foreach (string file in errorFiles)
        {
          errorMsg += file + "\n ";
        }

        MessageBox.Show(errorMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void PerformMARTask(TaskType taskType, DoWorkEventArgs e)
    {
      try
      {
        switch (taskType)
        {
          case TaskType.MAR_SELECT:
            string categories = e.Argument as string;
            this.LoadMarCollections(categories);
            break;

          case TaskType.MAR_CREATE:
            List<object> paras = e.Argument as List<object>;
            string path = paras[0] as string;
            List<string> noList = paras[1] as List<string>;

            foreach (string cls in this.MarCollections)
            {
              if (!noList.Contains(cls))
              {
                DataRow[] rows = this.dtMars.Select("ARANFileName = '" + cls + "'", "DistanceStamp ASC");
                this.CreateMarFiles(path, cls, rows);
              }
            }
            break;

          case TaskType.MAR_CREATE_CSV:
            string filename = e.Argument as string;
            this.CreateMarCSVFile(filename);

            break;

          case TaskType.REPORTINTERVAL_IMPORT:
            string fileName = e.Argument as string;
            this.ImportReportingInterval(fileName);
            break;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private void ResetMar()
    {
      this.dtMars.Clear();
      this.dtMarCollections.Clear();
      this.MarCollections.Clear();
      this.ucMarCollections.DataSource = null;
      this.dgvMars.DataSource = null;
      this.sstsMarCollections.Text = "";
      this.sstsMarStatus.Text = "";
    }

    private void SelectMarCollections()
    {
      SelectCollections sc = new SelectCollections(this.dataMapper);
      dtMars.TableName = "MAR_Data";

      if (sc.ShowDialog(this) == DialogResult.OK)
      {
        // select categories
        DataTable dtCategories = this.dataMapper.DataProvider.ExecuteDataTable("SELECT DISTINCT Category FROM gpr.GPREventDescription", null);

        List<string> categories = new List<string>();
        foreach (DataRow row in dtCategories.Rows)
        {
          categories.Add(Convert.ToString(row["Category"]));
        }

        MarEventCategories mCategory = new MarEventCategories(categories);
        if (mCategory.ShowDialog(this) == DialogResult.OK)
        {
          this.ResetMar();

          // set selected collections
          this.sstsMarStatus.Text = "Loading selected collections ...";
          this.ssMar.Update();

          this.dtMarCollections = sc.GroupPartitionExplorer.SelectedCollections;

          this.EnableButtons(false);

          this.sstsMarStatus.Text = "Loading MARs ...";
          this.ssMar.Update();

          string groupName = "";

          if (sc.GroupPartitionExplorer.GroupFullpath != null)
          {
            foreach (string g in sc.GroupPartitionExplorer.GroupFullpath)
            {
              groupName += g + " ";
            }
          }

          logGPRTool("Select MAR Collections", groupName, "");

          this.sstsMarProgress.Visible = true;
          this.taskType = TaskType.MAR_SELECT;
          this.bgWorker.RunWorkerAsync(mCategory.SelectedCategories);
        }
      }
    }

    private void tsMar_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {
      string action = Convert.ToString(e.ClickedItem.Tag);

      switch (action)
      {
        case "Select":
          SelectMarCollections();
          break;

        case "Create":
          CreateMarFiles();
          break;

        case "Create CSV":
          CreateMarCSVFile();
          break;

        case "Reset":
          ResetMar();
          break;

        case "Import Reporting Interval":
          ImportReportingInterval();
          break;
      }
    }
    #endregion

    #region Core Request
    private void CompleteCoreRequestTask(TaskType taskType, RunWorkerCompletedEventArgs e)
    {
      this.sstsCoreReqStatus.Text = "";

      switch (taskType)
      {
        case TaskType.COREREQ_IMPORT:
          this.ucCoreReqCollections.DataSource = this.dtCoreReqCollections;
          this.dgvCoreRequest.DataSource = this.dtRawCoreRequests;

          this.sstsCoreReqCollections.Text = this.CoreReqCollections.Count == 0 ? string.Empty :
                             this.CoreReqCollections.Count.ToString() + " collections " +
                             this.dtRawCoreRequests.Rows.Count.ToString() + " cores";
          break;

        case TaskType.COREREQ_GENERATE:
          this.ucCoreReqCollections.DataSource = this.dtCoreReqCollections;
          this.dgvCoreRequest.DataSource = this.dtCoreRequests;

          this.FormatDataGridView(this.dgvCoreRequest);

          this.sstsCoreReqCollections.Text = this.CoreReqCollections.Count == 0 ? string.Empty :
                             this.CoreReqCollections.Count.ToString() + " collections " +
                             this.dtCoreRequests.Rows.Count.ToString() + " cores";
          break;

        case TaskType.COREREQ_EXPORT:
          if (!e.Cancelled && e.Error == null)
          {
            this.sstsCoreReqStatus.Text = "Core requests have been exported.";
          }
          else
          {
            this.sstsCoreReqStatus.Text = "Export core requests failed.";
          }

          break;
      }

      this.sstsCoreRequestProgressBar.Visible = false;
    }

    private void CreateCoreRequestTextFile(string filename)
    {
      if (this.CoreReqCollections.Count == 0 || this.dtCoreRequests.Rows.Count == 0)
      {
        return;
      }

      try
      {
        if (File.Exists(filename))
        {
          File.Delete(filename);
        }

        StreamWriter writer = new StreamWriter(filename);

        // header
        int totalCols = this.dtCoreRequests.Columns.Count;
        string header = string.Empty;

        for (int i = 1; i <= totalCols; i++)
        {
          header = header + "," + this.dtCoreRequests.Columns[i - 1].ColumnName;
        }

        if (header.StartsWith(","))
        {
          header = header.Substring(1);
        }

        writer.WriteLine(header);

        // content
        string imagePath;
        string content;
        int totalRows;

        foreach (string cls in this.CoreReqCollections)
        {
          DataRow[] rows = this.dtCoreRequests.Select("COREREQ_ID LIKE '" + cls + "%'", "COREREQ_ID ASC");

          if (rows.Length > 0)
          {
            imagePath = "Images\\" + cls + "\\";
            totalRows = rows.Length;

            for (int i = 0; i < totalRows; i++)
            {
              content = string.Empty;
              for (int j = 1; j <= totalCols; j++)
              {
                if (j != totalCols - 2)
                {
                  content = content + "," + Convert.ToString(rows[i][j - 1]);
                }
                else
                {
                  content = content + "," + imagePath + Convert.ToString(rows[i]["IMAGENAME"]);
                }
              }

              if (content.StartsWith(","))
              {
                content = content.Substring(1);
              }

              writer.WriteLine(content);
            }
          }
        }

        writer.Flush();
        writer.Close();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void DeleteCoreRequests()
    {
      if (this.dgvCoreRequest.SelectedRows.Count > 0)
      {
        if (MessageBox.Show("Delete selected core requests?", "Delete Core Requests", MessageBoxButtons.YesNo) == DialogResult.Yes)
        {
          this.EnableButtons(false);
          this.sstsCoreReqStatus.Text = "Deleting core requests...";

          string coreId;
          object result;
          Dictionary<string, object> paras = new Dictionary<string, object>();
          string sqlComm = "exec gpr.usp_DeleteCoreRequest @CoreReqID";

          try
          {
            foreach (DataGridViewRow row in this.dgvCoreRequest.SelectedRows)
            {
              coreId = Convert.ToString(row.Cells["CoreReqID"].Value);
              paras.Clear();

              paras.Add("CoreReqID", coreId);
              result = this.dataMapper.DataProvider.ExecuteScalar(sqlComm, paras);

              if (result != null && Convert.ToInt32(result) == 1)
              {
                DataRow[] rows = this.dtCoreRequests.Select("CoreReqID = '" + coreId + "'");
                if (rows.Length > 0)
                {
                  this.dtCoreRequests.Rows.Remove(rows[0]);
                }
              }
              else if (result != null && Convert.ToInt32(result) == 0)
              {
                MessageBox.Show("The " + coreId + " cannot be deleted because there is a core result associated with it.\n" +
                        "Please delete the core result first and then delete the core request.", "Warning");
              }
            }

            this.dtCoreRequests.AcceptChanges();
          }
          catch (Exception ex)
          {
            MessageBox.Show(ex.StackTrace, ex.Message);
          }
          finally
          {
            this.dgvCoreRequest.DataSource = this.dtCoreRequests;
            this.sstsCoreReqCollections.Text = this.CoreReqCollections.Count.ToString() + " collections " +
                               this.dtCoreRequests.Rows.Count.ToString() + " cores";

            this.sstsCoreReqStatus.Text = "";
            EnableButtons(true);
          }
        }
      }
    }

    private void ExportCoreRequests()
    {
      if (this.CoreReqCollections.Count > 0 && this.dtCoreRequests.Rows.Count > 0)
      {
        this.sfSaveFile.DefaultExt = "XLS";
        this.sfSaveFile.Filter = "MS Excel File | *.xls";
        if (this.sfSaveFile.ShowDialog(this) == DialogResult.OK)
        {
          this.EnableButtons(false);
          this.sstsCoreReqStatus.Text = "Exporting core requests...";
          this.ssCoreRequest.Update();

          this.taskType = TaskType.COREREQ_EXPORT;
          this.sstsCoreRequestProgressBar.Visible = true;
          this.bgWorker.RunWorkerAsync(this.sfSaveFile.FileName);
        }
      }
      else
      {
        MessageBox.Show("Please generate core requests first.", "Warning");
      }
    }

    private void ExportCoreRequests(string fileFullName)
    {
      Excel.Application excelApp = null;
      //Excel.ApplicationClass excelApp = null;
      Excel.Workbook workbook = null;
      Excel.Worksheet worksheet = null;
      VBIDE.VBComponent module = null;

      try
      {
        // create .csv file
        //CreateCoreRequestTextFile(fileFullName.Substring(0, fileFullName.Length - 3) + "csv");

        // create workbook and worksheet
        //excelApp = new Excel.ApplicationClass();
        excelApp = new Excel.Application();
        excelApp.Visible = false;
        excelApp.UserControl = false;

        workbook = excelApp.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet) as Excel.Workbook;
        worksheet = workbook.ActiveSheet as Excel.Worksheet;

        worksheet.Name = "Core Request";

        // add header
        int totalCols = this.dtCoreRequests.Columns.Count;
        for (int i = 1; i <= totalCols; i++)
        {
          worksheet.Cells[1, i] = this.dtCoreRequests.Columns[i - 1].ColumnName;
          Excel.Range rng = worksheet.Cells[1, i] as Excel.Range;
          rng.EntireColumn.AutoFit();
        }

        // add contents
        string rootPath;
        string imagePath;
        string srcImageName;
        string imagename;
        string dstImageName;
        string relativePath;
        int totalRows;
        int startRowIndex = 2;

        foreach (string cls in this.CoreReqCollections)
        {
          DataRow[] rows = this.dtCoreRequests.Select("CoreReqID LIKE '" + cls + "%'", "CoreReqID ASC");

          if (rows.Length > 0)
          {
            // create image folder
            rootPath = fileFullName.Substring(0, fileFullName.LastIndexOf("\\"));
            imagePath = rootPath + "\\Images\\" + cls + "\\";
            if (!Directory.Exists(imagePath))
            {
              Directory.CreateDirectory(imagePath);
            }

            totalRows = rows.Length;

            for (int i = 0; i < totalRows; i++)
            {
              for (int j = 1; j <= totalCols; j++)
              {
                worksheet.Cells[i + startRowIndex, j] = rows[i][j - 1];
              }

              srcImageName = Convert.ToString(rows[i]["IMAGEPATH"]);

              if (!string.IsNullOrEmpty(srcImageName) && File.Exists(srcImageName))
              {
                imagename = srcImageName.Substring(srcImageName.LastIndexOf("\\") + 1);
                dstImageName = imagePath + imagename;
                File.Copy(srcImageName, dstImageName, true);

                relativePath = "Images\\" + cls + "\\" + imagename;

                Excel.Range rng = worksheet.Cells[i + startRowIndex, totalCols - 2] as Excel.Range;
                worksheet.Hyperlinks.Add(rng, dstImageName, Type.Missing, Type.Missing, relativePath);
              }

            }
            startRowIndex = startRowIndex + totalRows;
          }
        }

        #region macro
        //// create macro to change image link
        //module = workbook.VBProject.VBComponents.Add(VBIDE.vbext_ComponentType.vbext_ct_StdModule);
        //string code = "Sub ChangeHyperlink()\r\n" +
        //              "		Dim oldRoot As String\r\n" +
        //              "		Dim newRoot As String\r\n" +
        //              "		Dim h As Hyperlink\r\n" +
        //              "		newRoot = InputBox(\"Type new image root path\")\r\n" +
        //              "		For Each h In ActiveSheet.Hyperlinks\r\n" +
        //              "			x = InStrRev(h.Address, \"Images\\\")\r\n" +
        //              "			If x <= 1 Then\r\n" +
        //              "				If Right(newRoot, 1) <> \"\\\" Then\r\n" +
        //              "					h.Address = newRoot + \"\\\" + h.Address\r\n" +
        //              "				Else\r\n" +
        //              "					h.Address = newRoot + h.Address\r\n" +
        //              "				End If\r\n" +
        //              "			Else\r\n" +
        //              "				oldRoot = Mid(h.Address, 1, x - 1)\r\n" +
        //              "				If Right(newRoot, 1) <> \"\\\" Then\r\n" +
        //              "					h.Address = WorksheetFunction.Substitute(h.Address, oldRoot, newRoot + \"\\\")\r\n" +
        //              "				Else\r\n" +
        //              "					h.Address = WorksheetFunction.Substitute(h.Address, oldRoot, newRoot)\r\n" +
        //              "				End If\r\n" +
        //              "			End If\r\n" +
        //              "		Next\r\n" +
        //              "End Sub";

        //module.CodeModule.AddFromString(code);
        #endregion

        // save
        workbook.SaveAs(fileFullName, Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
              Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
        workbook.Close(true, fileFullName, Type.Missing);
      }
      catch (Exception ex)
      {
        throw ex;
      }
      finally
      {
        if (excelApp != null)
        {
          excelApp.Quit();
        }

        if (module != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(module);
          module = null;
        }

        if (worksheet != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(worksheet);
          worksheet = null;
        }

        if (workbook != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);
          workbook = null;
        }

        if (excelApp != null)
        {
          System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
          excelApp = null;
        }

        GC.Collect();
      }
    }

    private void GenerateCoreRequests()
    {
      try
      {
        if (this.CoreReqCollections.Count == 0)
        {
          SelectCollections sc = new SelectCollections(this.dataMapper);
          if (sc.ShowDialog(this) == DialogResult.OK)
          {
            this.sstsCoreReqStatus.Text = "Loading collections ...";
            this.ssCoreRequest.Update();

            this.dtCoreReqCollections = sc.GroupPartitionExplorer.SelectedCollections;
            this.CoreReqCollections = this.GetCollectionList(this.dtCoreReqCollections);

            this.sstsCoreReqStatus.Text = "";
            this.ssCoreRequest.Update();
          }
        }

        if (this.CoreReqCollections.Count > 0)
        {
          GPRSettings gs = new GPRSettings(DisplayOption.ChainageUnit);
          gs.SetChainageUnit(DisplayOption.ChainageUnit, this.coreUnits.ChainageUnit);

          if (gs.ShowDialog(this) == DialogResult.OK)
          {
            this.coreUnits.ChainageUnit = gs.GetChainageUnit(DisplayOption.ChainageUnit);
            double chainageUnitConversion = gs.GetConversionFactor(this.coreUnits.ChainageUnit);

            this.EnableButtons(false);
            this.sstsCoreReqStatus.Text = "Generating core requests ...";
            this.ssCoreRequest.Update();

            this.dtRawCoreRequests.Clear();
            this.dgvCoreRequest.DataSource = null;

            this.sstsCoreRequestProgressBar.Visible = true;
            this.taskType = TaskType.COREREQ_GENERATE;
            this.bgWorker.RunWorkerAsync(chainageUnitConversion);
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void GenerateCoreRequests(double chainageUnitConversion)
    {
      if (this.CoreReqCollections.Count > 0)
      {
        try
        {
          this.dtCoreRequests.Clear();
          string sqlComm;
          Dictionary<string, object> paras = new Dictionary<string, object>();

          foreach (string cls in this.CoreReqCollections)
          {
            paras.Clear();
            paras.Add("ARANFileName", cls);
            paras.Add("ChainageUnitConversion", chainageUnitConversion);

            sqlComm = "exec gpr.usp_GetCoreRequest @ARANFileName, @ChainageUnitConversion";

            this.dataMapper.DataProvider.FillDataTable(dtCoreRequests, sqlComm, paras);
          }
        }
        catch (Exception ex)
        {
          throw ex;

        }


      }
    }

    private void ImportCoreRequests()
    {
      this.fdOpenFile.DefaultExt = "PCK";
      this.fdOpenFile.Filter = "Core Request | *.PCK";
      this.fdOpenFile.Multiselect = true;

      if (this.fdOpenFile.ShowDialog(this) == DialogResult.OK)
      {
        this.EnableButtons(false);
        this.sstsCoreReqStatus.Text = "Creating core requests...";
        this.ssCoreRequest.Update();

        this.ResetCoreRequests();

        logGPRTool("Import Core Requests", "", this.fdOpenFile.FileNames[0].ToString());

        this.taskType = TaskType.COREREQ_IMPORT;
        this.sstsCoreRequestProgressBar.Visible = true;
        this.bgWorker.RunWorkerAsync(this.fdOpenFile.FileNames);
      }
    }

    private void ImportCoreRequests(DataTable dtRequest)
    {
      string sqlComm = "exec gpr.usp_CreateCoreRequest @ARANFileName, @MChain, @Description";
      Dictionary<string, object> paras = new Dictionary<string, object>();

      //TODO: change this to report the files that have errors
      try
      {
        foreach (string uniquerun in this.CoreReqCollections)
        {
          DataRow[] rows = dtRequest.Select("ARANFileName = '" + uniquerun + "'", "MChain ASC");

          if (rows.Length > 0)
          {
            // create new request
            for (int i = 1; i <= rows.Length; i++)
            {
              paras.Clear();

              paras.Add("ARANFileName", Convert.ToString(rows[i - 1]["ARANFileName"]));
              paras.Add("MChain", Convert.ToDouble(rows[i - 1]["MChain"]));
              paras.Add("Description", "");

              this.dataMapper.DataProvider.ExecuteNonQuery(sqlComm, paras);
            }
          }
        }

        this.dtRawCoreRequests = dtRequest;
        this.dtCoreReqCollections = this.GetCollections(this.CoreReqCollections);
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private void PerformCoreRequestTask(TaskType taskType, DoWorkEventArgs e)
    {
      try
      {
        switch (taskType)
        {
          case TaskType.COREREQ_IMPORT:
            string[] filenames = e.Argument as string[];
            DataTable dtCoreReq = this.ReadCoreRequests(filenames);
            this.ImportCoreRequests(dtCoreReq);
            break;

          case TaskType.COREREQ_GENERATE:
            double chainageUnitConversion = Convert.ToDouble(e.Argument);
            this.GenerateCoreRequests(chainageUnitConversion);
            break;

          case TaskType.COREREQ_EXPORT:
            string filename = e.Argument as string;
            this.ExportCoreRequests(filename);
            break;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private DataTable ReadCoreRequests(string[] fileNames)
    {
      DataTable dt = new DataTable();
      dt.Columns.Add("ARANFileName", typeof(string));
      dt.Columns.Add("MChain", typeof(double));

      this.CoreReqCollections.Clear();

      try
      {
        foreach (string fileName in fileNames)
        {
          ReadCoreRequests(fileName, ref dt);
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }

      return dt;
    }

    private DataTable ReadCoreRequests(string fileName, ref DataTable dt)
    {
      string line, uniquerun;
      string[] data;

      StreamReader reader = new StreamReader(fileName);

      try
      {
        line = reader.ReadLine();
        while (line != null)
        {
          data = line.Split(new string[] { "\t", " " }, 11, StringSplitOptions.RemoveEmptyEntries);

          if (data.Length > 5)
          {
            uniquerun = data[0].Substring(0, 8);

            if (!this.CoreReqCollections.Contains(uniquerun))
            {
              this.CoreReqCollections.Add(uniquerun);
            }

            DataRow row = dt.NewRow();
            row["ARANFileName"] = uniquerun;
            row["MChain"] = Convert.ToDouble(data[4]);
            dt.Rows.Add(row);
          }

          line = reader.ReadLine();
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      finally
      {
        reader.Close();
      }

      return dt;
    }

    private void ResetCoreRequests()
    {
      this.dtCoreRequests.Clear();
      this.dtRawCoreRequests.Clear();
      this.dtCoreReqCollections.Clear();
      this.CoreReqCollections.Clear();
      this.ucCoreReqCollections.DataSource = null;
      this.dgvCoreRequest.DataSource = null;
      this.sstsCoreReqCollections.Text = "";
      this.sstsCoreReqStatus.Text = "";
    }

    private void tsCoreRequest_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {
      string action = Convert.ToString(e.ClickedItem.Tag);

      switch (action)
      {
        case "Import":
          ImportCoreRequests();
          break;

        case "Generate":
          GenerateCoreRequests();
          break;

        case "Export":
          ExportCoreRequests();
          break;

        case "Delete":
          DeleteCoreRequests();
          break;

        case "Reset":
          ResetCoreRequests();
          break;
      }
    }

    #endregion

    #region Core Results
    private void tsCoreResult_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {
      string action = Convert.ToString(e.ClickedItem.Tag);

      switch (action)
      {
        case "Import":
          ImportCoreResults();
          break;

        case "Select":
          SelectCoreResults();
          break;

        case "Create":
          ExportCoreResults();
          break;

        case "Set Image Path":
          SetCoreResultImagePath();
          break;

        case "Delete":
          DeleteCoreResults();
          break;

        case "Reset":
          ResetCoreResults();
          break;

        case "Import Existing":
          ImportExistingResults();
          break;
      }
    }

    private void dgvCoreResults_SelectionChanged(object sender, EventArgs e)
    {
      if (this.dgvCoreResults.CurrentRow != null)
      {
        // display core layers
        int IDCoreResult = Convert.ToInt32(this.dgvCoreResults.CurrentRow.Cells["IDCoreResult"].Value);
        DataRow[] layers = this.dtCoreLayers.Select("IDCoreResult = " + IDCoreResult.ToString(), "LayerOrdinalPosition");

        this.lvLayers.Items.Clear();
        foreach (DataRow layer in layers)
        {
          ListViewItem item = new ListViewItem(Convert.ToString(layer["LayerType"]));
          item.SubItems.Add(Convert.ToString(layer["Thickness"]));
          item.SubItems.Add(Convert.ToString(layer["LayerCharacteristic"]));
          this.lvLayers.Items.Add(item);
        }
      }
    }

    private void ImportCoreResults()
    {
      this.fdOpenFile.Multiselect = false;
      this.fdOpenFile.DefaultExt = "xls";
      this.fdOpenFile.Filter = "MS Excel Files | *.xls";

      if (this.fdOpenFile.ShowDialog(this) == DialogResult.OK)
      {
        object[] paras = new object[3];
        paras[0] = this.fdOpenFile.FileName;

        GPRSettings gs = new GPRSettings(DisplayOption.CoreUnit);
        gs.SetChainageUnit(DisplayOption.CoreUnit, this.coreUnits.ChainageUnit);
        gs.SetThicknessUnit(DisplayOption.CoreUnit, this.coreUnits.ThicknessUnit);

        if (gs.ShowDialog(this) == DialogResult.OK)
        {
          // customer chainage unit
          this.coreUnits.ChainageUnit = gs.GetChainageUnit(DisplayOption.CoreUnit);
          this.coreUnits.ThicknessUnit = gs.GetThicknessUnit(DisplayOption.CoreUnit);

          paras[1] = gs.GetConversionFactor(this.coreUnits.ChainageUnit);
          paras[2] = gs.GetConversionFactor(this.coreUnits.ThicknessUnit);

          this.EnableButtons(false);
          this.ResetCoreResults();

          this.sstsCoreResultStatus.Text = "Importing core results ...";
          this.ssCoreResult.Update();

          logGPRTool("Import Core Results", "", this.fdOpenFile.FileName);

          this.sstsCoreResultProgressBar.Visible = true;
          this.taskType = TaskType.CORERES_IMPORT;
          this.bgWorker.RunWorkerAsync(paras);
        }
      }
    }

    private void ImportCoreResults(string filename, double chainageConversion, double thickConversion)
    {
      try
      {
        string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties=Excel 5.0";

        using (OleDbConnection conn = new OleDbConnection(connString))
        {
          conn.Open();

          DataTable dtCores = new DataTable();
          DataTable dtLayers = new DataTable();
          DataTable dtImages = new DataTable();

          // core data
          string strQuery = "SELECT DISTINCT [CoreReqID], [CSLM] as Chainage, [Bore Number] as BoreNumber, [Date Cored] as CoreDate, " +
                     "[Recored?] as Recored, [Lane Direction] as LaneDirection, " +
                     "[GPS Latitude] as Latitude, [GPS Longitude] as Longitude, LatitudeM, LongitudeM, LatitudeS, LongitudeS, " +
                     "[Surface Type] as SurfaceType, [Stripping or Separation in Asphalt] as StrippingOrSeparationInAsphalt, " +
                     "[Honeycomb or \"D\" Cracking in PCC] as CrackingInPCC, [Reinforching Fabric Present] as RFPresent, [Other Notes] as Notes " +
                     "FROM [CoreData$] where [GPS Latitude] <> 0";

          using (OleDbCommand command = new OleDbCommand(strQuery, conn))
          {
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
              adapter.Fill(dtCores);
            }
          }

          // core layer
          strQuery = "SELECT [CoreReqID], [tblCoreLayers_ID] as CoreLayerID, [idKeyCodeLayerType] as CoreLayerType, " +
                 "[idKeyCodeLayerCharacteristic] as LayerCharacteristic, [Thickness], [Notes], [DeteriorationNoted] " +
                 "FROM [CoreData$] ORDER BY [CoreReqID]";
          using (OleDbCommand command = new OleDbCommand(strQuery, conn))
          {
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
              adapter.Fill(dtLayers);
            }
          }

          // core images
          try
          {
            strQuery = "SELECT [CoreReqID], [VicinityImageName], [CoreImageName], [HoleImageName] FROM [CoreImages$]";
            using (OleDbCommand command = new OleDbCommand(strQuery, conn))
            {
              using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
              {
                adapter.Fill(dtImages);
              }
            }
          }
          catch
          {
            //do nothing, no images
          }

          ImportCoreResults(dtCores, dtLayers, dtImages, chainageConversion, thickConversion);
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void ImportCoreResults(DataTable dtcores, DataTable dtCoreLayers, DataTable dtCoreImages,
                    double chainageConversion, double thickConversion)
    {
      try
      {
        Dictionary<string, object> paras = new Dictionary<string, object>();
        string sqlCore = "exec gpr.usp_UpdateCoreResult @CoreReqID, @Chainage, @BoreNumber, @CoreDate, @Recored, @LaneDirection, " +
                 "@Latitude, @Longitude, @LatitudeM, @LongitudeM, @LatitudeS, @LongitudeS, @SurfaceType, " +
                 "@Stripping, @Cracking, @RFPresent, @Notes, @DistanceStamp, @DistanceToVehicle";

        string sqlLayer = "exec gpr.usp_InsertCoreLayer @IDCoreResult, @LayerOrdinal, @LayerType, @LayerCharacteristic, " +
                  "@Thickness, @Notes, @DeteriorationNoted";

        string sqlImage = "exec gpr.usp_InsertCoreImage @IDCoreResult, @ImageName, @ImageType";

        string coreReqID, aranFileName;
        int IDCoreResult;
        double chainage, distanceStamp, latitude, longitude, distanceToVehicle;
        object result;
        string message = string.Empty;
        bool imagesPresent = true;
        string temp = string.Empty;

        foreach (DataRow row in dtcores.Rows)
        {
          coreReqID = Convert.ToString(row["CoreReqID"]);
          chainage = Convert.ToDouble(row["Chainage"]) / chainageConversion;
          latitude = Convert.ToDouble(row["Latitude"]) * Math.PI / 180;
          longitude = Convert.ToDouble(row["Longitude"]) * Math.PI / 180;
          distanceStamp = CalculateDistanceStamp(latitude, longitude, coreReqID, out distanceToVehicle);

          paras.Clear();
          paras.Add("CoreReqID", coreReqID);
          paras.Add("Chainage", chainage);
          paras.Add("BoreNumber", Convert.ToInt32(row["BoreNumber"]));
          paras.Add("CoreDate", Convert.ToDateTime(row["CoreDate"]));

          temp = Convert.ToString(row["Recored"]);
          if (!string.IsNullOrEmpty(temp) && (temp.ToLower() == "true" || temp.ToLower() == "yes"))
          {
            paras.Add("Recored", true);
          }
          else
          {
            paras.Add("Recored", false);
          }

          paras.Add("LaneDirection", Convert.ToString(row["LaneDirection"]));
          paras.Add("Latitude", latitude);
          paras.Add("Longitude", longitude);

          if (row.IsNull("LatitudeM") || row["LatitudeM"] == DBNull.Value)
          {
            paras.Add("LatitudeM", 0);
          }
          else
          {
            paras.Add("LatitudeM", Convert.ToDouble(row["LatitudeM"]) * Math.PI / 180);
          }

          if (row.IsNull("LongitudeM") || row["LongitudeM"] == DBNull.Value)
          {
            paras.Add("LongitudeM", 0);
          }
          else
          {
            paras.Add("LongitudeM", Convert.ToDouble(row["LongitudeM"]) * Math.PI / 180);
          }

          if (row.IsNull("LatitudeS") || row["LatitudeS"] == DBNull.Value)
          {
            paras.Add("LatitudeS", 0);
          }
          else
          {
            paras.Add("LatitudeS", Convert.ToDouble(row["LatitudeS"]) * Math.PI / 180);
          }

          if (row.IsNull("LongitudeS") || row["LongitudeS"] == DBNull.Value)
          {
            paras.Add("LongitudeS", 0);
          }
          else
          {
            paras.Add("LongitudeS", Convert.ToDouble(row["LongitudeS"]) * Math.PI / 180);
          }

          paras.Add("SurfaceType", Convert.ToString(row["SurfaceType"]));
          paras.Add("Stripping", Convert.ToString(row["StrippingOrSeparationInAsphalt"]));
          paras.Add("Cracking", Convert.ToString(row["CrackingInPCC"]));

          temp = Convert.ToString(row["RFPresent"]);
          if (!string.IsNullOrEmpty(temp) && (temp.ToLower() == "true" || temp.ToLower() == "yes"))
          {
            paras.Add("RFPresent", true);
          }
          else
          {
            paras.Add("RFPresent", false);
          }

          paras.Add("Notes", Convert.ToString(row["Notes"]));

          if (!double.IsNaN(distanceStamp))
            paras.Add("DistanceStamp", distanceStamp);
          else
            paras.Add("DistanceStamp", -1);

          if (double.IsNaN(distanceToVehicle))
          {
            distanceToVehicle = 0;
          }

          paras.Add("DistanceToVehicle", distanceToVehicle);

          result = this.dataMapper.DataProvider.ExecuteScalar(sqlCore, paras);

          if (result != null && result != DBNull.Value)
          {
            IDCoreResult = Convert.ToInt32(result);

            aranFileName = Convert.ToString(this.dataMapper.DataProvider.ExecuteScalar(
                              "SELECT gpr.fn_GetCoreARANFileName(" + IDCoreResult.ToString() + ")", null));

            if (!this.CoreResCollections.Contains(aranFileName))
            {
              this.CoreResCollections.Add(aranFileName);
            }

            // core layer
            this.DataMapper.DataProvider.ExecuteNonQuery("DELETE FROM gpr.CoreLayers WHERE IDCoreResult = " + IDCoreResult.ToString(), null);

            DataRow[] layerRows = dtCoreLayers.Select("CoreReqID = '" + coreReqID + "'", "CoreLayerID");

            for (int i = 0; i < layerRows.Length; i++)
            {
              paras.Clear();
              paras.Add("IDCoreResult", IDCoreResult);
              paras.Add("LayerOrdinal", i);
              paras.Add("LayerType", Convert.ToString(layerRows[i]["CoreLayerType"]));
              paras.Add("LayerCharacteristic", Convert.ToString(layerRows[i]["LayerCharacteristic"]));

              if (layerRows[i].IsNull("Thickness") || layerRows[i]["Thickness"] == DBNull.Value)
              {
                paras.Add("Thickness", 0);
              }
              else
              {
                paras.Add("Thickness", Convert.ToDouble(layerRows[i]["Thickness"]) / thickConversion);
              }

              paras.Add("Notes", Convert.ToString(layerRows[i]["Notes"]));

              temp = Convert.ToString(layerRows[i]["DeteriorationNoted"]);
              if (!string.IsNullOrEmpty(temp) && (temp.ToLower() == "true" || temp.ToLower() == "yes"))
              {
                paras.Add("DeteriorationNoted", true);
              }
              else
              {
                paras.Add("DeteriorationNoted", false);
              }

              //paras.Add("DeteriorationNoted", Convert.ToBoolean(layerRows[i]["DeteriorationNoted"]));

              this.dataMapper.DataProvider.ExecuteNonQuery(sqlLayer, paras);
            }

            // core images
            if (dtCoreImages.Rows.Count > 0)
            {
              this.dataMapper.DataProvider.ExecuteNonQuery("DELETE FROM gpr.CoreImages WHERE IDCoreResult = " + IDCoreResult.ToString(), null);

              DataRow[] imageRows = dtCoreImages.Select("CoreReqID = '" + coreReqID + "'");

              foreach (DataRow row1 in imageRows)
              {
                if (!string.IsNullOrEmpty(Convert.ToString(row1["VicinityImageName"])))
                {
                  paras.Clear();
                  paras.Add("IDCoreResult", IDCoreResult);
                  paras.Add("ImageName", Convert.ToString(row1["VicinityImageName"]));
                  paras.Add("ImageType", "Vicinity");
                  this.dataMapper.DataProvider.ExecuteNonQuery(sqlImage, paras);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(row1["CoreImageName"])))
                {
                  paras.Clear();
                  paras.Add("IDCoreResult", IDCoreResult);
                  paras.Add("ImageName", Convert.ToString(row1["CoreImageName"]));
                  paras.Add("ImageType", "Core");
                  this.dataMapper.DataProvider.ExecuteNonQuery(sqlImage, paras);
                }

                if (!string.IsNullOrEmpty(Convert.ToString(row1["HoleImageName"])))
                {
                  paras.Clear();
                  paras.Add("IDCoreResult", IDCoreResult);
                  paras.Add("ImageName", Convert.ToString(row1["HoleImageName"]));
                  paras.Add("ImageType", "Hole");
                  this.dataMapper.DataProvider.ExecuteNonQuery(sqlImage, paras);
                }
              }
            }
            else
            {
              imagesPresent = false;
            }
          }
          else
          {
            if (string.IsNullOrEmpty(message))
            {
              message = "Following CoreReqIDs are not found:\n" + coreReqID;
            }
            else
            {
              message = message + ", " + coreReqID;
            }
          }
        }

        if (!imagesPresent)
        {
          message += "\n\n Images sheet not found for Core Results";
        }

        if (!string.IsNullOrEmpty(message))
        {
          MessageBox.Show(message, "Warning");
        }

        this.LoadCoreResults();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }


    private void ImportExistingResults()
    {
      this.fdOpenFile.Multiselect = false;
      this.fdOpenFile.DefaultExt = "xls";
      this.fdOpenFile.Filter = "MS Excel Files | *.xls";

      string filename = "";
      DataTable dtCores = new DataTable();

      if (this.fdOpenFile.ShowDialog(this) == DialogResult.OK)
      {
        filename = this.fdOpenFile.FileName;

        try
        {
          string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename + ";Extended Properties=Excel 5.0";

          using (OleDbConnection conn = new OleDbConnection(connString))
          {
            conn.Open();



            // core data
            string strQuery = "SELECT [layerno], [control_section], [CoreReqID], [Date Cored] as CoreDate, " +
                       "[CSLM] as Chainage, [Lane Direction] as LaneDirection, " +
                       "[GPS Latitude] as Latitude, [GPS Longitude] as Longitude, [idKeyCodeLayerType] as type, [Thickness], [idKeyCodeLayerCharacteristic] as layerChar " +
                       "FROM [CoreData$] ORDER BY [control_section], [CoreReqID], [layerno]";

            using (OleDbCommand command = new OleDbCommand(strQuery, conn))
            {
              using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
              {
                adapter.Fill(dtCores);
              }
            }

            conn.Close();
          }
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.StackTrace, ex.Message);
        }

        string control_section, coreReqID = "", laneDirection, type, layerChar;
        int layerNo;
        double chainage, latitude, longitude, thickness, distanceStamp, distanceToVehicle;
        object result;
        string message = string.Empty;
        int coreCount = 1;
        int i = 1;

        Dictionary<string, object> paras = new Dictionary<string, object>();

        //delete all existing cores before importing, covers any case where data possibly changes (re-import)
        paras.Add("Note", "Existing Core");
        string sqlDelete = "delete from gpr.corelayers where Notes = @Note";
        this.dataMapper.DataProvider.ExecuteNonQuery(sqlDelete, paras);

        sqlDelete = "delete from gpr.coreresults where Notes = @Note";
        this.dataMapper.DataProvider.ExecuteNonQuery(sqlDelete, paras);

        sqlDelete = "delete from gpr.corerequests where [Description] = @Note";
        this.dataMapper.DataProvider.ExecuteNonQuery(sqlDelete, paras);

        int zeroInt = 0;

        foreach (DataRow row in dtCores.Rows)
        {
          //core request
          layerNo = Convert.ToInt32(row["layerno"]);

          //only import requests where the layer number is 1 (one request/result per core, not per layer)
          if (layerNo == 1)
          {
            i = 1;
            paras.Clear();

            try
            {
              using (SqlConnection conn = new SqlConnection(this.connStr))
              {
                conn.Open();

                using (SqlCommand command = new SqlCommand())
                {
                  command.CommandTimeout = 15000;
                  command.Connection = conn;

                  string sqlImage = "gpr.usp_CreateExistingRequest";

                  layerNo = Convert.ToInt32(row["layerno"]);
                  control_section = Convert.ToString(row["control_section"]);

                  coreReqID = Convert.ToString(row["CoreReqID"]);
                  chainage = Convert.ToDouble(row["Chainage"]);
                  laneDirection = Convert.ToString(row["LaneDirection"]);

                  latitude = Convert.ToDouble(row["Latitude"]) * Math.PI / 180; ;
                  longitude = Convert.ToDouble(row["Longitude"]) * Math.PI / 180;

                  thickness = Convert.ToDouble(row["Thickness"]);

                  type = Convert.ToString(row["type"]);
                  layerChar = Convert.ToString(row["layerChar"]);

                  string[] locators = control_section.Split(':');

                  while (locators[1].Length < 3)
                  {
                    locators[1] = '0' + locators[1];
                  }

                  locators[2] = locators[2].Substring(0, 1);

                  command.CommandType = CommandType.StoredProcedure;
                  command.Parameters.Add(new SqlParameter("@Locator1", locators[0].ToString()));
                  command.Parameters.Add(new SqlParameter("@Locator2", locators[1].ToString()));
                  command.Parameters.Add(new SqlParameter("@Locator3", locators[2].ToString()));
                  command.Parameters.Add(new SqlParameter("@Locator4", locators[3].ToString()));
                  command.Parameters.Add(new SqlParameter("@PostMile", locators[4].ToString()));
                  command.Parameters.Add(new SqlParameter("@layerNo", coreCount));
                  command.Parameters.Add(new SqlParameter("@latitude", latitude));
                  command.Parameters.Add(new SqlParameter("@longitude", longitude));
                  command.Parameters.Add(new SqlParameter("@CoreReq_id", coreReqID));

                  command.CommandText = sqlImage;
                  command.ExecuteNonQuery();

                  //import core result
                  paras.Clear();
                  command.Parameters.Clear();

                  command.Parameters.Add(new SqlParameter("@CoreReqId", coreReqID));
                  command.Parameters.Add(new SqlParameter("@Chainage", zeroInt));
                  command.Parameters.Add(new SqlParameter("@BoreNumber", zeroInt));

                  if (!string.IsNullOrEmpty(row["CoreDate"].ToString()))
                  {
                    //paras.Add("CoreDate", Convert.ToDateTime(row["CoreDate"]));
                    command.Parameters.Add(new SqlParameter("@CoreDate", Convert.ToDateTime(row["CoreDate"])));
                  }
                  else
                  {
                    command.Parameters.Add(new SqlParameter("@CoreDate", Convert.ToDateTime("01/01/2001")));
                    //paras.Add("CoreDate", Convert.ToDateTime("01/01/2001"));
                  }

                  command.Parameters.Add(new SqlParameter("@Recored", zeroInt));
                  command.Parameters.Add(new SqlParameter("@LaneDirection", laneDirection));
                  command.Parameters.Add(new SqlParameter("@Latitude", latitude));
                  command.Parameters.Add(new SqlParameter("@Longitude", longitude));
                  command.Parameters.Add(new SqlParameter("@LatitudeM", zeroInt));
                  command.Parameters.Add(new SqlParameter("@LongitudeM", zeroInt));
                  command.Parameters.Add(new SqlParameter("@LatitudeS", zeroInt));
                  command.Parameters.Add(new SqlParameter("@LongitudeS", zeroInt));
                  command.Parameters.Add(new SqlParameter("@SurfaceType", ""));
                  command.Parameters.Add(new SqlParameter("@Stripping", "N/A"));
                  command.Parameters.Add(new SqlParameter("@Cracking", "N/A"));
                  command.Parameters.Add(new SqlParameter("@RFPresent", zeroInt));
                  command.Parameters.Add(new SqlParameter("@Notes", "Existing Core"));

                  distanceStamp = CalculateDistanceStamp(latitude, longitude, coreReqID, out distanceToVehicle);

                  if (!double.IsNaN(distanceStamp))
                    command.Parameters.Add(new SqlParameter("@DistanceStamp", distanceStamp));
                  else
                    command.Parameters.Add(new SqlParameter("@DistanceStamp", -1));


                  command.Parameters.Add(new SqlParameter("@DistanceToVehicle", distanceToVehicle));

                  string sqlCore = "gpr.usp_UpdateCoreResult";

                  command.CommandText = sqlCore;
                  command.ExecuteNonQuery();
                }
              }
            }
            catch (Exception ex)
            {
              MessageBox.Show(ex.StackTrace, ex.Message);
            }

          }


          //Core layers
          string sqlLayer = "gpr.usp_InsertCoreLayer";

          paras.Clear();

          try
          {
            using (SqlConnection conn = new SqlConnection(this.connStr))
            {
              conn.Open();

              using (SqlCommand command = new SqlCommand())
              {
                command.CommandTimeout = 15000;
                command.Connection = conn;
                command.CommandType = CommandType.Text;

                // get IDCoreResult
                string sqlComm = "SELECT IDCoreResult FROM gpr.CoreResults " +
                                 "WHERE IDCoreRequest IN (SELECT IDCoreRequest FROM gpr.CoreRequests WHERE CoreReqID = '" + coreReqID + "')";


                command.CommandText = sqlComm;
                int IDCoreResult = Convert.ToInt32(command.ExecuteScalar());

                if (IDCoreResult > 0)
                {
                  command.Parameters.Add(new SqlParameter("@IDCoreResult", IDCoreResult));
                  command.Parameters.Add(new SqlParameter("@LayerOrdinal", i));
                  command.Parameters.Add(new SqlParameter("@LayerType", Convert.ToString(row["type"])));
                  command.Parameters.Add(new SqlParameter("@LayerCharacteristic", Convert.ToString(row["layerChar"])));

                  if (row.IsNull("Thickness") || row["Thickness"] == DBNull.Value)
                  {
                    //paras.Add("Thickness", 0);
                    command.Parameters.Add(new SqlParameter("@Thickness", zeroInt));

                  }
                  else
                  {
                    //TODO: is the thickness conversion needed?
                    command.Parameters.Add(new SqlParameter("@Thickness", Convert.ToDouble(row["Thickness"]) / 1000));
                  }

                  command.Parameters.Add(new SqlParameter("@Notes", "Existing Core"));
                  command.Parameters.Add(new SqlParameter("@DeteriorationNoted", false));

                  command.CommandType = CommandType.StoredProcedure;

                  command.CommandText = sqlLayer;
                  command.ExecuteNonQuery();
                }
              }
            }
          }
          catch (Exception ex)
          {
            MessageBox.Show(ex.StackTrace, ex.Message);
          }

          i++;
        }

      }

    }

    private double CalculateDistanceStamp(double latitude, double longitude, string coreReqID, out double distance)
    {
      //double distanceStamp = double.NaN;
      //double cDistanceStamp1 = double.NaN;
      //double distanceStamp1, distanceStamp2, sDistance, theta1;
      //Length distc1, distc2, dist12;
      //GeographicCoordinate vGPS1 = new GeographicCoordinate();
      //GeographicCoordinate vGPS2 = new GeographicCoordinate();
      //GeographicCoordinate coreGPS = new GeographicCoordinate();
      //GeographicCoordinate cGPS1 = new GeographicCoordinate();
      //GeographicCoordinate cGPS2 = new GeographicCoordinate();

      //coreGPS.Latitude.Radians = latitude;
      //coreGPS.Longitude.Radians = longitude;
      //sDistance = 9999999;
      //distance = -1;

      string sqlComm = "SELECT v.DistanceStamp, Latitude, Longitude FROM VehiclePositions v " +
               "INNER JOIN gpr.CoreRequests c ON v.IDSession = c.IDSession " +
               "WHERE c.CoreReqID = '" + coreReqID + "' AND v.DistanceStamp is not null " +
               "ORDER BY v.DistanceStamp";

      DataTable dtGPS = this.dataMapper.DataProvider.ExecuteDataTable(sqlComm, null);

      return this.CalculateDistanceStamp(latitude, longitude, dtGPS, out distance);

      //int count = dtGPS.Rows.Count;

      //if (count > 0)
      //{
      //    // find two closest points
      //    vGPS1.Latitude.Radians = Convert.ToDouble(dtGPS.Rows[0]["Latitude"]);
      //    vGPS1.Longitude.Radians = Convert.ToDouble(dtGPS.Rows[0]["Longitude"]);
      //    distanceStamp1 = Convert.ToDouble(dtGPS.Rows[0]["DistanceStamp"]);

      //    distc1 = (ProjectedCoordinate)coreGPS - (ProjectedCoordinate)vGPS1;

      //    for (int i = 1; i < count; i++)
      //    {
      //        vGPS2.Latitude.Radians = Convert.ToDouble(dtGPS.Rows[i]["Latitude"]);
      //        vGPS2.Longitude.Radians = Convert.ToDouble(dtGPS.Rows[i]["Longitude"]);
      //        distanceStamp2 = Convert.ToDouble(dtGPS.Rows[i]["DistanceStamp"]);

      //        if (distc1.Metres < sDistance)
      //        {
      //            cGPS1.Latitude.Radians = vGPS1.Latitude.Radians;
      //            cGPS1.Longitude.Radians = vGPS1.Longitude.Radians;
      //            cGPS2.Latitude.Radians = vGPS2.Latitude.Radians;
      //            cGPS2.Longitude.Radians = vGPS2.Longitude.Radians;
      //            cDistanceStamp1 = distanceStamp1;

      //            sDistance = distc1.Metres;
      //        }

      //        distanceStamp1 = distanceStamp2;
      //        distc1 = (ProjectedCoordinate)coreGPS - (ProjectedCoordinate)vGPS2;
      //        vGPS1.Latitude.Radians = vGPS2.Latitude.Radians;
      //        vGPS1.Longitude.Radians = vGPS2.Longitude.Radians;
      //    }


      //    // distance stamp
      //    if (!double.IsNaN(cDistanceStamp1))
      //    {
      //        distc1 = (ProjectedCoordinate)coreGPS - (ProjectedCoordinate)cGPS1;
      //        distc2 = (ProjectedCoordinate)coreGPS - (ProjectedCoordinate)cGPS2;
      //        dist12 = (ProjectedCoordinate)cGPS1 - (ProjectedCoordinate)cGPS2;

      //        theta1 = (distc1.Metres * distc1.Metres + dist12.Metres * dist12.Metres - distc2.Metres * distc2.Metres) / (
      //                          2 * distc1.Metres * dist12.Metres);

      //        distance = Math.Sqrt(distc1.Metres * distc1.Metres - distc1.Metres * theta1 * distc1.Metres * theta1);

      //        distanceStamp = cDistanceStamp1 + distc1.Metres * theta1;
      //    }
      //}

      //return distanceStamp;
    }

    private double CalculateDistanceStamp(double latitude, double longitude, DataTable dtGPS, out double distance)
    {
      double distanceStamp = double.NaN;
      double cDistanceStamp1 = double.NaN;
      double distanceStamp1, distanceStamp2, sDistance, theta1;
      Length distc1, distc2, dist12;
      GeographicCoordinate vGPS1 = new GeographicCoordinate();
      GeographicCoordinate vGPS2 = new GeographicCoordinate();
      GeographicCoordinate coreGPS = new GeographicCoordinate();
      GeographicCoordinate cGPS1 = new GeographicCoordinate();
      GeographicCoordinate cGPS2 = new GeographicCoordinate();

      coreGPS.Latitude.Radians = latitude;
      coreGPS.Longitude.Radians = longitude;
      sDistance = 9999999;
      distance = -1;

      int count = dtGPS.Rows.Count;

      if (count > 0)
      {
        // find two closest points
        vGPS1.Latitude.Radians = Convert.ToDouble(dtGPS.Rows[0]["Latitude"]);
        vGPS1.Longitude.Radians = Convert.ToDouble(dtGPS.Rows[0]["Longitude"]);
        distanceStamp1 = Convert.ToDouble(dtGPS.Rows[0]["DistanceStamp"]);

        distc1 = (ProjectedCoordinate)coreGPS - (ProjectedCoordinate)vGPS1;

        for (int i = 1; i < count; i++)
        {
          vGPS2.Latitude.Radians = Convert.ToDouble(dtGPS.Rows[i]["Latitude"]);
          vGPS2.Longitude.Radians = Convert.ToDouble(dtGPS.Rows[i]["Longitude"]);
          distanceStamp2 = Convert.ToDouble(dtGPS.Rows[i]["DistanceStamp"]);

          if (distc1.Metres < sDistance)
          {
            cGPS1.Latitude.Radians = vGPS1.Latitude.Radians;
            cGPS1.Longitude.Radians = vGPS1.Longitude.Radians;
            cGPS2.Latitude.Radians = vGPS2.Latitude.Radians;
            cGPS2.Longitude.Radians = vGPS2.Longitude.Radians;
            cDistanceStamp1 = distanceStamp1;

            sDistance = distc1.Metres;
          }

          distanceStamp1 = distanceStamp2;
          distc1 = (ProjectedCoordinate)coreGPS - (ProjectedCoordinate)vGPS2;
          vGPS1.Latitude.Radians = vGPS2.Latitude.Radians;
          vGPS1.Longitude.Radians = vGPS2.Longitude.Radians;
        }


        // distance stamp
        if (!double.IsNaN(cDistanceStamp1))
        {
          distc1 = (ProjectedCoordinate)coreGPS - (ProjectedCoordinate)cGPS1;
          distc2 = (ProjectedCoordinate)coreGPS - (ProjectedCoordinate)cGPS2;
          dist12 = (ProjectedCoordinate)cGPS1 - (ProjectedCoordinate)cGPS2;

          theta1 = (distc1.Metres * distc1.Metres + dist12.Metres * dist12.Metres - distc2.Metres * distc2.Metres) / (
                    2 * distc1.Metres * dist12.Metres);

          distance = Math.Sqrt(distc1.Metres * distc1.Metres - distc1.Metres * theta1 * distc1.Metres * theta1);

          distanceStamp = cDistanceStamp1 + distc1.Metres * theta1;
        }
      }

      return distanceStamp;
    }

    private void SelectCoreResults()
    {
      try
      {
        SelectCollections sc = new SelectCollections(this.dataMapper);

        if (this.CoreResCollections.Count == 0)
        {

          if (sc.ShowDialog(this) == DialogResult.OK)
          {
            this.sstsCoreResultStatus.Text = "Loading collections ...";
            this.ssCoreResult.Update();

            this.dtCoreResCollections = sc.GroupPartitionExplorer.SelectedCollections;
            this.CoreResCollections = this.GetCollectionList(this.dtCoreResCollections);
          }
        }

        if (this.CoreResCollections.Count > 0)
        {
          this.EnableButtons(false);
          this.sstsCoreResultStatus.Text = "Loading core results ...";
          this.ssCoreResult.Update();

          this.dtCoreResults.Clear();
          this.dtCoreLayers.Clear();
          this.dgvCoreResults.DataSource = null;
          this.pnlLayers.Visible = false;

          logGPRTool("Select Core Results", sc.GroupPartitionExplorer.GroupFullpath[0].ToString(), "");

          this.sstsCoreResultProgressBar.Visible = true;
          this.taskType = TaskType.CORERES_SELECT;
          this.bgWorker.RunWorkerAsync();
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void LoadCoreResults()
    {
      if (this.CoreResCollections.Count > 0)
      {
        try
        {
          this.dtCoreResults.Clear();
          this.dtCoreLayers.Clear();
          this.dtCoreImages.Clear();

          string sqlComm, sqlImages;
          Dictionary<string, object> paras = new Dictionary<string, object>();

          sqlComm = "exec gpr.usp_GetCoreResult @ARANFileName";

          foreach (string cls in this.CoreResCollections)
          {
            paras.Clear();
            paras.Add("ARANFileName", cls);
            this.dataMapper.DataProvider.FillDataTable(this.dtCoreResults, sqlComm, paras);
          }

          int IDCoreResult;
          sqlComm = "exec gpr.usp_GetCoreLayers @IDCoreResult";
          sqlImages = "exec gpr.usp_GetCoreImages @IDCoreResult";

          foreach (DataRow row in this.dtCoreResults.Rows)
          {
            IDCoreResult = Convert.ToInt32(row["IDCoreResult"]);

            paras.Clear();
            paras.Add("IDCoreResult", IDCoreResult);
            this.dataMapper.DataProvider.FillDataTable(this.dtCoreLayers, sqlComm, paras);

            this.dataMapper.DataProvider.FillDataTable(this.dtCoreImages, sqlImages, paras);
          }
        }
        catch (Exception ex)
        {
          throw ex;
        }
      }
    }

    private void DisplayCoreResults()
    {
      this.dgvCoreResults.DataSource = this.dtCoreResults;

      foreach (DataGridViewColumn col in this.dgvCoreResults.Columns)
      {
        if (col.DataPropertyName == "IDCoreResult" || col.DataPropertyName == "MChain" || col.DataPropertyName == "ARANFileName")
        {
          col.Visible = false;
        }
      }

      if (this.dgvCoreResults.Rows.Count > 0 && this.dgvCoreResults.CurrentRow == null)
      {
        this.dgvCoreResults.CurrentCell = this.dgvCoreResults.Rows[0].Cells[1];
      }

      if (this.dgvCoreResults.CurrentRow != null)
      {
        this.pnlLayers.Visible = true;

        // display core layers
        int IDCoreResult = Convert.ToInt32(this.dgvCoreResults.CurrentRow.Cells["IDCoreResult"].Value);
        DataRow[] layers = this.dtCoreLayers.Select("IDCoreResult = " + IDCoreResult.ToString(), "LayerOrdinalPosition");

        this.lvLayers.Items.Clear();
        foreach (DataRow layer in layers)
        {
          ListViewItem item = new ListViewItem(Convert.ToString(layer["LayerType"]));
          item.SubItems.Add(Convert.ToString(layer["Thickness"]));
          item.SubItems.Add(Convert.ToString(layer["LayerCharacteristic"]));
          this.lvLayers.Items.Add(item);
        }
      }

      this.dtCoreResCollections = this.GetCollections(this.CoreResCollections);
      this.ucCoreResCollections.DataSource = this.dtCoreResCollections;
    }

    private void ExportCoreResults()
    {
      if (this.CoreResCollections.Count > 0 && this.dtCoreResults.Rows.Count > 0)
      {
        if (this.fbSelectFolder.ShowDialog(this) == DialogResult.OK)
        {
          object[] paras = new object[2];
          paras[0] = this.fbSelectFolder.SelectedPath;

          GPRSettings gs = new GPRSettings(DisplayOption.ThicknessUnit);
          gs.SetThicknessUnit(DisplayOption.ThicknessUnit, this.coreUnits.ThicknessUnit);

          if (gs.ShowDialog(this) == DialogResult.OK)
          {
            this.coreUnits.ThicknessUnit = gs.GetThicknessUnit(DisplayOption.ThicknessUnit);
            paras[1] = gs.GetConversionFactor(this.coreUnits.ThicknessUnit);

            this.EnableButtons(false);
            this.sstsCoreResultStatus.Text = "Exporting core results...";
            this.ssCoreResult.Update();

            this.taskType = TaskType.CORERES_EXPORT;
            this.sstsCoreResultProgressBar.Visible = true;
            this.bgWorker.RunWorkerAsync(paras);
          }
        }
      }
      else
      {
        MessageBox.Show("No core results. Please select collections which have core results first.", "Warning");
      }
    }

    private void ExportCoreResults(string path, double thicknessConversion)
    {
      try
      {
        foreach (string aranfile in this.CoreResCollections)
        {
          DataRow[] cores = this.dtCoreResults.Select("ARANFileName = '" + aranfile + "'", "CoreReqID");

          if (cores.Length > 0)
          {
            CreateCoreFile(path, aranfile, cores, thicknessConversion);
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void CreateCoreFile(string path, string aranfileName, DataRow[] cores, double thicknessConversion)
    {
      StreamWriter writer = null;
      string fileFullName;

      if (!path.EndsWith("\\"))
      {
        path = path + "\\";
      }

      fileFullName = path + aranfileName + "001_01.COR";

      try
      {
        if (File.Exists(fileFullName))
        {
          File.Delete(fileFullName);
        }

        // write to file
        writer = new StreamWriter(fileFullName);
        string format = this.GetDoubleStringFormat(3);
        string thickness;
        double depth;

        for (int j = 0; j < cores.Length; j++)
        {
          if (j > 0)
            writer.WriteLine();

          writer.WriteLine("CORE " + Convert.ToString(cores[j]["CoreReqID"]));
          writer.WriteLine(Convert.ToString(cores[j]["MChain"]));

          DataRow[] layers = this.dtCoreLayers.Select("IDCoreResult = " + Convert.ToString(cores[j]["IDCoreResult"]),
                                "LayerOrdinalPosition");

          for (int i = 1; i <= layers.Length; i++)
          {
            depth = Convert.ToDouble(layers[i - 1]["Thickness"]);

            if (depth == 0)
              thickness = "1";
            else
            {
              depth = depth * thicknessConversion;

              if (string.IsNullOrEmpty(format))
              {
                thickness = Convert.ToInt32(depth).ToString();
              }
              else
              {
                thickness = string.Format(format, depth);
              }
            }

            writer.WriteLine(i.ToString() + "," + thickness + "," + Convert.ToString(layers[i - 1]["LayerNumber"]) +
                     ",[" + Convert.ToString(layers[i - 1]["LayerCharacteristic"]) + "]");
          }
        }

        writer.Flush();
      }
      catch (Exception ex)
      {
        throw ex;
      }
      finally
      {
        if (writer != null)
          writer.Close();
      }
    }

    private void SetCoreResultImagePath()
    {
      try
      {
        if (this.CoreResCollections.Count == 0)
        {
          if (MessageBox.Show("Set up path for all the cores in the database?", "Set Image Path", MessageBoxButtons.YesNo) == DialogResult.Yes)
          {
            if (this.fbSelectFolder.ShowDialog(this) == DialogResult.OK)
            {
              this.EnableButtons(false);
              this.sstsCoreResultStatus.Text = "Setting up core image path ...";
              this.ssCoreResult.Update();

              object[] paras = new object[2];
              paras[0] = this.fbSelectFolder.SelectedPath;
              paras[1] = true;

              this.sstsCoreResultProgressBar.Visible = true;
              this.taskType = TaskType.CORERES_SET_PATH;
              this.bgWorker.RunWorkerAsync(paras);
            }
          }
        }
        else
        {
          if (MessageBox.Show("Set up path for selected collections?", "Set Image Path", MessageBoxButtons.YesNo) == DialogResult.Yes)
          {
            if (this.fbSelectFolder.ShowDialog(this) == DialogResult.OK)
            {
              this.EnableButtons(false);
              this.sstsCoreResultStatus.Text = "Setting up core image path ...";
              this.ssCoreResult.Update();

              object[] paras = new object[2];
              paras[0] = this.fbSelectFolder.SelectedPath;
              paras[1] = false;

              this.sstsCoreResultProgressBar.Visible = true;
              this.taskType = TaskType.CORERES_SET_PATH;
              this.bgWorker.RunWorkerAsync(paras);
            }
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
      finally
      {
        this.sstsCoreResultStatus.Text = "";
        this.ssCoreResult.Update();
      }
    }

    private void SetCoreResultImagePath(string path, bool all)
    {
      try
      {
        if (all)
        {
          string sqlComm = @"UPDATE gpr.CoreResults SET ImagePath = '" + path + "'";
          this.dataMapper.DataProvider.ExecuteNonQuery(sqlComm, null);
        }
        else
        {
          string sqlComm;
          string coreId;

          foreach (DataRow row in this.dtCoreResults.Rows)
          {
            coreId = Convert.ToString(row["IDCoreResult"]);
            sqlComm = @"UPDATE gpr.CoreResults SET ImagePath = '" + path + "' WHERE IDCoreResult = " + coreId;
            this.dataMapper.DataProvider.ExecuteNonQuery(sqlComm, null);
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private void DeleteCoreResults()
    {
      if (this.dgvCoreResults.SelectedRows.Count > 0)
      {
        if (MessageBox.Show("Delete selected core results?", "Delete Core Results", MessageBoxButtons.YesNo) == DialogResult.Yes)
        {
          this.EnableButtons(false);
          this.sstsCoreResultStatus.Text = "Deleting core results...";
          this.ssCoreResult.Update();

          string sqlComm = "exec gpr.usp_DeleteCoreResult @IDCoreResult";
          Dictionary<string, object> paras = new Dictionary<string, object>();
          string coreId;

          try
          {
            foreach (DataGridViewRow row in this.dgvCoreResults.SelectedRows)
            {
              coreId = Convert.ToString(row.Cells["IDCoreResult"].Value);

              paras.Clear();
              paras.Add("IDCoreResult", coreId);
              this.dataMapper.DataProvider.ExecuteNonQuery(sqlComm, paras);

              DataRow[] rows = this.dtCoreResults.Select("IDCoreResult = '" + coreId + "'");
              if (rows.Length > 0)
              {
                this.dtCoreResults.Rows.Remove(rows[0]);
              }
            }

            this.dtCoreResults.AcceptChanges();
          }
          catch (Exception ex)
          {
            MessageBox.Show(ex.StackTrace, ex.Message);
          }
          finally
          {
            this.dgvCoreResults.DataSource = this.dtCoreResults;
            this.sstsCoreResultCollections.Text = this.CoreResCollections.Count.ToString() + " collections " +
                               this.dtCoreResults.Rows.Count.ToString() + " cores";

            this.sstsCoreResultStatus.Text = "";
            EnableButtons(true);
          }
        }
      }
    }

    private void ResetCoreResults()
    {
      this.dtCoreResults.Clear();
      this.dtCoreResCollections.Clear();
      this.CoreResCollections.Clear();
      this.dtCoreImages.Clear();
      this.dtCoreLayers.Clear();

      this.ucCoreResCollections.DataSource = null;
      this.dgvCoreResults.DataSource = null;
      this.lvLayers.Items.Clear();
      this.sstsCoreResultCollections.Text = "";
      this.sstsCoreResultStatus.Text = "";

      this.pnlLayers.Visible = false;
    }

    private void PerformCoreResultTask(TaskType taskType, DoWorkEventArgs e)
    {
      object[] paras;
      string path;

      switch (taskType)
      {
        case TaskType.CORERES_IMPORT:
          paras = e.Argument as object[];
          string filename = paras[0] as string;
          double chainageConversion = Convert.ToDouble(paras[1]);
          double thickConversion = Convert.ToDouble(paras[2]);

          this.ImportCoreResults(filename, chainageConversion, thickConversion);
          break;

        case TaskType.CORERES_SELECT:
          this.LoadCoreResults();
          break;

        case TaskType.CORERES_EXPORT:
          paras = e.Argument as object[];
          path = paras[0] as string;
          double thicknessConversion = Convert.ToDouble(paras[1]);
          this.ExportCoreResults(path, thicknessConversion);
          break;

        case TaskType.CORERES_SET_PATH:
          paras = e.Argument as object[];
          path = paras[0] as string;
          bool all = Convert.ToBoolean(Convert.ToString(paras[1]));
          this.SetCoreResultImagePath(path, all);
          break;
      }
    }

    private void CompleteCoreResultTask(TaskType taskType, RunWorkerCompletedEventArgs e)
    {
      switch (taskType)
      {
        case TaskType.CORERES_IMPORT:
        case TaskType.CORERES_SELECT:
          this.sstsCoreResultStatus.Text = "";

          DisplayCoreResults();

          this.sstsCoreResultCollections.Text = this.CoreResCollections.Count == 0 ? string.Empty :
                              this.CoreResCollections.Count.ToString() + " collections " +
                              this.dtCoreResults.Rows.Count.ToString() + " cores";
          break;

        case TaskType.CORERES_EXPORT:
          if (!e.Cancelled && e.Error == null)
          {
            this.sstsCoreResultStatus.Text = "Core results have been exported.";
          }
          else
          {
            this.sstsCoreResultStatus.Text = "Export core result failed.";
          }

          break;

        case TaskType.CORERES_SET_PATH:
          if (!e.Cancelled && e.Error == null)
          {
            this.sstsCoreResultStatus.Text = "Core image path has been set.";
          }
          else
          {
            this.sstsCoreResultStatus.Text = "Set Image path failed.";
          }

          break;
      }

      this.sstsCoreResultProgressBar.Visible = false;
    }
    #endregion

    #region GPR Results

    private void CompleteGPRResultTask(TaskType taskType, RunWorkerCompletedEventArgs e)
    {
      this.sstsGPRStatus.Text = "";

      switch (taskType)
      {
        case TaskType.GPR_IMPORT:
          this.dgvGPRResults.DataSource = this.dtGPRResults;
          this.dtGPRCollections = this.GetCollections(this.GPRCollections);
          this.ucGPRCollections.DataSource = this.dtGPRCollections;

          //auto copy gpr import results to clipboard if there are errors
          if (GPRImportErrorText != "")
          {
            Clipboard.SetDataObject(GPRImportErrorText, true);
          }

          //show error form if there are errors in the datatable
          if (dtImportErrors.Rows.Count > 0)
          {

            GPRErrorForm errorForm = new GPRErrorForm("No match found in PCK for the following MAR2 entries", dtImportErrors);
            errorForm.Show();

          }
          foreach (DataGridViewColumn col in this.dgvGPRResults.Columns)
          {
            if (col.DataPropertyName == "IDGPRResult")
            {
              col.Visible = false;
              break;
            }
          }

          this.sstsGPRCollections.Text = this.GPRCollections.Count == 0 ? string.Empty :
                           this.GPRCollections.Count.ToString() + " collections " +
                           this.dtGPRResults.Count.ToString() + " rows";

          break;

        case TaskType.GPR_PROCESS:
          this.LoadProcessedGPRResults();

          this.ucGPRCollections.DataSource = this.dtGPRCollections;
          this.dgvGPRResults.DataSource = this.dtGPRProcessedResults;

          this.sstsGPRCollections.Text = this.GPRCollections.Count == 0 ? string.Empty :
                           this.GPRCollections.Count.ToString() + " collections " +
                           this.dtGPRProcessedResults.Rows.Count.ToString() + " rows";

          break;

        case TaskType.GPR_REPORT:
        case TaskType.GPR_PCK:
        case TaskType.GPR_WARNING:
          this.ucGPRCollections.DataSource = this.dtGPRCollections;
          this.dgvGPRResults.DataSource = this.dtGPRReport;
          this.dgvGPRResults.Update();

          this.sstsGPRCollections.Text = this.GPRCollections.Count == 0 ? string.Empty :
                           this.GPRCollections.Count.ToString() + " collections " +
                           this.dtGPRReport.Rows.Count.ToString() + " rows";

          if (this.taskType == TaskType.GPR_REPORT)
          {
            switch (this.reportType)
            {
              case ReportType.GPR_ANALYSIS:
                this.sstsGPRStatus.Text = "GPR Analysis Report";
                this.ssGPRResults.Update();
                break;

              case ReportType.GPR_THICKNESS:
                this.sstsGPRStatus.Text = "GPR Thickness Report";
                this.ssGPRResults.Update();
                break;
            }
          }

          break;

        case TaskType.GPR_SAVE:
          if (!e.Cancelled && e.Error == null)
          {
            this.sstsGPRStatus.Text = "Data has been saved.";
          }
          else
          {
            this.sstsGPRStatus.Text = "Save data failed.";
          }
          break;

        case TaskType.GPR_ELMOD:
          if (!e.Cancelled && e.Error == null)
          {
            this.sstsGPRStatus.Text = "ELMOD files have been created.";
          }
          else
          {
            this.sstsGPRStatus.Text = "Creating ELMOD failed.";
          }
          break;

        case TaskType.FWD_IMPORT:
          this.dgvGPRResults.DataSource = this.dtFWDStations;
          break;
      }

      this.sstsGPRProgressBar.Visible = false;
    }

    private void DisplayWarningSections()
    {
      try
      {
        if (this.GPRCollections.Count == 0)
        {
          SelectCollections sc = new SelectCollections(this.dataMapper);
          if (sc.ShowDialog(this) == DialogResult.OK)
          {
            this.sstsGPRStatus.Text = "Loading collections ...";
            this.ssGPRResults.Update();

            this.dtGPRCollections = sc.GroupPartitionExplorer.SelectedCollections;
            this.GPRCollections = this.GetCollectionList(this.dtGPRCollections);

            this.sstsGPRStatus.Text = "";
            this.ssGPRResults.Update();
          }
        }

        if (this.GPRCollections.Count > 0)
        {
          this.EnableButtons(false);
          this.sstsGPRStatus.Text = "Loading sections with warning ...";
          this.ssGPRResults.Update();

          this.dtGPRResults.Clear();
          this.dtGPRProcessedResults.Clear();
          this.dtGPRReport.Clear();
          this.dtGPRReport = null;
          this.dtGPRReport = new DataTable();
          this.dgvGPRResults.DataSource = null;

          this.sstsGPRProgressBar.Visible = true;
          this.taskType = TaskType.GPR_WARNING;
          this.bgWorker.RunWorkerAsync();
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void DisplayWarningSections(List<string> fileList)
    {
      if (fileList.Count > 0)
      {
        try
        {
          string sqlComm;
          Dictionary<string, object> paras = new Dictionary<string, object>();

          foreach (string filename in fileList)
          {
            paras.Clear();
            paras.Add("ARANFileName", filename);
            sqlComm = "exec gpr.usp_GetGPRWarningSections @ARANFileName";

            this.dataMapper.DataProvider.FillDataTable(this.dtGPRReport, sqlComm, paras);
          }
        }
        catch (Exception ex)
        {
          throw ex;
        }
      }
    }

    private bool ErrorCheckGPRResults()
    {
      //error check the PCK file to ensure there is a confidence record between each construction change
      GPRImportErrorText = "";

      foreach (string filename in this.GPRCollections)
      {
        double prevChain = -1;

        DataRow[] rows = this.dtGPRResults.Select("GPRFileName like '" + filename + "%'", "X1 ASC");

        if (rows.Length > 0)
        {
          for (int i = 0; i < rows.Length - 1; i++)
          {
            if (rows[i][11].ToString() == "0")
            {

              if (prevChain > -1)
              {
                DataRow[] confRows = this.dtGPRResults.Select("X1 > " + prevChain + " AND X1  < " + Convert.ToDouble(rows[i][5]) + " AND LayerNumber in (81,82,83) and GPRFileName = '" + rows[i][1].ToString() + "'", "X1 ASC");

                if (confRows.Length < 1)
                {
                  GPRImportErrorText += rows[i][1].ToString() + " between " + prevChain.ToString() + " and " + Convert.ToDouble(rows[i][5]) + "\n";
                }

              }


              prevChain = Convert.ToDouble(rows[i][5]);

            }
          }
        }
      }

      if (GPRImportErrorText != "")
      {
        MessageBox.Show("Missing Confidence: \n\n" + GPRImportErrorText + "\n Results have been copied to the Clipboard", "Import Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }

      return true;
    }

    private void executeBulkCopy(SqlConnection conn, DataRow[] rows, string tableName)
    {
      int retryCount = 3;
      bool success = false;
      while (retryCount > 0 && !success)
      {
        try
        {
          conn.Open();
          using (SqlBulkCopy bcopy = new SqlBulkCopy(conn))
          {

            bcopy.BulkCopyTimeout = 15000;
            bcopy.DestinationTableName = tableName;
            bcopy.WriteToServer(rows);

          }

          success = true;
        }
        catch (SqlException exception)
        {
          if (exception.Number != 1205)
          {
            // a sql exception that is not a deadlock  
            throw;
          }
          // Add delay here if you wish.  
          retryCount--;
          if (retryCount == 0) throw;
        }
        finally
        {
          conn.Close();
        }
      }
    }

    private void executeGPRNonQuery(SqlConnection conn, SqlCommand command)
    {
      command.Connection = conn;

      int retryCount = 3;
      bool success = false;
      while (retryCount > 0 && !success)
      {
        try
        {
          conn.Open();
          command.ExecuteNonQuery();
          success = true;
        }
        catch (SqlException exception)
        {
          if (exception.Number != 1205)
          {
            // a sql exception that is not a deadlock  
            throw;
          }
          // Add delay here if you wish.  
          retryCount--;
          if (retryCount == 0) throw;
        }
        finally
        {
          conn.Close();
        }
      }
    }

    private void GenerateELMOD()
    {
      // it is for LAGPR project only and should be make it generic in the future
      if (this.reportType != ReportType.GPR_THICKNESS ||
        (this.reportType == ReportType.GPR_THICKNESS && this.dtGPRReport.Rows.Count == 0))
      {
        MessageBox.Show("Please generate thickness report first.", "Warning");
        return;
      }

      if (this.fbSelectFolder.ShowDialog(this) == DialogResult.OK)
      {
        this.EnableButtons(false);
        this.sstsGPRStatus.Text = "Generating ELMOD ...";
        this.ssGPRResults.Update();

        this.sstsGPRProgressBar.Visible = true;
        this.taskType = TaskType.GPR_ELMOD;
        this.bgWorker.RunWorkerAsync(this.fbSelectFolder.SelectedPath);
      }
    }

    private void GenerateELMOD(string path)
    {
      path = path.EndsWith("\\") ? path : path + "\\";

      DataRow[] rows = this.dtGPRReport.Select("CSECT LIKE '%'", "CSECT, DIRECTION, ROUTE, CS_FROM ASC");
      int count = rows.Length;

      if (count > 0)
      {
        // get distinct routes
        DataTable dtRoutes = new DataTable();
        dtRoutes.Columns.Add("CSECT", typeof(string));
        dtRoutes.Columns.Add("DIRECTION", typeof(string));
        dtRoutes.Columns.Add("ROUTE", typeof(string));
        dtRoutes.Columns.Add("CS_FROM", typeof(string));
        dtRoutes.Columns.Add("CS_TO", typeof(string));

        string csect, direction, route, from, to;
        string prevCsect, prevDir, prevRoute;

        csect = Convert.ToString(rows[0]["CSECT"]);
        direction = Convert.ToString(rows[0]["DIRECTION"]);
        route = Convert.ToString(rows[0]["ROUTE"]);
        from = Convert.ToString(rows[0]["CS_FROM"]);

        prevCsect = csect;
        prevDir = direction;
        prevRoute = route;


        for (int i = 1; i < count; i++)
        {
          csect = Convert.ToString(rows[i]["CSECT"]);
          direction = Convert.ToString(rows[i]["DIRECTION"]);
          route = Convert.ToString(rows[i]["ROUTE"]);

          if (csect != prevCsect || direction != prevDir || route != prevRoute || i == count - 1)
          {
            DataRow row = dtRoutes.NewRow();
            row["CSECT"] = prevCsect;
            row["DIRECTION"] = prevDir;
            row["ROUTE"] = prevRoute;
            row["CS_FROM"] = from;

            if (i == count - 1)
              row["CS_TO"] = Convert.ToString(rows[i]["CS_TO"]);
            else
              row["CS_TO"] = Convert.ToString(rows[i - 1]["CS_TO"]);

            dtRoutes.Rows.Add(row);

            if (i < count - 1)
            {
              prevCsect = Convert.ToString(rows[i]["CSECT"]);
              prevDir = Convert.ToString(rows[i]["DIRECTION"]);
              prevRoute = Convert.ToString(rows[i]["ROUTE"]);
              from = Convert.ToString(rows[i]["CS_FROM"]);
            }
          }
        }

        // create ELMOD for each route
        string fileFullName;
        foreach (DataRow row in dtRoutes.Rows)
        {
          csect = Convert.ToString(row["CSECT"]);
          direction = Convert.ToString(row["DIRECTION"]);
          route = Convert.ToString(row["Route"]);
          from = Convert.ToString(row["CS_FROM"]);
          to = Convert.ToString(row["CS_TO"]);

          DataRow[] rrows = this.dtGPRReport.Select("CSECT = '" + csect +
                             "' AND DIRECTION = '" + direction +
                             "' AND ROUTE = '" + route + "'", "CS_FROM ASC");

          direction = direction == "1" ? "P" : "S";
          fileFullName = path + csect + direction + " " + route + " LM " + from + " to LM " + to + ".gpr";

          this.GenerateELMOD(fileFullName, rrows);
        }
      }
    }

    private void GenerateELMOD(string fileFullName, DataRow[] rows)
    {
      if (rows.Length == 0)
        return;

      try
      {
        if (File.Exists(fileFullName))
        {
          File.Delete(fileFullName);
        }

        StreamWriter writer = new StreamWriter(fileFullName);
        double thickness1, thickness2, thickness3, thickness4;
        string chainage;

        foreach (DataRow row in rows)
        {
          chainage = Convert.ToString(row["CS_FROM"]);
          thickness1 = Convert.ToDouble(row["GPR_LAY_TH_01"]);
          thickness2 = Convert.ToDouble(row["GPR_LAY_TH_02"]);
          thickness3 = Convert.ToDouble(row["GPR_LAY_TH_03"]);
          thickness4 = Convert.ToDouble(row["GPR_LAY_TH_04"]);

          if (thickness1 > 0)
          {
            thickness2 = thickness2 == -1 ? 0 : thickness2;
            thickness3 = thickness3 == -1 ? 0 : thickness3;
            thickness4 = thickness4 == -1 ? 0 : thickness4;

            writer.WriteLine(chainage + "\t" + thickness1.ToString() + "\t" + thickness2.ToString() +
                     "\t" + thickness3.ToString() + "\t" + thickness4.ToString());
          }
        }

        writer.Flush();
        writer.Close();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void GenerateGPRAnalysisReport()
    {
      GenerateGPRReport(ReportType.GPR_ANALYSIS);
    }

    private void GenerateGPRReport(ReportType repType)
    {
      try
      {
        if (this.GPRCollections.Count == 0)
        {
          SelectCollections sc = new SelectCollections(this.dataMapper);
          if (sc.ShowDialog(this) == DialogResult.OK)
          {
            this.sstsGPRStatus.Text = "Loading collections ...";
            this.ssGPRResults.Update();

            this.dtGPRCollections = sc.GroupPartitionExplorer.SelectedCollections;
            this.GPRCollections = this.GetCollectionList(this.dtGPRCollections);

            this.sstsGPRStatus.Text = "";
            this.ssGPRResults.Update();
          }
        }

        if (this.GPRCollections.Count > 0)
        {
          GPRSettings gs = new GPRSettings(DisplayOption.GPRReporting);
          gs.SetChainageUnit(DisplayOption.GPRReporting, this.GPRReportSetting.ChainageUnit);
          gs.SetThicknessUnit(DisplayOption.GPRReporting, this.GPRReportSetting.ThicknessUnit);
          gs.GPRReportingInterval = this.GPRReportSetting.ReportingInterval;

          if (gs.ShowDialog(this) == DialogResult.OK)
          {
            this.GPRReportSetting.ChainageUnit = gs.GetChainageUnit(DisplayOption.GPRReporting);
            this.GPRReportSetting.ThicknessUnit = gs.GetThicknessUnit(DisplayOption.GPRReporting);
            this.GPRReportSetting.ReportingInterval = gs.GPRReportingInterval;

            object[] paras = new object[3];
            double chainageConversion = gs.GetConversionFactor(this.GPRReportSetting.ChainageUnit);
            paras[0] = chainageConversion;
            paras[1] = gs.GetConversionFactor(this.GPRReportSetting.ThicknessUnit);
            paras[2] = this.GPRReportSetting.ReportingInterval / chainageConversion;

            this.reportType = repType;

            this.EnableButtons(false);
            this.sstsGPRStatus.Text = "Generating GPR report ...";
            this.ssGPRResults.Update();

            this.dtGPRResults.Clear();
            this.dtGPRProcessedResults.Clear();
            this.dtGPRReport.Clear();
            this.dtGPRReport = null;
            this.dtGPRReport = new DataTable("GPR_Report");
            this.dgvGPRResults.DataSource = null;

            this.sstsGPRProgressBar.Visible = true;
            this.taskType = TaskType.GPR_REPORT;
            this.bgWorker.RunWorkerAsync(paras);
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void GenerateGPRReport(double chainageConversion, double thicknessConversion, double interval)
    {

      List<string> errorFiles = new List<string>();

      try
      {
        string sp = string.Empty;

        if (this.taskType == TaskType.GPR_PCK)
        {
          if (this.reportType == ReportType.GPR_RESEARCH)
          {
            sp = "gpr.usp_GetGPRPicks_TestSection";
          }
          else
          {
            sp = "gpr.usp_GetGPRPicks";
          }
        }
        else
        {
          switch (this.reportType)
          {
            case ReportType.GPR_ANALYSIS:
              sp = "gpr.usp_GetGPRReport";
              break;

            case ReportType.GPR_THICKNESS:
              sp = "gpr.usp_GetGPRThicknessReport";
              break;

            case ReportType.GPR_RESEARCH:
              sp = "gpr.usp_GetGPRThicknessReport_TestSection";
              break;
          }
        }



        using (SqlConnection conn = new SqlConnection(this.connStr))
        {
          conn.Open();

          foreach (string filename in this.GPRCollections)
          {
            using (SqlCommand command = new SqlCommand())
            {
              command.Connection = conn;
              command.CommandTimeout = 60;

              command.CommandText = sp;
              command.CommandType = CommandType.StoredProcedure;
              command.Parameters.Add(new SqlParameter("@ARANFileName", filename));
              command.Parameters.Add(new SqlParameter("@Interval", interval));
              command.Parameters.Add(new SqlParameter("@ReportChainageUnitConversion", chainageConversion));
              command.Parameters.Add(new SqlParameter("@ThicknessUnitConversion", thicknessConversion));

              try
              {

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                  adapter.Fill(this.dtGPRReport);
                }
              }
              catch
              {
                errorFiles.Add(filename);
              }
            }
          }

          conn.Close();
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }

      if (errorFiles.Count > 0)
      {
        string errorMsg = "";
        foreach (string errorFile in errorFiles)
        {
          errorMsg = errorFile + Environment.NewLine;
        }

        MessageBox.Show("Error with these files: " + Environment.NewLine + errorMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

    }

    private void GenerateGPRThicknessReport()
    {
      GenerateGPRReport(ReportType.GPR_THICKNESS);
    }

    private void GeneratePCK(ReportType reportType)
    {
      try
      {
        if (this.GPRCollections.Count == 0)
        {
          SelectCollections sc = new SelectCollections(this.dataMapper);
          if (sc.ShowDialog(this) == DialogResult.OK)
          {
            this.sstsGPRStatus.Text = "Loading collections ...";
            this.ssGPRResults.Update();

            this.dtGPRCollections = sc.GroupPartitionExplorer.SelectedCollections;
            this.GPRCollections = this.GetCollectionList(this.dtGPRCollections);

            this.sstsGPRStatus.Text = "";
            this.ssGPRResults.Update();
          }
        }

        if (this.GPRCollections.Count > 0)
        {
          GPRSettings gs = new GPRSettings(DisplayOption.GPRReporting);
          gs.SetChainageUnit(DisplayOption.GPRReporting, this.GPRReportSetting.ChainageUnit);
          gs.SetThicknessUnit(DisplayOption.GPRReporting, this.GPRReportSetting.ThicknessUnit);
          gs.GPRReportingInterval = this.GPRReportSetting.ReportingInterval;

          if (gs.ShowDialog(this) == DialogResult.OK)
          {
            this.GPRReportSetting.ChainageUnit = gs.GetChainageUnit(DisplayOption.GPRReporting);
            this.GPRReportSetting.ThicknessUnit = gs.GetThicknessUnit(DisplayOption.GPRReporting);
            this.GPRReportSetting.ReportingInterval = gs.GPRReportingInterval;

            object[] paras = new object[3];
            double chainageConversion = gs.GetConversionFactor(this.GPRReportSetting.ChainageUnit);
            paras[0] = chainageConversion;
            paras[1] = gs.GetConversionFactor(this.GPRReportSetting.ThicknessUnit);
            paras[2] = this.GPRReportSetting.ReportingInterval / chainageConversion;

            this.EnableButtons(false);
            this.sstsGPRStatus.Text = "Generating PCK ...";
            this.ssGPRResults.Update();

            this.dtGPRResults.Clear();
            this.dtGPRProcessedResults.Clear();
            this.dtGPRReport.Clear();
            this.dtGPRReport = null;
            this.dtGPRReport = new DataTable("GPR_PCK");
            this.dgvGPRResults.DataSource = null;

            this.sstsGPRProgressBar.Visible = true;
            this.taskType = TaskType.GPR_PCK;
            this.reportType = reportType;
            this.bgWorker.RunWorkerAsync(paras);
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void ImportGPRResults()
    {
      this.fdOpenFile.Multiselect = true;
      this.fdOpenFile.DefaultExt = "pck";
      this.fdOpenFile.Filter = "GPR Files | *.pck";

      if (this.fdOpenFile.ShowDialog(this) == DialogResult.OK)
      {
        GPRResultsOptions gprOptions = new GPRResultsOptions();

        if (gprOptions.ShowDialog() == DialogResult.OK)
        {
          gprResults_all = gprOptions.all;
          mar2path = gprOptions.mar2folder;

          this.EnableButtons(false);
          this.sstsGPRStatus.Text = "Importing GPR results ...";
          this.ssGPRResults.Update();

          this.ResetGPRResults();

          logGPRTool("Import GPR Results", "", this.fdOpenFile.FileNames[0].ToString());

          this.taskType = TaskType.GPR_IMPORT;
          this.sstsGPRProgressBar.Visible = true;
          this.bgWorker.RunWorkerAsync(this.fdOpenFile.FileNames);
        }
      }
    }

    private void ImportGPRResults(string[] filenames)
    {
      ReadGPRResults(filenames);

      if (!gprResults_all)
      {
        ReadGPRResults_mar2();
      }

      //if (ErrorCheckGPRResults())
      //{
      SaveGPRResultsToDatabase();
      //}
      //else
      //{
      //this.dtGPRResults.Clear();
      //  this.GPRCollections.Clear();
      //}
    }

    private void LoadProcessedGPRResults()
    {
      try
      {
        string sqlComm;
        Dictionary<string, object> paras = new Dictionary<string, object>();

        sqlComm = "exec gpr.usp_GetProcessedGPRResults @ARANFileName";

        foreach (string cls in this.GPRCollections)
        {
          paras.Clear();
          paras.Add("ARANFileName", cls);
          this.dataMapper.DataProvider.FillDataTable(this.dtGPRProcessedResults, sqlComm, paras);
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private void PerformGPRResultTask(TaskType taskType, DoWorkEventArgs e)
    {
      switch (taskType)
      {
        case TaskType.GPR_IMPORT:
          string[] filenames = e.Argument as string[];
          this.ImportGPRResults(filenames);
          break;

        case TaskType.GPR_PROCESS:
          object[] paras = e.Argument as object[];
          double threshold = Convert.ToDouble(paras[0]);
          double velocity = Convert.ToDouble(paras[1]);

          this.ProcessGPRResults(threshold, velocity);
          break;

        case TaskType.GPR_REPORT:
        case TaskType.GPR_PCK:
          object[] rParas = e.Argument as object[];
          double chainageConversion = Convert.ToDouble(rParas[0]);
          double thicknessConversion = Convert.ToDouble(rParas[1]);
          double interval = Convert.ToDouble(rParas[2]);

          this.GenerateGPRReport(chainageConversion, thicknessConversion, interval);
          break;

        case TaskType.GPR_SAVE:
          object[] sParas = e.Argument as object[];
          string filename = Convert.ToString(sParas[0]);
          string filetype = Convert.ToString(sParas[1]);
          this.Save(filename, filetype);
          break;

        case TaskType.GPR_ELMOD:
          string path = e.Argument as string;
          this.GenerateELMOD(path);
          break;

        case TaskType.GPR_WARNING:
          this.DisplayWarningSections(this.GPRCollections);
          break;

        case TaskType.FWD_IMPORT:
          string[] fwdFilenames = e.Argument as string[];
          this.ImportFWDStations(fwdFilenames);
          break;
      }
    }

    private void ProcessGPRResults()
    {
      try
      {
        if (this.GPRCollections.Count == 0)
        {
          SelectCollections sc = new SelectCollections(this.dataMapper);
          if (sc.ShowDialog(this) == DialogResult.OK)
          {
            this.sstsGPRStatus.Text = "Loading collections ...";
            this.ssGPRResults.Update();

            this.dtGPRCollections = sc.GroupPartitionExplorer.SelectedCollections;
            this.GPRCollections = this.GetCollectionList(this.dtGPRCollections);

            this.sstsGPRStatus.Text = "";
            this.ssGPRResults.Update();
          }
        }

        if (this.GPRCollections.Count > 0)
        {
          GPRSettings gs = new GPRSettings(DisplayOption.GPRProcessing);
          gs.GPRVelocity = this.GPRReportSetting.Velocity;
          gs.GPRThreshold = this.GPRReportSetting.Threshold;
          gs.GPRVelocityUnit = this.GPRReportSetting.VelocityUnit;
          gs.GPRVelocityType = this.GPRReportSetting.SelectedVelocityType;

          if (gs.ShowDialog(this) == DialogResult.OK)
          {
            this.GPRReportSetting.Velocity = gs.GPRVelocity;
            this.GPRReportSetting.VelocityUnit = gs.GPRVelocityUnit;
            this.GPRReportSetting.Threshold = gs.GPRThreshold;
            this.GPRReportSetting.SelectedVelocityType = gs.GPRVelocityType;

            object[] paras = new object[2];
            paras[0] = this.GPRReportSetting.Threshold;
            paras[1] = this.GPRReportSetting.Velocity == 0 ? this.GPRReportSetting.Velocity :
                   this.GPRReportSetting.Velocity / gs.GetVelocityConversionFactor(this.GPRReportSetting.VelocityUnit);

            this.EnableButtons(false);
            this.sstsGPRStatus.Text = "Processing GPR results ...";
            this.ssGPRResults.Update();

            this.dtGPRResults.Clear();
            this.dtGPRReport.Clear();
            this.dtGPRProcessedResults.Clear();
            this.dgvGPRResults.DataSource = null;

            this.sstsGPRProgressBar.Visible = true;
            this.taskType = TaskType.GPR_PROCESS;
            this.bgWorker.RunWorkerAsync(paras);
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void ProcessGPRResults(double threshold, double velocity)
    {
      List<string> errorFiles = new List<string>();

      using (SqlConnection conn = new SqlConnection(this.connStr))
      {
        conn.Open();

        foreach (string filename in this.GPRCollections)
        {

          try
          {
            using (SqlCommand command = new SqlCommand())
            {
              command.Connection = conn;
              command.CommandTimeout = 300;

              command.CommandText = "gpr.usp_ProcessGPRResults";
              command.CommandType = CommandType.StoredProcedure;
              command.Parameters.Add(new SqlParameter("@ARANFileName", filename));
              command.Parameters.Add(new SqlParameter("@Velocity", velocity));
              command.Parameters.Add(new SqlParameter("@Threshold", threshold));

              command.ExecuteNonQuery();

            }
          }
          catch (Exception ex)
          {
            //add the file to the error list but continue processing
            errorFiles.Add(filename);
          }
        }

        conn.Close();
      }

      if (errorFiles.Count > 0)
      {
        string errorText = "Error with the following " + errorFiles.Count.ToString() + " files: \n\n";

        foreach (string file in errorFiles)
        {
          errorText += file + "\n";
        }

        MessageBox.Show(errorText, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void ReadGPRResults(string[] filenames)
    {
      this.GPRCollections.Clear();
      this.dtGPRResults.Clear();

      string line, uniquerun;
      string[] data;
      StreamReader reader = null;

      try
      {
        foreach (string filename in filenames)
        {
          reader = new StreamReader(filename);

          line = reader.ReadLine();
          while (line != null)
          {
            data = line.Split(new string[] { "\t", " " }, StringSplitOptions.RemoveEmptyEntries);

            if (data.Length >= 12)
            {
              uniquerun = data[0].Substring(0, 8);

              if (!this.GPRCollections.Contains(uniquerun))
              {
                this.GPRCollections.Add(uniquerun);
              }

              DSGPRResults.GPRResultsRow row = this.dtGPRResults.NewGPRResultsRow();
              row.GPRFileName = data[0];
              row.Time = Convert.ToDouble(data[1]);
              row.Depth = Convert.ToDouble(data[2]);
              row.Amplitude = Convert.ToDouble(data[3]);
              row.X1 = Convert.ToDouble(data[4]);
              row.Y1 = Convert.ToDouble(data[5]);
              row.Z1 = Convert.ToDouble(data[6]);
              row.X2 = Convert.ToDouble(data[7]);
              row.Y2 = Convert.ToDouble(data[8]);
              row.Z2 = Convert.ToDouble(data[9]);
              row.LayerNumber = Convert.ToInt32(data[10]);
              row.ScanNumber = Convert.ToInt32(Convert.ToDecimal(data[11]));


              this.dtGPRResults.Rows.Add(row);
            }

            line = reader.ReadLine();
          }

          reader.Close();
          reader = null;
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      finally
      {
        if (reader != null)
          reader.Close();
      }
    }

    private void ReadGPRResults_mar2()
    {
      //read the mar2 into a datatable
      this.dtGPRResults_mar2.Clear();

      string line;
      string[] data;
      StreamReader reader = null;

      DirectoryInfo mar2Dir = new DirectoryInfo(mar2path);

      FileInfo[] mar2Files = mar2Dir.GetFiles("*.txt");

      try
      {
        foreach (FileInfo filename in mar2Files)
        {
          reader = new StreamReader(filename.FullName);

          line = reader.ReadLine();

          while (line != null)
          {
            //find the first index of a space, insert a * as the split character. TXT file is not comma delimted
            int g = line.IndexOf(" ", 0);
            if (line.Length > 1)
            {
              line = filename.Name.Substring(0, 8) + "*" + line.Substring(0, g) + "*" + line.Substring(g + 1, line.Length - (g + 1));

              data = line.Split(new string[] { "*" }, StringSplitOptions.RemoveEmptyEntries);

              DSGPRResults.GPRResults_staging_mar2Row row = this.dtGPRResults_mar2.NewGPRResults_staging_mar2Row();
              row.Uniquerun = data[0];
              row.Chainage = Convert.ToDouble(data[1]);
              row.Events = data[2];

              this.dtGPRResults_mar2.Rows.Add(row);
            }
            line = reader.ReadLine();
          }

          reader.Close();
          reader = null;

        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      finally
      {
        if (reader != null)
          reader.Close();
      }

    }

    private void ResetGPRResults()
    {
      this.dtGPRResults.Clear();
      this.dtGPRReport.Clear();
      this.dtGPRCollections.Clear();
      this.GPRCollections.Clear();
      this.dtFWDStations.Clear();

      this.dgvGPRResults.DataSource = null;
      this.ucGPRCollections.DataSource = null;
      this.sstsGPRCollections.Text = "";
      this.sstsCoreResultStatus.Text = "";
    }

    private void Save(string filename, string fileType)
    {
      switch (fileType)
      {
        case "GPR_Report":
          this.SaveGPRReport(filename);
          break;

        case "GPR_PCK":
          this.SaveGPRPicks(filename);
          break;
      }
    }

    private void SaveGPRPicks(string filename)
    {
      StreamWriter writer = null;

      try
      {
        if (File.Exists(filename))
        {
          File.Delete(filename);
        }

        writer = new StreamWriter(filename);

        foreach (string fname in this.GPRCollections)
        {
          DataRow[] rows = this.dtGPRReport.Select("FileName LIKE '" + fname + "%'", "X1 ASC");

          for (int i = 0; i < rows.Length; i++)
          {
            writer.WriteLine(Convert.ToString(rows[i]["FileName"]) + "\t" +
                     Convert.ToString(rows[i]["Time"]) + "\t" +
                     Convert.ToString(rows[i]["Depth"]) + "\t" +
                     Convert.ToString(rows[i]["Amplitude"]) + "\t" +
                     Convert.ToString(rows[i]["X1"]) + "\t" +
                     Convert.ToString(rows[i]["Y1"]) + "\t" +
                     Convert.ToString(rows[i]["Z1"]) + "\t" +
                     Convert.ToString(rows[i]["X2"]) + "\t" +
                     Convert.ToString(rows[i]["Y2"]) + "\t" +
                     Convert.ToString(rows[i]["Z2"]) + "\t" +
                     Convert.ToString(rows[i]["LayerNumber"]) + "\t" +
                     Convert.ToString(rows[i]["ScanNumber"]));
          }

          writer.Flush();
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
      finally
      {
        if (writer != null)
          writer.Close();
      }
    }

    private void SaveGPRReport(string filename)
    {
      try
      {
        string tbName = string.Empty;

        switch (this.reportType)
        {
          case ReportType.GPR_ANALYSIS:
            tbName = "STRUCTURAL_ANALYSIS_RESULTS";
            break;

          case ReportType.GPR_THICKNESS:
          case ReportType.GPR_RESEARCH:
            tbName = "REPRESENTATIVE_THICKNESS";
            break;
        }

        //dtGPRReport.TableName = tbName;

        string connStr = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", filename);
        if (!File.Exists(filename))
        {
          ADOX.Catalog cat = new ADOX.Catalog();
          cat.Create(connStr);
          cat = null;
        }

        using (OleDbConnection conn = new OleDbConnection(connStr))
        {
          conn.Open();

          this.CreateMSAccessTable(this.dtGPRReport, tbName, conn);

          foreach (DataRow row in this.dtGPRReport.Rows)
          {
            string sqlColumns = "";
            string sqlValues = "";
            string colValue = "";
            foreach (DataColumn column in this.dtGPRReport.Columns)
            {
              if (!row.IsNull(column))
              {
                if (sqlColumns != "")
                {
                  sqlColumns += ",";
                }

                sqlColumns += "[" + column.ColumnName + "]";

                if (sqlValues != "")
                {
                  sqlValues += ",";
                }

                if (column.DataType == System.Type.GetType("System.String"))
                {
                  colValue = row[column].ToString();
                  colValue = colValue.Replace("'", "''");
                  sqlValues += "'" + colValue + "'";
                }
                else
                {
                  sqlValues += row[column].ToString();
                }
              }
            }

            using (OleDbCommand command = new OleDbCommand())
            {
              command.Connection = conn;
              command.CommandText = "INSERT INTO " + tbName +
                " (" + sqlColumns + ") " +
                " VALUES (" + sqlValues + ");";

              command.ExecuteNonQuery();
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private void SaveGPRResults()
    {
      if (this.dtGPRReport.Rows.Count > 0)
      {
        switch (this.dtGPRReport.TableName)
        {
          case "GPR_Report":
            this.sfSaveFile.DefaultExt = "mdb";
            this.sfSaveFile.Filter = "MS Access File | *.mdb";
            break;

          case "GPR_PCK":
            this.sfSaveFile.DefaultExt = "pck";
            this.sfSaveFile.Filter = "GPR PCK File | *.pck";
            break;
        }

        if (this.sfSaveFile.ShowDialog(this) == DialogResult.OK)
        {
          this.EnableButtons(false);
          this.sstsGPRStatus.Text = "Saving ...";
          this.ssGPRResults.Update();

          this.taskType = TaskType.GPR_SAVE;
          this.sstsGPRProgressBar.Visible = true;

          object[] paras = new object[2];
          paras[0] = this.sfSaveFile.FileName;
          paras[1] = this.dtGPRReport.TableName;
          this.bgWorker.RunWorkerAsync(paras);
        }
      }
    }

    private void SaveGPRResultsToDatabase()
    {
      GPRImportErrorText = "";

      if (dtImportErrors.Columns.Count < 1)
      {
        dtImportErrors.Columns.Add("Uniquerun");
        dtImportErrors.Columns.Add("Chainage");
        dtImportErrors.Columns.Add("Events");
      }

      dtImportErrors.Rows.Clear();

      if (this.dtGPRResults.Rows.Count > 0)
      {
        try
        {
          using (SqlConnection conn = new SqlConnection(this.connStr))
          {
            //conn.Open();

            using (SqlCommand command = new SqlCommand())
            {
              command.CommandTimeout = 15000;
              //command.Connection = conn;

              foreach (string filename in this.GPRCollections)
              {
                DataRow[] mar2rows = this.dtGPRResults_mar2.Select("Uniquerun like '" + filename + "%'", "Chainage ASC");

                //PO: fb case 1869
                if (mar2rows.Length > 0 || gprResults_all)
                {
                  // delete existing data

                  //clear the staging tables
                  if (!gprResults_all)
                  {
                    command.CommandText = "DELETE FROM gpr.GPRResults_staging WHERE GPRFileName like '%" + filename + "%'";
                    executeGPRNonQuery(conn, command);
                    //command.ExecuteNonQuery();

                    command.CommandText = "DELETE FROM gpr.GPRResults_staging_mar2 WHERE Uniquerun like '%" + filename + "%'";
                    executeGPRNonQuery(conn, command);
                    //command.ExecuteNonQuery();
                  }

                  command.CommandTimeout = 15000;
                  command.CommandText = "gpr.usp_DeleteGPRResults '" + filename + "'";
                  executeGPRNonQuery(conn, command);
                  //command.ExecuteNonQuery();

                  DataRow[] rows = this.dtGPRResults.Select("GPRFileName like '" + filename + "%'", "X1 ASC");


                  if (rows.Length > 0)
                  {
                    // insert all data
                    if (gprResults_all)
                    {
                      //conn.Open();
                      //using (SqlBulkCopy bcopy = new SqlBulkCopy(conn))
                      //{
                      //    bcopy.BulkCopyTimeout = 15000;
                      //    bcopy.DestinationTableName = "gpr.GPRResults";
                      //    bcopy.WriteToServer(rows);
                      //}
                      //conn.Close();
                      executeBulkCopy(conn, rows, "gpr.GPRResults");
                    }
                    else  //insert only the records that have a match in the MAR2 by chainage (X1)
                    {

                      //populate the staging tables
                      if (mar2rows.Length > 0)
                      {
                        executeBulkCopy(conn, rows, "gpr.GPRResults_staging");
                        //bcopy.BulkCopyTimeout = 15000;
                        //bcopy.DestinationTableName = "gpr.GPRResults_staging";
                        //bcopy.WriteToServer(rows);

                        executeBulkCopy(conn, mar2rows, "gpr.GPRResults_staging_mar2");
                        //bcopy.BulkCopyTimeout = 15000;
                        //bcopy.DestinationTableName = "gpr.GPRResults_staging_mar2";
                        //bcopy.WriteToServer(mar2rows);
                      }


                      //PO - this was timing out on larger files, increased to 600 from the default
                      command.CommandTimeout = 15000;

                      //populate gpr.gprresults table with the records that match the mar2
                      command.CommandText = "exec gpr.usp_CopyGPRResultsByMar2 '" + filename + "'";
                      executeGPRNonQuery(conn, command);
                      //command.ExecuteNonQuery();

                      //error check
                      conn.Open();
                      command.CommandTimeout = 15000;
                      command.CommandText = "SELECT * FROM gpr.fn_GPRMar2ImportQC ('" + filename + "')";
                      SqlDataReader reader = command.ExecuteReader();

                      while (reader.Read())
                      {
                        DataRow newrow = dtImportErrors.NewRow();

                        newrow[0] = filename;
                        newrow[1] = reader.GetDouble(0).ToString();
                        newrow[2] = reader.GetString(1);

                        dtImportErrors.Rows.Add(newrow);
                      }

                      reader.Close();
                      conn.Close();



                    }

                    // update idsession and distancestamp
                    command.CommandTimeout = 15000;
                    command.CommandText = "exec gpr.usp_SetGPRIDSessionAndDistanceStamp '" + filename + "'";
                    executeGPRNonQuery(conn, command);
                    //command.ExecuteNonQuery();

                  }
                }
              }

            }

            conn.Close();
          }
        }
        catch (Exception ex)
        {
          throw ex;
        }
      }
    }

    private void tsGPRGenerateReport_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {
      string action = Convert.ToString(e.ClickedItem.Tag);

      switch (action)
      {
        case "Analysis":
          GenerateGPRAnalysisReport();
          break;

        case "Thickness":
          GenerateGPRThicknessReport();
          break;
      }
    }

    private void tsGPRResults_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {
      string action = Convert.ToString(e.ClickedItem.Tag);

      switch (action)
      {
        case "Import":
          ImportGPRResults();
          break;

        case "Process":
          ProcessGPRResults();
          break;

        case "Report":
          GenerateGPRThicknessReport();
          break;

        case "Save":
          SaveGPRResults();
          break;

        case "Create PCK":
          GeneratePCK(ReportType.GPR_THICKNESS);
          break;

        case "Warning":
          DisplayWarningSections();
          break;

        case "ELMOD":
          GenerateELMOD();
          break;

        case "Reset":
          ResetGPRResults();
          break;

        case "Import FWD Stations":
          ImportFWDStations();
          break;
      }
    }

    #region test sections
    private void tsResearch_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {
      string action = Convert.ToString(e.ClickedItem.Tag);

      switch (action)
      {
        case "Import":
          ImportFWDStations();
          break;

        case "Report":
          GenerateGPRReport(ReportType.GPR_RESEARCH);
          break;

        case "Create PCK":
          GeneratePCK(ReportType.GPR_RESEARCH);
          break;
      }
    }

    private void ImportFWDStations()
    {
      this.fdOpenFile.Multiselect = true;
      this.fdOpenFile.DefaultExt = "mdb";
      this.fdOpenFile.Filter = "MS Access (*.mdb) | *.mdb";

      if (this.fdOpenFile.ShowDialog(this) == DialogResult.OK)
      {
        this.EnableButtons(false);
        this.sstsGPRStatus.Text = "Importing FWD ...";
        this.ssGPRResults.Update();

        this.ResetGPRResults();

        this.taskType = TaskType.FWD_IMPORT;
        this.sstsGPRProgressBar.Visible = true;
        this.bgWorker.RunWorkerAsync(this.fdOpenFile.FileNames);
      }
    }

    private void ImportFWDStations(string[] filenames)
    {
      foreach (string filename in filenames)
      {
        ImportFWDStations(filename);
        this.SaveFWDStations();
        CalculateFWDDistanceStampByGPS(filename);
      }
    }

    private void ImportFWDStations(string filename)
    {
      try
      {
        string csect = filename.Substring(filename.IndexOf("_") + 1);
        csect = csect.Substring(0, 6);
        csect = csect.Replace("_", "-");

        DataTable dt = new DataTable();

        using (OleDbConnection conn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename))
        {
          string sqlSelect = "SELECT '" + csect + "' as CSect, " +
                    "Station, StationDirection, Latitude, Longitude, Comment FROM Stations";

          using (OleDbCommand comm = new OleDbCommand())
          {
            comm.Connection = conn;
            comm.CommandText = sqlSelect;

            this.dtFWDStations.Clear();
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(comm))
            {
              adapter.Fill(this.dtFWDStations);
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private void SaveFWDStations()
    {
      if (this.dtFWDStations.Rows.Count == 0)
        return;

      double station, latitude, longitude;
      string csect = string.Empty;
      string direction = string.Empty;
      string comment, sqlCommand;

      try
      {
        using (SqlConnection conn = new SqlConnection(this.connStr))
        {
          conn.Open();

          int i = 0;
          int count = this.dtFWDStations.Rows.Count;

          foreach (DataRow row in this.dtFWDStations.Rows)
          {
            i++;
            csect = Convert.ToString(row["CSect"]);
            direction = Convert.ToString(row["StationDirection"]);
            comment = Convert.ToString(row["Comment"]);
            comment = comment.Replace("'", "''");

            station = Convert.ToDouble(row["Station"]) * 1000;
            latitude = Convert.ToDouble(row["Latitude"]) * Math.PI / 180;
            longitude = Convert.ToDouble(row["Longitude"]) * Math.PI / 180;

            if (i == 1 || i == count)
            {
              int startEnd = i == 1 ? 1 : 2;
              sqlCommand = "INSERT INTO gpr.FWDStations (CSect, StationDirection, Station, Latitude, Longitude, Comment, StartOrEnd) " +
                     "VALUES ('" + csect + "', '" + direction + "', " + station.ToString() + ", " +
                     latitude.ToString() + ", " + longitude.ToString() + ", '" + comment + "', " + startEnd.ToString() + ")";
            }
            else
            {
              sqlCommand = "INSERT INTO gpr.FWDStations (CSect, StationDirection, Station, Latitude, Longitude, Comment) " +
                     "VALUES ('" + csect + "', '" + direction + "', " + station.ToString() + ", " +
                     latitude.ToString() + ", " + longitude.ToString() + ", '" + comment + "')";
            }

            using (SqlCommand comm = new SqlCommand())
            {
              comm.Connection = conn;
              comm.CommandText = sqlCommand;

              comm.ExecuteNonQuery();
            }

          }

          using (SqlCommand comm = new SqlCommand())
          {
            comm.Connection = conn;
            comm.CommandType = CommandType.StoredProcedure;
            comm.CommandText = "gpr.usp_ProcessFWDStations";
            comm.Parameters.Add(new SqlParameter("@CSect", csect));
            comm.Parameters.Add(new SqlParameter("@Direction", direction));

            comm.ExecuteNonQuery();
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private void CalculateFWDDistanceStampByGPS(string filename)
    {
      string csect = filename.Substring(filename.IndexOf("_") + 1);
      csect = csect.Substring(0, 6);
      csect = csect.Replace("_", "-");

      string sqlCommand = "SELECT * FROM gpr.FWDStations WHERE CSect = '" + csect +
                "' and StationDirection = '1' ORDER BY csect, station";

      DataTable dtSections = this.dataMapper.DataProvider.ExecuteDataTable(sqlCommand, null);

      int idSession;
      double latitude, longitude, distancestamp, distance;
      int IDFWDStation;

      if (dtSections.Rows.Count > 0)
      {
        idSession = Convert.ToInt32(dtSections.Rows[0]["IDSession"]);

        sqlCommand = "SELECT Latitude, Longitude, DistanceStamp FROM VehiclePositions WHERE IDSession = " + idSession.ToString() +
               " and DistanceStamp is not null";

        DataTable dtGPS = this.dataMapper.DataProvider.ExecuteDataTable(sqlCommand, null);

        foreach (DataRow row in dtSections.Rows)
        {
          IDFWDStation = Convert.ToInt32(row["IDFWDStation"]);
          latitude = Convert.ToDouble(row["Latitude"]);
          longitude = Convert.ToDouble(row["Longitude"]);

          distancestamp = this.CalculateDistanceStamp(latitude, longitude, dtGPS, out distance);
          distancestamp = distancestamp - 0.3048 * 24;

          if (!double.IsNaN(distancestamp))
          {
            sqlCommand = "UPDATE gpr.FWDStations SET GPS_DistanceStamp = " + distancestamp.ToString() +
                   " WHERE IDFWDStation = " + IDFWDStation.ToString();
            this.dataMapper.DataProvider.ExecuteNonQuery(sqlCommand, null);
          }
        }
      }

    }
    #endregion

    #endregion

    #region QC Reports
    private void tsQCReports_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
    {
      string action = Convert.ToString(e.ClickedItem.Tag);

      switch (action)
      {
        case "Collections":
          SelectQCGroup();
          break;
        case "Lane Analysis":
          StartQCLaneAnalysis();
          break;
        case "Clear Temp Tables":
          ClearTempTables();
          break;
        case "Reset":
          tsslQCSelectedGroup.Text = "No Group Selected";
          dgvQCReports.DataSource = null;
          dtSelectedGroup.Clear();
          dtLanes.Clear();
          break;
      }
    }

    private void SelectQCGroup()
    {

      SelectCollections sc = new SelectCollections(this.dataMapper);

      if (sc.ShowDialog(this) == DialogResult.OK)
      {
        tsslQCSelectedGroup.Text = sc.GroupPartitionExplorer.GroupFullpath[0].ToString();
        dtSelectedGroup = sc.GroupPartitionExplorer.SelectedCollections;

        qcGroupName = sc.GroupPartitionExplorer.GroupFullpath[0].ToString();


        this.dgvQCReports.DataSource = null;


      }
    }

    private void StartQCLaneAnalysis()
    {
      if (tsslQCSelectedGroup.Text == "No Group Selected")
      {
        MessageBox.Show("No Group Selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      SaveFileDialog sfdSave = new SaveFileDialog();
      sfdSave.Filter = "CSV | *.csv";
      sfdSave.Title = "Save as CSV file";
      sfdSave.ShowDialog();

      if (sfdSave.FileName != "")
      {

        dgvQCReports.DataSource = null;
        this.taskType = TaskType.QC_LANE_ANALYSIS;
        this.sstsQCReportsProgressBar.Visible = true;
        this.sstsQCReportsProgressBar.Style = ProgressBarStyle.Blocks;

        logGPRTool("Lane Analysis", tsslQCSelectedGroup.Text, "");

        this.bgWorker.RunWorkerAsync(sfdSave.FileName);
      }


    }

    private void ClearTempTables()
    {
      if (tsslQCSelectedGroup.Text == "No Group Selected")
      {
        MessageBox.Show("No Group Selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      DialogResult result = MessageBox.Show("Clear all temporary tables for " + tsslQCSelectedGroup.Text + "?", "Clear tables?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

      if (result == DialogResult.Yes)
      {
        dgvQCReports.DataSource = null;
        this.taskType = TaskType.CLEAR_TEMP_TABLES;
        //this.sstsQCReportsProgressBar.Visible = true;

        logGPRTool("Clear Temp Tables", tsslQCSelectedGroup.Text, "");
        this.sstsQCReportsProgressBar.Visible = true;
        this.sstsQCReportsProgressBar.Style = ProgressBarStyle.Marquee;


        this.bgWorker.RunWorkerAsync();
      }

    }

    #endregion

    #region helpers
    private void EnableButtons(bool enabled)
    {
      // mar tab
      this.tsMarCollections.Enabled = enabled;
      this.tsMarCreate.Enabled = enabled;
      this.tsMarCreateCSV.Enabled = enabled;
      this.tsMarRefresh.Enabled = enabled;
      this.tsMarImportReportingInterval.Enabled = enabled;

      // core request tab
      this.tsCoreReqImport.Enabled = enabled;
      this.tsCoreReqExport.Enabled = enabled;
      this.tsCoreReqRefresh.Enabled = enabled;
      this.tsCoreReqGenerate.Enabled = enabled;
      this.tsCoreReqDelete.Enabled = enabled;

      // core result tab
      this.tsCoreResImport.Enabled = enabled;
      this.tsCoreResSelect.Enabled = enabled;
      this.tsCoreResExport.Enabled = enabled;
      this.tsCoreResReset.Enabled = enabled;
      this.tsCoreResUpdateImagePath.Enabled = enabled;
      this.tsCoreResDelete.Enabled = enabled;

      // gpr tab
      this.tsGPRImport.Enabled = enabled;
      this.tsGPRProcess.Enabled = enabled;
      this.tsGPRReport.Enabled = enabled;
      this.tsGPRGenerateReport.Enabled = enabled;
      this.tsGPRSave.Enabled = enabled;
      this.tsGPRReset.Enabled = enabled;
      this.tsGPRPick.Enabled = enabled;
      this.tsGPRWarning.Enabled = enabled;
      this.tsResearch.Enabled = enabled;
      this.tsELMOD.Enabled = enabled;

      // setup table
      this.tsSetupLayerCode.Enabled = enabled;
      this.tsSetupVelocity.Enabled = enabled;
      this.tsSetupReset.Enabled = enabled;
    }

    private List<string> GetCollectionList(DataTable dtCollections)
    {
      List<string> lsCollections = new List<string>();
      string collection = string.Empty;

      foreach (DataRow row in dtCollections.Rows)
      {
        collection = Convert.ToString(row["UniqueRun"]);
        //collection = collection.Substring(0, 6) + "00";

        if (!lsCollections.Contains(collection))
        {
          lsCollections.Add(collection);
        }
      }

      return lsCollections;
    }

    private DataTable GetCollections(List<string> lsCollections)
    {
      DataTable dtCollections = new DataTable();

      if (lsCollections.Count > 0)
      {
        try
        {
          string sqlComm;
          Dictionary<string, object> paras = new Dictionary<string, object>();

          foreach (string cls in lsCollections)
          {
            paras.Clear();
            paras.Add("UniqueRun", cls);
            sqlComm = "exec dbo.usp_GetCollection @UniqueRun";

            this.dataMapper.DataProvider.FillDataTable(dtCollections, sqlComm, paras);
          }
        }
        catch (Exception ex)
        {
          throw ex;
        }
      }

      return dtCollections;
    }

    private void CreateMSAccessTable(DataTable table, string tbName, OleDbConnection conn)
    {
      string sqlCommand = "OID int identity(1,1)";
      foreach (DataColumn column in table.Columns)
      {
        sqlCommand += ",\n[" + column.ColumnName + "]";

        if (column.DataType == System.Type.GetType("System.String"))
        {
          sqlCommand += " VARCHAR(250)";
        }
        else
        {
          sqlCommand += " FLOAT";
        }
      }

      try
      {
        if (conn != null && conn.State == ConnectionState.Open)
        {
          DataTable dtTables = conn.GetSchema("Tables");
          DataRow[] rows = dtTables.Select("TABLE_NAME = '" + tbName + "'");

          if (rows.Length == 0)
          {
            using (OleDbCommand command = new OleDbCommand())
            {
              command.Connection = conn;

              sqlCommand = "CREATE TABLE " + tbName + " ( \n" + sqlCommand + "\n);";

              command.CommandText = sqlCommand;
              command.ExecuteNonQuery();
            }
          }
        }
      }
      catch (Exception ex)
      {
        throw ex;
      }
    }

    private string GetDoubleStringFormat(int numDecimal)
    {
      string format = string.Empty;

      if (numDecimal >= 1)
      {
        format = "{0:0.";
        int i = 1;

        while (i <= numDecimal)
        {
          format += "#";
          i++;
        }

        format += "}";
      }

      return format;
    }

    private void FormatDataGridView(DataGridView dg)
    {
      DataTable table = dg.DataSource as DataTable;
      if (table == null)
      {
        return;
      }

      dg.Columns.Clear();

      foreach (DataColumn column in table.Columns)
      {
        if (column.ColumnName.ToLower() == "imagepath")
        {
          DataGridViewLinkColumn linkColumn = new DataGridViewLinkColumn();
          linkColumn.DataPropertyName = column.ColumnName;
          linkColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
          linkColumn.HeaderText = column.Caption;
          linkColumn.HeaderCell.Style.ForeColor = System.Drawing.SystemColors.ControlText;
          linkColumn.ReadOnly = true;
          linkColumn.LinkBehavior = LinkBehavior.HoverUnderline;
          linkColumn.Name = column.ColumnName;

          this.dgvCoreRequest.Columns.Add(linkColumn);
        }
        else
        {
          DataGridViewTextBoxColumn textColumn = new DataGridViewTextBoxColumn();
          textColumn.DataPropertyName = column.ColumnName;
          textColumn.HeaderText = column.Caption;
          textColumn.HeaderCell.Style.ForeColor = System.Drawing.SystemColors.ControlText;
          textColumn.ReadOnly = true;
          textColumn.Name = column.ColumnName;
          if (column.DataType.Name.ToLower() != "string")
          {
            textColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          }
          else
          {
            textColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
          }

          dg.Columns.Add(textColumn);
        }
      }

      dg.Update();
    }
    #endregion

    #region background work
    private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      try
      {
        BackgroundWorker bw = sender as BackgroundWorker;

        if (!bw.CancellationPending)
        {
          switch (this.taskType)
          {
            case TaskType.MAR_SELECT:
            case TaskType.MAR_CREATE:
            case TaskType.MAR_CREATE_CSV:
            case TaskType.REPORTINTERVAL_IMPORT:
              this.PerformMARTask(this.taskType, e);
              break;

            case TaskType.COREREQ_IMPORT:
            case TaskType.COREREQ_GENERATE:
            case TaskType.COREREQ_EXPORT:
              this.PerformCoreRequestTask(this.taskType, e);
              break;

            case TaskType.CORERES_IMPORT:
            case TaskType.CORERES_SELECT:
            case TaskType.CORERES_EXPORT:
            case TaskType.CORERES_SET_PATH:
              this.PerformCoreResultTask(this.taskType, e);
              break;

            case TaskType.GPR_IMPORT:
            case TaskType.GPR_PROCESS:
            case TaskType.GPR_REPORT:
            case TaskType.GPR_SAVE:
            case TaskType.GPR_PCK:
            case TaskType.GPR_ELMOD:
            case TaskType.GPR_WARNING:
            case TaskType.FWD_IMPORT:
              this.PerformGPRResultTask(this.taskType, e);
              break;

            case TaskType.SETUP_LAYER:
            case TaskType.SETUP_VELOCITY:
              this.PerformSetupTask(this.taskType, e);
              break;
            case TaskType.QC_LANE_ANALYSIS:
              this.PerformQCReportTask(this.taskType, e);
              break;
            case TaskType.CLEAR_TEMP_TABLES:
              this.PerformClearTempTables();
              break;
          }
        }
      }
      catch (Exception ex)
      {
        e.Cancel = true;
        MessageBox.Show(ex.StackTrace, ex.Message);
      }
    }

    private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      switch (this.taskType)
      {
        case TaskType.MAR_SELECT:
        case TaskType.MAR_CREATE:
        case TaskType.MAR_CREATE_CSV:
        case TaskType.REPORTINTERVAL_IMPORT:
          CompleteMARTask(this.taskType, e);
          break;

        case TaskType.COREREQ_IMPORT:
        case TaskType.COREREQ_GENERATE:
        case TaskType.COREREQ_EXPORT:
          CompleteCoreRequestTask(this.taskType, e);
          break;

        case TaskType.CORERES_IMPORT:
        case TaskType.CORERES_SELECT:
        case TaskType.CORERES_EXPORT:
        case TaskType.CORERES_SET_PATH:
          CompleteCoreResultTask(this.taskType, e);
          break;

        case TaskType.GPR_IMPORT:
        case TaskType.GPR_PROCESS:
        case TaskType.GPR_REPORT:
        case TaskType.GPR_SAVE:
        case TaskType.GPR_PCK:
        case TaskType.GPR_ELMOD:
        case TaskType.GPR_WARNING:
        case TaskType.FWD_IMPORT:
          CompleteGPRResultTask(this.taskType, e);
          break;

        case TaskType.SETUP_LAYER:
        case TaskType.SETUP_VELOCITY:
          CompleteSetupTask(this.taskType, e);
          break;
        case TaskType.QC_LANE_ANALYSIS:
          CompleteQCReportTask(this.taskType, e);
          break;
        case TaskType.CLEAR_TEMP_TABLES:
          MessageBox.Show("Clear Temp Tables Completed", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
          sstsQCReportsProgressBar.Visible = false;
          break;
      }

      this.EnableButtons(true);
    }
    #endregion

    DataTable dtSelectedGroup = new DataTable();
    string qcGroupName = "";
    int qcGroupID = 0;

    private void PerformQCReportTask(TaskType taskType, DoWorkEventArgs e)
    {
      string filename = "";

      switch (taskType)
      {
        case TaskType.QC_LANE_ANALYSIS:
          filename = e.Argument as string;

          this.QCLaneAnalysis(filename);
          break;
      }

    }


    private void QCLaneAnalysis(string filename)
    {
      dtLanes.Clear();

      string sqlComm = "select idgrouppartition from wf.grouppartitions where grouppartitionname = '" + qcGroupName + "'";
      qcGroupID = Convert.ToInt32(this.dataMapper.DataProvider.ExecuteScalar(sqlComm, null));

      sqlComm = "";
      Dictionary<string, object> paras = new Dictionary<string, object>();

      paras.Clear();
      paras.Add("GroupID", qcGroupID);
      paras.Add("group", qcGroupName);

      sqlComm = "exec gpr.usp_QcLanesToAnalyse @GroupID, @group";

      this.dataMapper.DataProvider.FillDataTable(dtLanes, sqlComm, paras);

      StreamWriter sw = new StreamWriter(filename, false);
      int iColCount = dtLanes.Columns.Count;

      for (int i = 0; i < iColCount; i++)
      {
        sw.Write(dtLanes.Columns[i]);

        if (i < iColCount - 1)
        {
          sw.Write(",");
        }
      }
      sw.Write(sw.NewLine);

      foreach (DataRow dr in dtLanes.Rows)
      {
        for (int i = 0; i < iColCount; i++)
        {
          if (!Convert.IsDBNull(dr[i]))
          {
            sw.Write(dr[i].ToString());
          }
          if (i < iColCount - 1)
          {
            sw.Write(",");
          }
        }
        sw.Write(sw.NewLine);
      }

      sw.Close();

    }

    private void PerformClearTempTables()
    {
      //cleans up the temp tables, to be perfomed after a batch is delivered and accepted
      using (SqlConnection conn = new SqlConnection(this.connStr))
      {
        conn.Open();

        using (SqlCommand command = new SqlCommand())
        {
          command.Connection = conn;
          command.CommandTimeout = 15000;

          string sqlComm = "delete from gpr.GPR_Stage3GPRReps where groupid = (select idgrouppartition from wf.grouppartitions where grouppartitionname = '" + qcGroupName + "')";
          qcGroupID = Convert.ToInt32(this.dataMapper.DataProvider.ExecuteScalar(sqlComm, null));

          command.CommandText = sqlComm;
          command.ExecuteNonQuery();

          sqlComm = "delete from gpr.GPR_Stage3CoringReps where groupid = (select idgrouppartition from wf.grouppartitions where grouppartitionname = '" + qcGroupName + "')";
          qcGroupID = Convert.ToInt32(this.dataMapper.DataProvider.ExecuteScalar(sqlComm, null));

          command.CommandText = sqlComm;
          command.ExecuteNonQuery();

          sqlComm = "delete from gpr.GPR_Stage1Reps where groupid = (select idgrouppartition from wf.grouppartitions where grouppartitionname = '" + qcGroupName + "')";
          qcGroupID = Convert.ToInt32(this.dataMapper.DataProvider.ExecuteScalar(sqlComm, null));

          command.CommandText = sqlComm;
          command.ExecuteNonQuery();

          sqlComm = "delete from gpr.Stage1bRep where groupid = (select idgrouppartition from wf.grouppartitions where grouppartitionname = '" + qcGroupName + "')";
          qcGroupID = Convert.ToInt32(this.dataMapper.DataProvider.ExecuteScalar(sqlComm, null));

          command.CommandText = sqlComm;
          command.ExecuteNonQuery();

          sqlComm = "delete from gpr.Stage1bRepDetail where groupid = (select idgrouppartition from wf.grouppartitions where grouppartitionname = '" + qcGroupName + "')";
          qcGroupID = Convert.ToInt32(this.dataMapper.DataProvider.ExecuteScalar(sqlComm, null));

          command.CommandText = sqlComm;
          command.ExecuteNonQuery();

          foreach (DataRow row in dtSelectedGroup.Rows)
          {
            sqlComm = "delete from gpr.GPRResults_staging where gprfilename like '%" + row["UniqueRun"].ToString().Substring(0, 6) + "%'";
            qcGroupID = Convert.ToInt32(this.dataMapper.DataProvider.ExecuteScalar(sqlComm, null));

            command.CommandText = sqlComm;
            command.ExecuteNonQuery();

            sqlComm = "delete from gpr.GPRResults_staging_mar2 where Uniquerun like '%" + row["UniqueRun"].ToString().Substring(0, 6) + "%'";
            qcGroupID = Convert.ToInt32(this.dataMapper.DataProvider.ExecuteScalar(sqlComm, null));

            command.CommandText = sqlComm;
            command.ExecuteNonQuery();

          }


        }
      }

    }

    DataTable dtLanes = new DataTable();

    private void CompleteQCReportTask(TaskType taskType, RunWorkerCompletedEventArgs e)
    {
      switch (taskType)
      {

        case TaskType.QC_LANE_ANALYSIS:
          //this.ucCoreReqCollections.DataSource = this.dtCoreReqCollections;
          this.dgvQCReports.DataSource = this.dtLanes;

          //this.sstsMarStatus.Text = "";


          break;

      }

      sstsQCReportsProgressBar.Visible = false;
    }

    private void logGPRTool(string task, string batch, string filename)
    {
      //simple log to keep track of actions perfomed in the database
      try
      {
        string sqlComm = "INSERT INTO gpr.GPRToolLog (task, batch, filename, username, timestamp) VALUES ('" + task + "', '" + batch + "', '" + filename + "', '" + System.Environment.UserName + "', '" + System.DateTime.Now + "')";
        this.dataMapper.DataProvider.ExecuteNonQuery(sqlComm, null);
      }
      catch
      {
        //do nothing, allow the user to continue if log fails
      }


    }
  }
}