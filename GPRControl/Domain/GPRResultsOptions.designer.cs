namespace GPRControls.Domain
{
    partial class GPRResultsOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GPRResultsOptions));
            this.rbImportAll = new System.Windows.Forms.RadioButton();
            this.rbImportMar2 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMar2 = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnMar2Browse = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rbImportAll
            // 
            this.rbImportAll.AutoSize = true;
            this.rbImportAll.Checked = true;
            this.rbImportAll.Location = new System.Drawing.Point(17, 17);
            this.rbImportAll.Name = "rbImportAll";
            this.rbImportAll.Size = new System.Drawing.Size(131, 17);
            this.rbImportAll.TabIndex = 0;
            this.rbImportAll.TabStop = true;
            this.rbImportAll.Text = "Import all GPR Results";
            this.rbImportAll.UseVisualStyleBackColor = true;
            // 
            // rbImportMar2
            // 
            this.rbImportMar2.AutoSize = true;
            this.rbImportMar2.Location = new System.Drawing.Point(17, 44);
            this.rbImportMar2.Name = "rbImportMar2";
            this.rbImportMar2.Size = new System.Drawing.Size(231, 17);
            this.rbImportMar2.TabIndex = 1;
            this.rbImportMar2.Text = "Import only the results that match the MAR2";
            this.rbImportMar2.UseVisualStyleBackColor = true;
            this.rbImportMar2.CheckedChanged += new System.EventHandler(this.rbImportMar2_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "MAR2 Files:";
            // 
            // txtMar2
            // 
            this.txtMar2.Location = new System.Drawing.Point(93, 75);
            this.txtMar2.Name = "txtMar2";
            this.txtMar2.Size = new System.Drawing.Size(234, 20);
            this.txtMar2.TabIndex = 3;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(204, 115);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnMar2Browse
            // 
            this.btnMar2Browse.Image = ((System.Drawing.Image)(resources.GetObject("btnMar2Browse.Image")));
            this.btnMar2Browse.Location = new System.Drawing.Point(333, 73);
            this.btnMar2Browse.Name = "btnMar2Browse";
            this.btnMar2Browse.Size = new System.Drawing.Size(25, 23);
            this.btnMar2Browse.TabIndex = 4;
            this.btnMar2Browse.UseVisualStyleBackColor = true;
            this.btnMar2Browse.Click += new System.EventHandler(this.btnMar2Browse_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(288, 115);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // GPRResultsOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 150);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnMar2Browse);
            this.Controls.Add(this.txtMar2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbImportMar2);
            this.Controls.Add(this.rbImportAll);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GPRResultsOptions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Import Options";
            this.Load += new System.EventHandler(this.GPRResultsOptions_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbImportAll;
        private System.Windows.Forms.RadioButton rbImportMar2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMar2;
        private System.Windows.Forms.Button btnMar2Browse;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
    }
}