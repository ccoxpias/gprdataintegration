using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GPRControls.Domain
{
    public partial class GPRResultsOptions : Form
    {
        public bool all = true;
        public string mar2folder = "";

        public GPRResultsOptions()
        {
            InitializeComponent();
        }

        private void rbImportMar2_CheckedChanged(object sender, EventArgs e)
        {
            txtMar2.Enabled = rbImportMar2.Checked ? true : false;
            btnMar2Browse.Enabled = rbImportMar2.Checked ? true : false;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (rbImportMar2.Checked)
            {
                if (txtMar2.Text == "" || !System.IO.Directory.Exists(txtMar2.Text))
                {
                    MessageBox.Show("Please select a valid MAR2 folder", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                all = false;
                mar2folder = txtMar2.Text;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void btnMar2Browse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdOpenFolder = new FolderBrowserDialog();
            
            fdOpenFolder.Description = "Select the folder containing the MAR2 files";
            fdOpenFolder.SelectedPath = @"\\data\Data_Analysis\PROJECTS\5418CAGPR09\Batches";

            if (fdOpenFolder.ShowDialog(this) == DialogResult.OK)
            {
                txtMar2.Text = fdOpenFolder.SelectedPath;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GPRResultsOptions_Load(object sender, EventArgs e)
        {
            txtMar2.Enabled = rbImportMar2.Checked ? true : false;
            btnMar2Browse.Enabled = rbImportMar2.Checked ? true : false;
        }

        

       
    }
}