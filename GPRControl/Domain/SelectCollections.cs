using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Roadware.DataMapping;
using Roadware.WorkflowManagement.WindowsControls;

namespace GPRControls.Domain
{
	public partial class SelectCollections : Form
	{
		private DataMapper dataMapper;

		public GroupPartitionExplorer GroupPartitionExplorer
		{
			get
			{
				return this.ucGroup;
			}
		}

		public SelectCollections(DataMapper dataMapper)
		{
			this.dataMapper = dataMapper;
			InitializeComponent();
		}

		private void SelectCollections_Load(object sender, EventArgs e)
		{
			this.ucGroup.DataMapper = this.dataMapper;
		}
	}
}