using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace GPRControls.Domain
{
	public partial class DisplayCollections : UserControl
	{
		private DataTable dtCollections;

		public DataTable DataSource
		{
			get { return dtCollections; }
			set 
			{ 
				dtCollections = value;
				this.dgvCollections.DataSource = dtCollections;

				foreach (DataGridViewColumn col in dgvCollections.Columns)
				{
					if (col.Name.ToUpper().StartsWith("ID"))
					{
						col.Visible = false;
					}
				}
			}
		}

		public string Title
		{
			get { return this.lbTitle.Text; }
			set { this.lbTitle.Text = value; }
		}

		public DisplayCollections()
		{
			InitializeComponent();
		}
	}
}
