using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GPRControls.Domain
{
	public partial class MarEventCategories : Form
	{
		private string selectedCategories; 

		public string SelectedCategories
		{
			get { return selectedCategories; }
		}

		public MarEventCategories(List<string> categories)
		{
			InitializeComponent();

			LoadCategories(categories);
		}

		private void LoadCategories(List<string> categories)
		{
			categories.Sort();

			foreach (string category in categories)
			{
				this.clbCategory.Items.Add(category);
			}
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			if (this.clbCategory.CheckedItems.Count == 0)
			{
				this.selectedCategories = string.Empty;

				MessageBox.Show("Please select at least one category.", "Warning");
				this.DialogResult = DialogResult.Cancel;
                
			}
			else
			{
				this.selectedCategories = ",";

				foreach (object item in this.clbCategory.CheckedItems)
				{
					this.selectedCategories += item.ToString() + ",";
				}
			}
		}

        private void tsbSelectAll_Click(object sender, EventArgs e)
        {
            //check all items 
            for (int i = 0; i < clbCategory.Items.Count; i++)
            {
                this.clbCategory.SetItemCheckState(i, CheckState.Checked);
            }
        }

        private void tsbClearAll_Click(object sender, EventArgs e)
        {
            //uncheck all items
            for (int i = 0; i < clbCategory.Items.Count; i++)
            {
                clbCategory.SetItemCheckState(i, CheckState.Unchecked);
            }
        }
	}
}