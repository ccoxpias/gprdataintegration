using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GPRControls.Domain
{
	public enum ChainageUnit
	{
		Kilometer,
		Meter,
		Mile,
		Millimile,
		Foot
	}

	public enum ThicknessUnit
	{
		Meter,
		Centimeter,
		Millimeter,
		Millimile,
		Foot,
		Inch
	}

	public enum DisplayOption
	{
		CoreUnit,
		ChainageUnit,
		ThicknessUnit,
		GPRProcessing,
		GPRReporting
	}

	public enum VelocityType
	{
		Calculated,
		Constant
	}

	public partial class GPRSettings : Form
	{
		string[] CHAINAGE_UNITS = {"Kilometre", "Metre", "Mile", "Millimile", "Foot"};
		string[] VELOCITY_UNITS = {"m/ns", "mm/ns", "mmile/ns", "ft/ns", "in/ns" };
		string[] THICKNESS_UNITS = {"Metre", "Centimetre", "Millimetre", "Millimile", "Foot", "Inch"};
		int[] THICKNESS_DECIMAL = { 0, 1, 2, 3 };

		DisplayOption displayOption;
		VelocityType gprVelocityType;
		bool sizeChanged = false;

		public GPRSettings(DisplayOption option)
		{
			InitializeComponent();

			this.displayOption = option;
			SetDisplayOption(option);
		}

		#region methods
		public string GetChainageUnit(DisplayOption option)
		{
			string unit = null;

			switch (option)
			{
				case DisplayOption.CoreUnit:
				case DisplayOption.ChainageUnit:
					unit = Convert.ToString(this.cbCoreChainage.SelectedItem);
					break;

				case DisplayOption.GPRReporting:
					unit = Convert.ToString(this.cbGPRChainage.SelectedItem);
					break;
			}

			return unit;
		}

		public void SetChainageUnit(DisplayOption option, string unit)
		{
			switch (option)
			{
				case DisplayOption.CoreUnit:
				case DisplayOption.ChainageUnit:
					this.cbCoreChainage.SelectedItem = unit;
					break;

				case DisplayOption.GPRReporting:
					this.cbGPRChainage.SelectedItem = unit;
					break;
			}
		}

		public string GetThicknessUnit(DisplayOption option)
		{
			string unit = null;

			switch (option)
			{
				case DisplayOption.CoreUnit:
				case DisplayOption.ThicknessUnit:
					unit = Convert.ToString(this.cbCoreThickness.SelectedItem);
					break;

				case DisplayOption.GPRReporting:
					unit = Convert.ToString(this.cbGPRThickness.SelectedItem);
					break;
			}

			return unit;
		}

		public void SetThicknessUnit(DisplayOption option, string unit)
		{
			switch (option)
			{
				case DisplayOption.CoreUnit:
				case DisplayOption.ThicknessUnit:
					this.cbCoreThickness.SelectedItem = unit;
					break;

				case DisplayOption.GPRReporting:
					this.cbGPRThickness.SelectedItem = unit;
					break;
			}
		}

		public string GPRVelocityUnit
		{
			get	{ return Convert.ToString(this.cbVelocity.SelectedItem);}

			set 
			{
				if (this.cbVelocity.Items.Contains(value))
					this.cbVelocity.SelectedItem = value;
				else
				{
					this.cbVelocity.Items.Add(value);
					this.cbVelocity.SelectedItem = value;
				}
			}
		}

		public double GPRReportingInterval
		{
			get
			{
				try
				{
					return Convert.ToDouble(this.txtInterval.Text.Trim());
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.StackTrace, ex.Message);
					return double.NaN;
				}
			}

			set
			{
				this.txtInterval.Text = value == -1 ? string.Empty : value.ToString();
			}
		}

		public double GPRVelocity
		{
			get
			{
				try
				{
					if (this.rbnCalcVelocity.Checked)
						return 0.0;
					else
						return Convert.ToDouble(this.txtVelocity.Text.Trim());
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.StackTrace, ex.Message);
					return double.NaN;
				}
			}

			set { this.txtVelocity.Text = value.ToString(); }
		}

		public VelocityType GPRVelocityType
		{
			get { return this.gprVelocityType; }
			set 
			{ 
				this.gprVelocityType = value;

				if (this.gprVelocityType == VelocityType.Calculated)
					this.rbnCalcVelocity.Checked = true;
				else
					this.rbnConstVelocity.Checked = true;
			}
		}

		public double GPRThreshold
		{
			get
			{
				try
				{
					return Convert.ToDouble(this.txtThreshold.Text.Trim());
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.StackTrace, ex.Message);
					return double.NaN;
				}
			}

			set { this.txtThreshold.Text = value.ToString(); }
		}

		public double GetConversionFactor(string unit)
		{
			double conversion = double.NaN;

			switch (unit.ToLower())
			{
				case "kilometre":
					conversion = 0.001;
					break;
				case "metre":
					conversion = 1.0;
					break;
				case "centimetre":
					conversion = 100.0;
					break;
				case "millimetre":
					conversion = 1000.0;
					break;
				case "mile":
					conversion = (1.0 / 1.609344) * 0.001;
					break;
				case "millimile":
					conversion = 1.0 / 1.609344;
					break;
				case "foot":
					conversion = 1.0 / 0.3048;
					break;
				case "inch":
					conversion = 1.0 / 0.0254;
					break;
			}

			return conversion;
		}

		public double GetVelocityConversionFactor(string unit)
		{
			double conversion = double.NaN;

			switch (unit.ToLower())
			{
				case "m/ns":
					conversion = GetConversionFactor("metre");
					break;
				case "mm/ns":
					conversion = GetConversionFactor("millimetre");
					break;
				case "mmile/ns":
					conversion = GetConversionFactor("millimile");
					break;
				case "ft/ns":
					conversion = GetConversionFactor("foot");
					break;
				case "in/ns":
					conversion = GetConversionFactor("inch");
					break;
			}

			return conversion;
		}
		#endregion

		private void cbGPRChainage_SelectedIndexChanged(object sender, EventArgs e)
		{
			string unit = Convert.ToString(this.cbGPRChainage.SelectedItem);

			if (!string.IsNullOrEmpty(unit))
			{
				this.lbIntervalUnit.Text = GetAbbreviation(unit);
			}
		}


		#region helper
		private void SetDisplayOption(DisplayOption option)
		{
			switch (option)
			{
				case DisplayOption.CoreUnit:
					DisplayCoreUnitSetting(true, true);
					break;

				case DisplayOption.ChainageUnit:
					DisplayCoreUnitSetting(true, false);
					break;

				case DisplayOption.ThicknessUnit:
					DisplayCoreUnitSetting(false, true);
					break;

				case DisplayOption.GPRProcessing:
					DisplayGPRProcessingSetting();
					break;

				case DisplayOption.GPRReporting:
					DisplayGPRReportingSetting();
					break;
			}
		}

		private void DisplayCoreUnitSetting(bool showChainage, bool showThickness)
		{
			foreach (string unit in CHAINAGE_UNITS)
			{
				this.cbCoreChainage.Items.Add(unit);
			}

			foreach (string unit in THICKNESS_UNITS)
			{
				this.cbCoreThickness.Items.Add(unit);
			}

			if (!showChainage)
			{
				this.lbCoreChainage.Enabled = false;
				this.cbCoreChainage.Enabled = false;
			}

			if (!showThickness)
			{
				this.lbCoreThickness.Enabled = false;
				this.cbCoreThickness.Enabled = false;
			}

			this.pnlChainage.Visible = true;
			this.Height = this.Height - this.pnlGPRProcessing.Height - this.pnlGPRReport.Height;

			this.Text = "Core Units";
		}

		private void DisplayGPRProcessingSetting()
		{
			if (this.rbnCalcVelocity.Checked)
			{
				this.txtVelocity.Enabled = false;
				this.cbVelocity.Enabled = false;
				this.gprVelocityType = VelocityType.Calculated;
			}
			else
			{
				this.txtVelocity.Enabled = true;
				this.cbVelocity.Enabled = true;
				this.gprVelocityType = VelocityType.Constant;

				foreach (string unit in VELOCITY_UNITS)
				{
					if (!this.cbVelocity.Items.Contains(unit))
						this.cbVelocity.Items.Add(unit);
				}
			}

			if (!this.sizeChanged)
			{
				this.pnlGPRProcessing.Visible = true;
				this.Height = this.Height - this.pnlChainage.Height - this.pnlGPRReport.Height;

				this.sizeChanged = true;
			}

			this.Text = "Parameters";
		}

		private void DisplayGPRReportingSetting()
		{
			foreach (string unit in CHAINAGE_UNITS)
			{
				this.cbGPRChainage.Items.Add(unit);
			}

			foreach (string unit in THICKNESS_UNITS)
			{
				this.cbGPRThickness.Items.Add(unit);
			}

			this.pnlGPRReport.Visible = true;
			this.Height = this.Height - this.pnlChainage.Height - this.pnlGPRProcessing.Height;

			this.Text = "GPR Report Setting";
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			string message = string.Empty;

			switch (this.displayOption)
			{
				case DisplayOption.ChainageUnit:
					if (string.IsNullOrEmpty(Convert.ToString(this.cbCoreChainage.SelectedItem)))
					{
						message = "Please select chainage unit.";
					}
					break;

				case DisplayOption.ThicknessUnit:
					if (string.IsNullOrEmpty(Convert.ToString(this.cbCoreThickness.SelectedItem)))
					{
						message = "Please select thickness unit.";
					}
					break;

				case DisplayOption.CoreUnit:
					if (string.IsNullOrEmpty(Convert.ToString(this.cbCoreChainage.SelectedItem)) ||
						string.IsNullOrEmpty(Convert.ToString(this.cbCoreThickness.SelectedItem)))
					{
						message = "Please select both chainage unit and thickness unit.";
					}
					break;

				case DisplayOption.GPRProcessing:
					if (this.rbnConstVelocity.Checked && (string.IsNullOrEmpty(Convert.ToString(this.cbVelocity.SelectedItem)) ||
						string.IsNullOrEmpty(this.txtThreshold.Text.Trim()) ||
						string.IsNullOrEmpty(this.txtVelocity.Text.Trim())))
					{
						message = "Please select velocity unit and enter both velocity value and threshold value.";
					}
					break;

				case DisplayOption.GPRReporting:
					if (string.IsNullOrEmpty(Convert.ToString(this.cbGPRChainage.SelectedItem)) ||
						string.IsNullOrEmpty(Convert.ToString(this.cbGPRThickness.SelectedItem)) ||
						string.IsNullOrEmpty(this.txtInterval.Text.Trim()))
					{
						message = "Please select both chainage unit and thickness unit, and enter reporting interval.";
					}
					break;
			}

			if (!string.IsNullOrEmpty(message))
			{
				MessageBox.Show(message, "Warning");
			}
			else
			{
				this.DialogResult = DialogResult.OK;
			}
		}

		private void rbnConstVelocity_CheckedChanged_1(object sender, EventArgs e)
		{
			DisplayGPRProcessingSetting();
		}

		private void rbnCalcVelocity_CheckedChanged(object sender, EventArgs e)
		{
			DisplayGPRProcessingSetting();
		}

		private string GetAbbreviation(string unit)
		{
			string unitAbbreviation = "";

			switch (unit.ToLower())
			{
				case "kilometre":
					unitAbbreviation = "km";
					break;
				case "metre":
					unitAbbreviation = "m";
					break;
				case "mile":
					unitAbbreviation = "mile";
					break;
				case "millimile":
					unitAbbreviation = "mmile";
					break;
				case "foot":
					unitAbbreviation = "ft";
					break;
			}

			return unitAbbreviation;
		}
		#endregion
	}

	public class GPRResultSetting
	{
		public double Threshold = 1;
		public double Velocity = 0.100584;
		public double ReportingInterval = -1;
		public string VelocityUnit = "m/ns";
		public string ChainageUnit = string.Empty;
		public string ThicknessUnit = string.Empty;
		public VelocityType SelectedVelocityType = VelocityType.Calculated;
	}

	public class CoreUnitSetting
	{
		public string ChainageUnit = string.Empty;
		public string ThicknessUnit = string.Empty;
	}
}